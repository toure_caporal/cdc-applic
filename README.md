# CDC-Applic
Code related to handling of S1000D like **expressions** (see §4.14), with the intent to 
support formal verification and transformation of expressions:
- are they valid?
- are they meaningful?
- are they defined in a consistent manner?
- ...  

## S1000D applicability
S1000D allows creation of formal applicability expressions using **operators** (and, or), **properties** and **values**.  

Those expressions can be used for filtering.  
Building meaningful applicability expressions is not in the scope of S1000D.  

Two kinds of properties are defined:
- **product attributes** (in ACT): they have an anonymous data type
- **conditions** (in CCT): they have an explicit data type

In this code, there is no such distinction. There are only properties and all data types are explicit.

S1000D data types for product attributes (4.14.1-2.1.2.1) and conditions (4.14.2-2.2.1) are:
- boolean
- string
- integer
- real 
 
In addition, an enumeration and pattern can be associated to those data types to constrain the set of allowed values.

Assigning values to a property can be done by using single values or ranges.
- 1|10~20|30
- A|BB~ZZ|FF
- 1.0~2.0|3.1 

Some additional attributes are associated to product attributes and conditions, but they have no influence on formal verification and are therefore ignored.  

S1000D approach in its generality is quite challenging for formal verification.  
This is why we adopt a pragmatic solution: we only allow things that can be verified formally.  
An expression being a combination of operators, properties and values, the main challenge is with negation (even if negation is not allowed y S1000D). Negation is necessary to write implications or equivalences (which are also out of S1000D scope).
Using negation necessitates computation of the complement of a set of values. Negation on ranges necessitates the computation of the successor or predecessor of a value in the set of possible values.
 
At the moment, the following types are supported:
 - boolean
 - integer
 - enumerated strings
 
There are no enumerated integers, reals, ... They did not seem necessary. Using enumerated strings seemed sufficient. 
 
Some provisions have been taken for:
- reals
- patterned strings

It is not envisioned to support patterned integers and patterned reals.  
Enumeration and pattern are useless for boolean. 

## Principles
- A **product family** is characterized by a set of **properties**.
- Each **property** has a **type** which is a **set of values**.
- The number of **properties** characterizing a **product family** can change (increase) with time.
- The **values** of a **type** can change (increase) with time.
- There is no time relationship between **properties**.  
  Doing something before another thing can not be expressed directly.  
- Each **property** of a **product instance** has one and only one **value** at a given time.  
  But this **value** is not necessarily know from everyone.    
- Each **property** of a **product instance** can change with time. This is typical of conditions.
