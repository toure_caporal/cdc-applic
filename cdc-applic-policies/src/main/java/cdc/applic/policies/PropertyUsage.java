package cdc.applic.policies;

/**
 * Enumeration of possible usages of a property in a policy.
 *
 * @author Damien Carbonne
 */
public enum PropertyUsage {
    /**
     * Use of the property is possible.
     */
    OPTIONAL,

    /**
     * Use of the property is recommended.
     */
    RECOMMENDED,

    /**
     * Use of the property is mandatory.
     */
    MANDATORY
}