package cdc.applic.policies;

import cdc.applic.expressions.Expression;

/**
 * Base interface of writing rules.
 *
 * @author Damien Carbonne
 */
public interface WritingRule {
    public String getName();

    public WritingRulePurpose getPurpose();

    public boolean isCompliant(Expression expression);
}