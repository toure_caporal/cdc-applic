package cdc.applic.policies;

import cdc.applic.expressions.literals.Name;
import cdc.applic.policies.items.Alias;
import cdc.applic.policies.items.NamedItem;
import cdc.applic.policies.items.Property;

public interface Registry extends Policy {

    public Iterable<? extends NamedItem> getNamedItems();

    public NamedItem getNamedItem(Name name);

    public default boolean isDeclaredNamedItem(Name name) {
        return getNamedItem(name) != null;
    }

    public Iterable<? extends Property> getProperties();

    public Property getProperty(Name name);

    public default boolean isDeclaredProperty(Name name) {
        return getProperty(name) != null;
    }

    public Iterable<? extends Alias> getAliases();

    public Alias getAlias(Name name);

    public default boolean isDeclaredAlias(Name name) {
        return getAlias(name) != null;
    }
}