package cdc.applic.policies.types;

import cdc.applic.expressions.content.BooleanItem;

/**
 * Interface of boolean types.
 *
 * @author Damien Carbonne
 */
public interface BooleanType extends Type, Domained {
    /**
     * @return The {@link BooleanItem}s that define this type.
     */
    @Override
    public Iterable<? extends BooleanItem> getDomain();

    /**
     * Returns {@code true} when an {@link BooleanItem} is compliant with this type.
     *
     * @param item The item.
     * @return {@code true} when {@code item} is compliant with this type.
     */
    public boolean isCompliant(BooleanItem item);
}