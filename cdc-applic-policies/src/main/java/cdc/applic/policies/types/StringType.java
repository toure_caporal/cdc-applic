package cdc.applic.policies.types;

import cdc.applic.expressions.content.StringItem;

/**
 * Base interface of string based types.
 *
 * @author Damien Carbonne
 */
public interface StringType extends ModifiableType {
    /**
     * Returns {@code true} when a {@link StringItem} is compliant with this type.
     *
     * @param item The item.
     * @return {@code true} when {@code item} is compliant with this type.
     */
    public boolean isCompliant(StringItem item);
}