package cdc.applic.policies.types;

import cdc.applic.expressions.content.RealItem;

/**
 * Interface describing a real type.
 *
 * @author Damien Carbonne
 */
public interface RealType extends ModifiableType, Domained {
    /**
     * @return The {@link RealItem}s that define this type.
     */
    @Override
    public Iterable<? extends RealItem> getDomain();

    /**
     * Returns {@code true} when a {@link RealItem} is compliant with this type.
     *
     * @param item The item.
     * @return {@code true} when {@code item} is compliant with this type.
     */
    public boolean isCompliant(RealItem item);
}