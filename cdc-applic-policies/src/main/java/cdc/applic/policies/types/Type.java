package cdc.applic.policies.types;

import cdc.applic.expressions.content.Item;

/**
 * Base interface of types.
 *
 * @author Damien Carbonne
 */
public interface Type {
    /**
     * @return The name of this type.
     */
    public String getName();

    /**
     * Returns {@code true} if an {@link Item} is compliant with this type.
     *
     * @param item The item.
     * @return {@code true} if {@code item} is compliant with this type.
     */
    public boolean isCompliant(Item item);

    /**
     * @return A string describing this type.
     */
    public String getDefinition();
}