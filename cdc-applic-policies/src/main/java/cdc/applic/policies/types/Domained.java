package cdc.applic.policies.types;

import cdc.applic.expressions.content.Item;

/**
 * Interface of types that have a Domain.
 */
public interface Domained {
    /**
     * @return The {@link Item}s that define this type.
     */
    public Iterable<? extends Item> getDomain();
}