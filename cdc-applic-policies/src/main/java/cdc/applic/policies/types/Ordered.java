package cdc.applic.policies.types;

import java.util.Comparator;

/**
 * Interface implemented by object that have an ordinal.
 * <p>
 * This ordinal can be used to sort objects.
 *
 * @author Damien Carbonne
 */
public interface Ordered {
    /**
     * A Comparator of Ordered objects that uses ordinal.
     */
    public static final Comparator<Ordered> ORDINAL_COMPARATOR =
            (o1,
             o2) -> o1.getOrdinal() - o2.getOrdinal();

    /**
     * @return The ordinal of this object.
     */
    public int getOrdinal();
}