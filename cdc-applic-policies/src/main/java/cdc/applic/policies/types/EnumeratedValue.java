package cdc.applic.policies.types;

import cdc.applic.expressions.literals.Name;

/**
 * Enumerated value.
 *
 * @author Damien Carbonne
 */
public interface EnumeratedValue extends Ordered {
    /**
     * @return The literal of this enumerated value.
     */
    public Name getLiteral();

    /**
     * @return The short literal of this enumerated value.
     */
    public Name getShortLiteral();
}