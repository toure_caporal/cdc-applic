package cdc.applic.policies.types;

import cdc.applic.expressions.content.IntegerItem;

/**
 * Interface describing an integer type.
 *
 * @author Damien Carbonne
 */
public interface IntegerType extends ModifiableType, Domained {
    /**
     * @return The {@link IntegerItem}s that define this type.
     */
    @Override
    public Iterable<? extends IntegerItem> getDomain();

    /**
     * Returns {@code true} when an {@link IntegerItem} is compliant with this type.
     *
     * @param item The item.
     * @return {@code true} when {@code item} is compliant with this type.
     */
    public boolean isCompliant(IntegerItem item);
}