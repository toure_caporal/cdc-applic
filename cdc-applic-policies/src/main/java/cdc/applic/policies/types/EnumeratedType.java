package cdc.applic.policies.types;

import cdc.applic.expressions.content.StringItem;
import cdc.applic.expressions.literals.Name;

/**
 * An enumerated type is a string type that is defined in extension.
 * <p>
 * The enumerated values literals shall be different.<br>
 * The enumerated values short literals shall also be different.
 *
 * @author Damien Carbonne
 */
public interface EnumeratedType extends StringType, Domained {
    /**
     * @return The {@link StringItem}s that defines this type.
     */
    @Override
    public Iterable<? extends StringItem> getDomain();

    /**
     * @return The enumerated values that defines this type.
     */
    public Iterable<? extends EnumeratedValue> getValues();

    /**
     * Returns the enumerated value that has a given name.
     *
     * @param name The name.
     * @return The enumerated value that is named {@code name}, or {@code null}.
     */
    public EnumeratedValue getValue(Name name);
}