package cdc.applic.policies.types;

/**
 * Base interface of {@link Type}s whose definition could possibly change with time.
 *
 * @author Damien Carbonne
 */
public interface ModifiableType extends Type {
    /**
     * @return {@code true} if the definition of this type can not change with time.
     */
    public boolean isFrozen();
}