package cdc.applic.policies.types;

/**
 * A PatternType is a {@link StringType} whose allowed values are defined by a pattern.
 * <p>
 * PatternType should only be used when {@link EnumeratedType} can not be used.
 *
 * @author Damien Carbonne
 */
public interface PatternType extends StringType {
    /**
     * @return The pattern that compliant values shall follow.
     */
    public String getPattern();
}