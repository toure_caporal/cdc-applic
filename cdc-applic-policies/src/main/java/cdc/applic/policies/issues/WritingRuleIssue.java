package cdc.applic.policies.issues;

import cdc.applic.expressions.issues.Issue;
import cdc.applic.expressions.issues.IssueType;

public class WritingRuleIssue extends Issue {
    private final String name;

    public WritingRuleIssue(String name,
                            String description) {
        super(IssueType.WRITING_ISSUE,
              description);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}