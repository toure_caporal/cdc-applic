package cdc.applic.policies;

public enum WritingRulePurpose {
    COMPLEXITY,
    HOMOGENEITY
}