package cdc.applic.policies;

import cdc.applic.expressions.literals.Name;
import cdc.applic.policies.items.Alias;
import cdc.applic.policies.items.Assertion;
import cdc.applic.policies.items.NamedItem;
import cdc.applic.policies.items.Property;

public interface Policy {
    public String getName();

    public Registry getRegistry();

    public Iterable<? extends NamedItem> getAllowedItems();

    public NamedItem getAllowedItem(Name name);

    public default boolean isAllowedItem(Name name) {
        return getAllowedItem(name) != null;
    }

    public Iterable<? extends Property> getAllowedProperties();

    public Iterable<? extends Property> getAllowedProperties(PropertyUsage usage);

    public Property getAllowedProperty(Name name);

    public default boolean isAllowedProperty(Name name) {
        return getAllowedProperty(name) != null;
    }

    public PropertyUsage getUsage(Property property);

    public Iterable<? extends Alias> getAllowedAliases();

    public Property getAllowedAlias(Name name);

    public default boolean isAllowedAlias(Name name) {
        return getAllowedAlias(name) != null;
    }

    public Iterable<? extends Assertion> getAssertions();

    public Iterable<? extends WritingRule> getWritingRules();
}