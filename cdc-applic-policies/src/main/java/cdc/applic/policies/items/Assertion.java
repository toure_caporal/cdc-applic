package cdc.applic.policies.items;

/**
 * An assertion is an expression that must always evaluate to true.
 * <p>
 * Assertions are used to link properties together: they are dependent.
 *
 * @author Damien Carbonne
 */
public interface Assertion extends Expressed {
    // Ignore
}