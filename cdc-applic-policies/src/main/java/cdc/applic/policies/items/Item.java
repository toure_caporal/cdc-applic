package cdc.applic.policies.items;

/**
 * Marking interface of policy items.
 *
 * @author Damien Carbonne
 */
public interface Item {
    // Ignore
}