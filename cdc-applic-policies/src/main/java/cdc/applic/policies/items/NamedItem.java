package cdc.applic.policies.items;

import cdc.applic.expressions.literals.Named;

/**
 * Base interface of items that have a Name.
 *
 * @author Damien Carbonne
 */
public interface NamedItem extends Item, Named {
    // Ignore
}