package cdc.applic.policies.items;

/**
 * Base interface of aliases.
 * <p>
 * An Alias is an named expression. This name can be used in place of the expression.
 *
 * @author Damien Carbonne
 */
public interface Alias extends Expressed, NamedItem {
    // Ignore
}