package cdc.applic.policies.items;

import cdc.applic.expressions.Expression;

/**
 * Base interface of items that are associated to an expression.
 *
 * @author Damien Carbonne
 */
public interface Expressed extends Item {
    /**
     * @return The associated expression.
     */
    public Expression getExpression();
}