package cdc.applic.policies.items;

import cdc.applic.policies.types.Ordered;
import cdc.applic.policies.types.Type;

/**
 * A property is a characteristic of a product.
 * <p>
 * It has a type that defines the set of values that can be taken.
 *
 * @author Damien Carbonne
 */
public interface Property extends NamedItem, Ordered {
    /**
     * @return The type of this property.
     */
    public Type getType();
}