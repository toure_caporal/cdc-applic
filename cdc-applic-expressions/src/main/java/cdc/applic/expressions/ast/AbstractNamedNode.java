package cdc.applic.expressions.ast;

import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.Named;

/**
 * Base class of nodes that have a name.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractNamedNode extends AbstractLeafNode implements ParsingNode, Named {
    protected final Name name;

    protected AbstractNamedNode(Name name) {
        this.name = name;
    }

    @Override
    public final String getKind() {
        return getNameKind().name();
    }

    @Override
    public final boolean isNamed() {
        return true;
    }

    @Override
    public final boolean isTrue() {
        return getNameKind() == NamedNodeKind.TRUE;
    }

    @Override
    public final boolean isFalse() {
        return getNameKind() == NamedNodeKind.FALSE;
    }

    @Override
    public final boolean isRef() {
        return getNameKind() == NamedNodeKind.REF;
    }

    public abstract NamedNodeKind getNameKind();

    @Override
    public final Name getName() {
        return name;
    }
}