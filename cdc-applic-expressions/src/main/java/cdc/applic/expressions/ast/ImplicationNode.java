package cdc.applic.expressions.ast;

public final class ImplicationNode extends AbstractBinaryNode {
    public ImplicationNode(Node alpha,
                           Node beta) {
        super(alpha,
              beta);
    }

    @Override
    public OperatorNodeKind getOperatorKind() {
        return OperatorNodeKind.IMPLICATION;
    }

    @Override
    public ImplicationNode create(Node alpha,
                                  Node beta) {
        return new ImplicationNode(alpha, beta);
    }
}