package cdc.applic.expressions.ast;

import cdc.util.lang.Checks;

/**
 * Base class of binary nodes.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractBinaryNode extends AbstractOperatorNode implements ParsingNode {
    private final Node alpha;
    private final Node beta;

    public AbstractBinaryNode(Node alpha,
                              Node beta) {
        Checks.isNotNull(alpha, "alpha");
        Checks.isNotNull(beta, "beta");
        this.alpha = alpha;
        this.beta = beta;
    }

    public final Node getAlpha() {
        return alpha;
    }

    public final Node getBeta() {
        return beta;
    }

    @Override
    public final int getChildrenCount() {
        return 2;
    }

    @Override
    public final Node getChildAt(int index) {
        if (index == 0) {
            return alpha;
        } else if (index == 1) {
            return beta;
        } else {
            throw new IllegalArgumentException("Invalid index (" + index + ")");
        }
    }

    @Override
    public final boolean isBinary() {
        return true;
    }

    @Override
    public final boolean isNary() {
        return false;
    }

    public abstract AbstractBinaryNode create(Node alpha,
                                              Node beta);

    @Override
    public final int getHeight() {
        return 1 + Math.max(alpha.getHeight(), beta.getHeight());
    }

    @Override
    public <R> R accept(Visitor<R> visitor) {
        return visitor.visitBinary(this);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AbstractBinaryNode)) {
            return false;
        }
        final AbstractBinaryNode other = (AbstractBinaryNode) object;
        return getOperatorKind() == other.getOperatorKind()
                && getAlpha().equals(other.getAlpha())
                && getBeta().equals(other.getBeta());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return getOperatorKind() + "(" + getAlpha() + "," + getBeta() + ")";
    }
}