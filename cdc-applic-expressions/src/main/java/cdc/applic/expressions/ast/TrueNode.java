package cdc.applic.expressions.ast;

import cdc.applic.expressions.literals.Name;

public final class TrueNode extends AbstractNamedNode implements AtomNode {
    public static final Name TRUE = new Name("true", false);
    public static final TrueNode INSTANCE = new TrueNode();

    private TrueNode() {
        super(TRUE);
    }

    @Override
    public NamedNodeKind getNameKind() {
        return NamedNodeKind.TRUE;
    }

    @Override
    public boolean isProperty() {
        return false;
    }

    @Override
    public boolean isValue() {
        return false;
    }

    @Override
    public boolean isSet() {
        return false;
    }

    @Override
    public boolean equals(Object object) {
        // There is only one instance of this class
        return this == object;
    }

    @Override
    public int hashCode() {
        return getNameKind().hashCode();
    }

    @Override
    public String toString() {
        return getKind();
    }
}