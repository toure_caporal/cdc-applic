package cdc.applic.expressions.ast;

import cdc.applic.expressions.content.ItemSet;
import cdc.applic.expressions.literals.Name;

public final class NotInNode extends AbstractSetNode {
    public NotInNode(Name propertyName,
                     ItemSet items) {
        super(propertyName,
              items);
    }

    @Override
    public NotInNode create(Name propertyName,
                            ItemSet items) {
        return new NotInNode(propertyName, items);
    }

    @Override
    public NamedNodeKind getNameKind() {
        return NamedNodeKind.NOT_IN;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof NotInNode)) {
            return false;
        }
        final NotInNode other = (NotInNode) object;
        return getName().equals(other.getName())
                && getItems().equals(other.getItems());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}