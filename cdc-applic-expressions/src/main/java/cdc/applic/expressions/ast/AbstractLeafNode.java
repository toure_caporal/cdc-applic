package cdc.applic.expressions.ast;

/**
 * Base class of leaf nodes.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractLeafNode implements Node {
    @Override
    public final int getChildrenCount() {
        return 0;
    }

    @Override
    public final Node getChildAt(int index) {
        throw new IllegalArgumentException("Invalid index (" + index + ")");
    }

    @Override
    public final int getHeight() {
        return 1;
    }

    @Override
    public final boolean isOperator() {
        return false;
    }

    @Override
    public final boolean isUnary() {
        return false;
    }

    @Override
    public final boolean isNot() {
        return false;
    }

    @Override
    public final boolean isBinary() {
        return false;
    }

    @Override
    public final boolean isBinaryAnd() {
        return false;
    }

    @Override
    public final boolean isBinaryOr() {
        return false;
    }

    @Override
    public final boolean isNary() {
        return false;
    }

    @Override
    public final boolean isNaryAnd() {
        return false;
    }

    @Override
    public final boolean isNaryOr() {
        return false;
    }

    @Override
    public final boolean isLeaf() {
        return true;
    }

    @Override
    public <R> R accept(Visitor<R> visitor) {
        return visitor.visitLeaf(this);
    }
}