package cdc.applic.expressions.ast;

import cdc.applic.expressions.literals.Name;

/**
 * Base class of property nodes.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractPropertyNode extends AbstractNamedNode {
    public AbstractPropertyNode(Name propertyName) {
        super(propertyName);
    }

    @Override
    public final boolean isProperty() {
        return true;
    }
}