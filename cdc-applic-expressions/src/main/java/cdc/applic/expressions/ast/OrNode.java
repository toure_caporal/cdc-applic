package cdc.applic.expressions.ast;

public final class OrNode extends AbstractBinaryNode {
    public OrNode(Node alpha,
                  Node beta) {
        super(alpha,
              beta);
    }

    @Override
    public OperatorNodeKind getOperatorKind() {
        return OperatorNodeKind.OR;
    }

    @Override
    public OrNode create(Node alpha,
                         Node beta) {
        return new OrNode(alpha, beta);
    }
}