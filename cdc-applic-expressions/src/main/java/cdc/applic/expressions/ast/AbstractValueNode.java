package cdc.applic.expressions.ast;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Base class of value nodes.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractValueNode extends AbstractPropertyNode {
    private final Value value;

    public AbstractValueNode(Name propertyName,
                             Value value) {
        super(propertyName);
        Checks.isNotNull(value, "value");
        this.value = value;
    }

    @Override
    public final boolean isValue() {
        return true;
    }

    @Override
    public final boolean isSet() {
        return false;
    }

    public abstract AbstractValueNode create(Name propertyName,
                                             Value value);

    public final Value getValue() {
        return value;
    }

    @Override
    public final String toString() {
        return getKind() + "(" + getName()
                + "," + getValue().getProtectedLiteral()
                + ")";
    }
}