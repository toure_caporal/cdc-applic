package cdc.applic.expressions.ast;

/**
 * Enumeration of possible {@link AbstractNamedNode}.
 *
 * @author Damien Carbonne
 */
public enum NamedNodeKind {
    /**
     * Kind of {@link EqualNode}.
     */
    EQUAL,

    /**
     * Kind of {@link NotEqualNode}.
     */
    NOT_EQUAL,

    /**
     * Kind of {@link InNode}.
     */
    IN,

    /**
     * Kind of {@link NotInNode}.
     */
    NOT_IN,

    /**
     * Kind of {@link RefNode}.
     */
    REF,

    /**
     * Kind of {@link TrueNode}.
     */
    TRUE,

    /**
     * Kind of {@link FalseNode}.
     */
    FALSE
}