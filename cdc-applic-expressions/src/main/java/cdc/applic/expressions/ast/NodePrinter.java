package cdc.applic.expressions.ast;

import java.io.PrintStream;

import cdc.util.debug.Printables;

public final class NodePrinter {
    private NodePrinter() {
    }

    public static void print(Node node,
                             PrintStream out) {
        print(node, out, 0);
    }

    /**
     * Prints a node tree.
     *
     * @param node The node t print.
     * @param out The PrintStream to use.
     * @param level The level of indentation to be used for node.
     */
    public static void print(Node node,
                             PrintStream out,
                             int level) {
        Printables.indent(out, level);
        out.print(node.getKind());

        if (node.isBinary()) {
            out.println();
            final AbstractBinaryNode n = (AbstractBinaryNode) node;
            print(n.getAlpha(), out, level + 1);
            print(n.getBeta(), out, level + 1);
        } else if (node.isNary()) {
            out.println();
            final AbstractNaryNode n = (AbstractNaryNode) node;
            for (final Node child : n.getChildren()) {
                print(child, out, level + 1);
            }
        } else if (node.isNot()) {
            out.println();
            final NotNode n = (NotNode) node;
            print(n.getAlpha(), out, level + 1);
        } else if (node.isLeaf()) {
            if (node instanceof AbstractValueNode) {
                final AbstractValueNode n = (AbstractValueNode) node;
                out.println(" " + n.getName() + " " + n.getValue());
            } else if (node instanceof AbstractSetNode) {
                final AbstractSetNode n = (AbstractSetNode) node;
                out.println(" " + n.getName() + " " + n.getItems());
            } else if (node instanceof TrueNode || node instanceof FalseNode) {
                out.println();
            } else if (node instanceof RefNode) {
                final RefNode n = (RefNode) node;
                out.println(" " + n.getName());
            }
        }
    }
}