package cdc.applic.expressions.ast;

import java.util.ArrayList;
import java.util.List;

import cdc.util.lang.Checks;

public final class NaryOrNode extends AbstractNaryNode {
    public NaryOrNode(Node... children) {
        super(children);
    }

    public NaryOrNode(List<? extends Node> children) {
        super(children);
    }

    @Override
    public OperatorNodeKind getOperatorKind() {
        return OperatorNodeKind.NARY_OR;
    }

    @Override
    public final NaryOrNode create(Node... children) {
        return new NaryOrNode(children);
    }

    @Override
    public NaryOrNode create(List<Node> children) {
        return new NaryOrNode(children);
    }

    public static Node createSimplestOr(List<? extends Node> nodes) {
        Checks.isNotNullOrEmpty(nodes, "nodes");

        if (nodes.size() == 1) {
            return nodes.get(0);
        } else if (nodes.size() == 2) {
            return new OrNode(nodes.get(0), nodes.get(1));
        } else {
            return new NaryOrNode(nodes);
        }
    }

    public static Node createSimplestOr(Node... nodes) {
        Checks.isNotNullOrEmpty(nodes, "nodes");

        if (nodes.length == 1) {
            return nodes[0];
        } else if (nodes.length == 2) {
            return new OrNode(nodes[0], nodes[1]);
        } else {
            return new NaryOrNode(nodes);
        }
    }

    public static Node mergeSimplestOr(Node... nodes) {
        Checks.isNotNullOrEmpty(nodes, "nodes");

        final List<Node> children = new ArrayList<>();
        for (final Node node : nodes) {
            if (node.isNaryOr()) {
                for (final Node child : ((NaryOrNode) node).getChildren()) {
                    children.add(child);
                }
            } else if (node.isBinaryOr()) {
                children.add(((OrNode) node).getAlpha());
                children.add(((OrNode) node).getBeta());
            } else {
                children.add(node);
            }
        }
        return createSimplestOr(children);
    }
}