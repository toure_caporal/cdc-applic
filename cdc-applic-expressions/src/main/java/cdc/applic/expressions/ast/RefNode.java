package cdc.applic.expressions.ast;

import cdc.applic.expressions.literals.Name;

public final class RefNode extends AbstractNamedNode implements AtomNode {
    public RefNode(Name name) {
        super(name);
    }

    @Override
    public NamedNodeKind getNameKind() {
        return NamedNodeKind.REF;
    }

    @Override
    public boolean isProperty() {
        return false;
    }

    @Override
    public boolean isValue() {
        return false;
    }

    @Override
    public boolean isSet() {
        return false;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof RefNode)) {
            return false;
        }
        final RefNode other = (RefNode) object;
        return name.equals(other.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return getKind() + "(" + getName() + ")";
    }
}