package cdc.applic.expressions.ast;

public final class EquivalenceNode extends AbstractBinaryNode {
    public EquivalenceNode(Node alpha,
                           Node beta) {
        super(alpha,
              beta);
    }

    @Override
    public OperatorNodeKind getOperatorKind() {
        return OperatorNodeKind.EQUIVALENCE;
    }

    @Override
    public final EquivalenceNode create(Node alpha,
                                        Node beta) {
        return new EquivalenceNode(alpha, beta);
    }
}