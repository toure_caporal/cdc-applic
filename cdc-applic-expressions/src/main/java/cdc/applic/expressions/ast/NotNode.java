package cdc.applic.expressions.ast;

public final class NotNode extends AbstractUnaryNode implements ParsingNode {
    public NotNode(Node alpha) {
        super(alpha);
    }

    @Override
    public OperatorNodeKind getOperatorKind() {
        return OperatorNodeKind.NOT;
    }

    @Override
    public final NotNode create(Node alpha) {
        return new NotNode(alpha);
    }

    public static Node simplestNot(Node node) {
        if (node.isNot()) {
            return ((NotNode) node).getAlpha();
        } else {
            return new NotNode(node);
        }
    }
}