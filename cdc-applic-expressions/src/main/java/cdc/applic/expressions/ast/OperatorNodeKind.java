package cdc.applic.expressions.ast;

/**
 * Enumeration of possible {@link AbstractOperatorNode}.
 *
 * @author Damien Carbonne
 */
public enum OperatorNodeKind {
    /**
     * Kind of {@link EquivalenceNode}.
     */
    EQUIVALENCE,

    /**
     * Kind of {@link ImplicationNode}.
     */
    IMPLICATION,

    /**
     * Kind of {@link OrNode}.
     */
    OR,

    /**
     * Kind of {@link NaryOrNode}.
     */
    NARY_OR,

    /**
     * Kind of {@link AndNode}.
     */
    AND,

    /**
     * Kind of {@link NaryAndNode}.
     */
    NARY_AND,

    /**
     * Kind of {@link NotNode}.
     */
    NOT
}