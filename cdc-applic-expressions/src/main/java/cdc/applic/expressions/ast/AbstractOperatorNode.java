package cdc.applic.expressions.ast;

/**
 * Base class of operator nodes.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractOperatorNode implements AtomNode {

    @Override
    public final String getKind() {
        return getOperatorKind().name();
    }

    public abstract OperatorNodeKind getOperatorKind();

    @Override
    public final boolean isOperator() {
        return true;
    }

    @Override
    public final boolean isUnary() {
        return getOperatorKind() == OperatorNodeKind.NOT;
    }

    @Override
    public final boolean isNot() {
        return getOperatorKind() == OperatorNodeKind.NOT;
    }

    // @Override
    // public final boolean isBinary() {
    // return getOperatorKind() == OperatorNodeKind.AND
    // || getOperatorKind() == OperatorNodeKind.OR
    // || getOperatorKind() == OperatorNodeKind.EQUIVALENCE
    // || getOperatorKind() == OperatorNodeKind.IMPLICATION;
    // }

    @Override
    public final boolean isBinaryAnd() {
        return getOperatorKind() == OperatorNodeKind.AND;
    }

    @Override
    public final boolean isBinaryOr() {
        return getOperatorKind() == OperatorNodeKind.OR;
    }

    // @Override
    // public final boolean isNary() {
    // return getOperatorKind() == OperatorNodeKind.NARY_AND
    // || getOperatorKind() == OperatorNodeKind.NARY_OR;
    // }

    @Override
    public final boolean isNaryAnd() {
        return getOperatorKind() == OperatorNodeKind.NARY_AND;
    }

    @Override
    public final boolean isNaryOr() {
        return getOperatorKind() == OperatorNodeKind.NARY_OR;
    }

    @Override
    public final boolean isLeaf() {
        return false;
    }

    @Override
    public final boolean isProperty() {
        return false;
    }

    @Override
    public final boolean isValue() {
        return false;
    }

    @Override
    public final boolean isSet() {
        return false;
    }

    @Override
    public final boolean isNamed() {
        return false;
    }

    @Override
    public final boolean isTrue() {
        return false;
    }

    @Override
    public final boolean isFalse() {
        return false;
    }

    @Override
    public final boolean isRef() {
        return false;
    }
}