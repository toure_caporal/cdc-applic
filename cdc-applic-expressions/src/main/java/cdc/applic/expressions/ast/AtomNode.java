package cdc.applic.expressions.ast;

/**
 * Marking interface of node that are atoms.
 * <p>
 * Atom nodes are the nodes that can be transformed to a clause.
 * Some {@link ParsingNode} are also atoms.
 *
 * @author Damien Carbonne
 */
public interface AtomNode extends Node {
    // Ignore
}