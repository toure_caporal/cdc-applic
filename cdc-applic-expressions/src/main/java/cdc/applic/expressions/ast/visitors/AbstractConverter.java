package cdc.applic.expressions.ast.visitors;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.Visitor;

/**
 * Base class of converters.
 * <p>
 * They can transform a node into another one.
 *
 * @author D. Carbonne
 *
 */
public class AbstractConverter implements Visitor<Node> {

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        // By default return the node
        return node;
    }

    @Override
    public Node visitUnary(AbstractUnaryNode node) {
        return node.create(node.getAlpha().accept(this));
    }

    @Override
    public Node visitBinary(AbstractBinaryNode node) {
        return node.create(node.getAlpha().accept(this),
                           node.getBeta().accept(this));
    }

    @Override
    public Node visitNary(AbstractNaryNode node) {
        final Node[] subs = node.getChildren().clone();
        for (int index = 0; index < subs.length; index++) {
            subs[index] = subs[index].accept(this);
        }
        return node.create(subs);
    }
}