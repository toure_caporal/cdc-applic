package cdc.applic.expressions.ast;

import cdc.applic.expressions.content.ItemSet;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Base class of set nodes.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractSetNode extends AbstractPropertyNode {
    private final ItemSet set;

    public AbstractSetNode(Name propertyName,
                           ItemSet items) {
        super(propertyName);
        Checks.isNotNull(items, "items");
        this.set = items;
    }

    /**
     * @return The set of items.
     */
    public final ItemSet getItems() {
        return set;
    }

    @Override
    public boolean isValue() {
        return false;
    }

    @Override
    public boolean isSet() {
        return true;
    }

    public abstract Node create(Name propertyName,
                                ItemSet set);

    @Override
    public final String toString() {
        return getKind() + "(" + getName()
                + "," + getItems()
                + ")";
    }
}