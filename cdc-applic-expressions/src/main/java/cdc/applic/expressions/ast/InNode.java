package cdc.applic.expressions.ast;

import cdc.applic.expressions.content.ItemSet;
import cdc.applic.expressions.literals.Name;

public final class InNode extends AbstractSetNode {
    public InNode(Name propertyName,
                  ItemSet items) {
        super(propertyName,
              items);
    }

    @Override
    public InNode create(Name propertyName,
                         ItemSet items) {
        return new InNode(propertyName, items);
    }

    @Override
    public NamedNodeKind getNameKind() {
        return NamedNodeKind.IN;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof InNode)) {
            return false;
        }
        final InNode other = (InNode) object;
        return getName().equals(other.getName())
                && getItems().equals(other.getItems());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}