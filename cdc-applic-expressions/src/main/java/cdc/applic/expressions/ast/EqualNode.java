package cdc.applic.expressions.ast;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;

public final class EqualNode extends AbstractValueNode {
    public EqualNode(Name propertyName,
                     Value value) {
        super(propertyName,
              value);
    }

    @Override
    public EqualNode create(Name propertyName,
                            Value value) {
        return new EqualNode(propertyName, value);
    }

    @Override
    public NamedNodeKind getNameKind() {
        return NamedNodeKind.EQUAL;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof EqualNode)) {
            return false;
        }
        final EqualNode other = (EqualNode) object;
        return getName().equals(other.getName())
                && getValue().equals(other.getValue());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}