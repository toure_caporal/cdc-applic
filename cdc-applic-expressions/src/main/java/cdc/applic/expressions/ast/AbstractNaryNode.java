package cdc.applic.expressions.ast;

import java.util.Arrays;
import java.util.List;

import cdc.util.lang.Checks;

/**
 * Base class of n-ary nodes.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractNaryNode extends AbstractOperatorNode {
    private final Node[] children;

    public AbstractNaryNode(Node... children) {
        Checks.isNotNull(children, "children");
        if (children.length < 1) {
            throw new IllegalArgumentException("No enough children");
        }
        this.children = children.clone();
    }

    public AbstractNaryNode(List<? extends Node> children) {
        this(children.toArray(new Node[children.size()]));
    }

    @Override
    public final int getChildrenCount() {
        return children.length;
    }

    @Override
    public final Node getChildAt(int index) {
        if (index >= 0 && index < children.length) {
            return children[index];
        } else {
            throw new IllegalArgumentException("Invalid index (" + index + ")");
        }
    }

    @Override
    public final boolean isBinary() {
        return false;
    }

    @Override
    public final boolean isNary() {
        return true;
    }

    public final Node[] getChildren() {
        return children;
    }

    public final List<Node> getChildrenAsList() {
        return Arrays.asList(children);
    }

    public abstract AbstractNaryNode create(Node... children);

    public abstract AbstractNaryNode create(List<Node> children);

    @Override
    public final int getHeight() {
        int max = 0;
        for (int index = 0; index < children.length; index++) {
            max = Math.max(max, children[index].getHeight());
        }
        return 1 + max;
    }

    @Override
    public <R> R accept(Visitor<R> visitor) {
        return visitor.visitNary(this);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AbstractNaryNode)) {
            return false;
        }
        final AbstractNaryNode other = (AbstractNaryNode) object;
        if (getOperatorKind() != other.getOperatorKind() || children.length != other.children.length) {
            return false;
        }
        for (int index = 0; index < children.length; index++) {
            if (!children[index].equals(other.children[index])) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(getOperatorKind());
        builder.append("(");
        for (int index = 0; index < children.length; index++) {
            if (index != 0) {
                builder.append(",");
            }
            builder.append(children[index]);
        }
        builder.append(")");

        return builder.toString();
    }
}