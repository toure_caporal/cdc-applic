package cdc.applic.expressions.ast;

public final class AndNode extends AbstractBinaryNode {
    public AndNode(Node alpha,
                   Node beta) {
        super(alpha,
              beta);
    }

    @Override
    public OperatorNodeKind getOperatorKind() {
        return OperatorNodeKind.AND;
    }

    @Override
    public AndNode create(Node alpha,
                          Node beta) {
        return new AndNode(alpha, beta);
    }
}