package cdc.applic.expressions.ast;

import cdc.applic.expressions.literals.Name;

public final class FalseNode extends AbstractNamedNode implements AtomNode {
    public static final Name FALSE = new Name("false", false);
    public static final FalseNode INSTANCE = new FalseNode();

    private FalseNode() {
        super(FALSE);
    }

    @Override
    public NamedNodeKind getNameKind() {
        return NamedNodeKind.FALSE;
    }

    @Override
    public boolean isProperty() {
        return false;
    }

    @Override
    public boolean isValue() {
        return false;
    }

    @Override
    public boolean isSet() {
        return false;
    }

    @Override
    public boolean equals(Object object) {
        // There is only one instance of this class
        return this == object;
    }

    @Override
    public int hashCode() {
        return getNameKind().hashCode();
    }

    @Override
    public String toString() {
        return getKind();
    }
}