package cdc.applic.expressions.ast;

import java.util.ArrayList;
import java.util.List;

import cdc.util.lang.Checks;

public final class NaryAndNode extends AbstractNaryNode {
    public NaryAndNode(Node... children) {
        super(children);
    }

    public NaryAndNode(List<? extends Node> children) {
        super(children);
    }

    @Override
    public OperatorNodeKind getOperatorKind() {
        return OperatorNodeKind.NARY_AND;
    }

    @Override
    public final NaryAndNode create(Node... children) {
        return new NaryAndNode(children);
    }

    @Override
    public NaryAndNode create(List<Node> children) {
        return new NaryAndNode(children);
    }

    public static Node createSimplestAnd(List<? extends Node> nodes) {
        Checks.isNotNullOrEmpty(nodes, "nodes");

        if (nodes.size() == 1) {
            return nodes.get(0);
        } else if (nodes.size() == 2) {
            return new AndNode(nodes.get(0), nodes.get(1));
        } else {
            return new NaryAndNode(nodes);
        }
    }

    public static Node createSimplestAnd(Node... nodes) {
        Checks.isNotNullOrEmpty(nodes, "nodes");

        if (nodes.length == 1) {
            return nodes[0];
        } else if (nodes.length == 2) {
            return new AndNode(nodes[0], nodes[1]);
        } else {
            return new NaryAndNode(nodes);
        }
    }

    public static Node mergeSimplestAnd(Node... nodes) {
        Checks.isNotNullOrEmpty(nodes, "nodes");

        final List<Node> children = new ArrayList<>();
        for (final Node node : nodes) {
            if (node.isNaryAnd()) {
                for (final Node child : ((NaryAndNode) node).getChildren()) {
                    children.add(child);
                }
            } else if (node.isBinaryAnd()) {
                children.add(((AndNode) node).getAlpha());
                children.add(((AndNode) node).getBeta());
            } else {
                children.add(node);
            }
        }
        return createSimplestAnd(children);
    }
}