package cdc.applic.expressions.ast;

import cdc.util.lang.Checks;

/**
 * Base class of unary nodes.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractUnaryNode extends AbstractOperatorNode {
    private final Node alpha;

    public AbstractUnaryNode(Node alpha) {
        Checks.isNotNull(alpha, "alpha");
        this.alpha = alpha;
    }

    @Override
    public final int getChildrenCount() {
        return 1;
    }

    @Override
    public final Node getChildAt(int index) {
        if (index == 0) {
            return alpha;
        } else {
            throw new IllegalArgumentException("Invalid index (" + index + ")");
        }
    }

    @Override
    public final boolean isBinary() {
        return false;
    }

    @Override
    public final boolean isNary() {
        return false;
    }

    public final Node getAlpha() {
        return alpha;
    }

    public abstract AbstractUnaryNode create(Node alpha);

    @Override
    public final int getHeight() {
        return 1 + alpha.getHeight();
    }

    @Override
    public <R> R accept(Visitor<R> visitor) {
        return visitor.visitUnary(this);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AbstractUnaryNode)) {
            return false;
        }
        final AbstractUnaryNode other = (AbstractUnaryNode) object;
        return getOperatorKind() == other.getOperatorKind()
                && getAlpha().equals(other.getAlpha());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return getOperatorKind() + "(" + getAlpha() + ")";
    }
}