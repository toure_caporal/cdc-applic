package cdc.applic.expressions.ast;

/**
 * Base interface of parsing and atom nodes.
 *
 * @author Damien Carbonne
 */
public interface Node {
    /**
     * @return The node kind as a string.
     */
    public String getKind();

    /**
     * @return The number of children nodes.
     */
    public int getChildrenCount();

    /**
     * Returns the child located at an index.
     *
     * @param index The index.
     * @return The child at {@code index}.
     */
    public Node getChildAt(int index);

    /**
     * @return The height of the tree under this node. The height of a leaf node is 1.
     */
    public int getHeight();

    /**
     * @return {@code true} if this is an {@link AbstractOperatorNode}.
     */
    public boolean isOperator();

    /**
     * @return {@code true} if this is an {@link AbstractUnaryNode}.
     */
    public boolean isUnary();

    /**
     * @return {@code true} if this is a {@link NotNode}.
     */
    public boolean isNot();

    /**
     * @return {@code true} if this is an {@link AbstractBinaryNode}.
     */
    public boolean isBinary();

    /**
     * @return {@code true} if this is a {@link AndNode}.
     */
    public boolean isBinaryAnd();

    /**
     * @return {@code true} if this is a {@link OrNode}.
     */
    public boolean isBinaryOr();

    /**
     * @return {@code true} if this is an {@link AbstractNaryNode}.
     */
    public boolean isNary();

    /**
     * @return {@code true} if this is a {@link NaryAndNode}.
     */
    public boolean isNaryAnd();

    /**
     * @return {@code true} if this is a {@link NaryOrNode}.
     */
    public boolean isNaryOr();

    /**
     * @return {@code true} if this is an {@link AbstractLeafNode}.
     */
    public boolean isLeaf();

    /**
     * @return {@code true} if this is an {@link AbstractPropertyNode}.
     */
    public boolean isProperty();

    /**
     * @return {@code true} if this is an {@link AbstractValueNode}.
     */
    public boolean isValue();

    /**
     * @return {@code true} if this is an {@link AbstractSetNode}.
     */
    public boolean isSet();

    /**
     * @return {@code true} if this is an {@link AbstractNamedNode}.
     */
    public boolean isNamed();

    /**
     * @return {@code true} if this is a {@link TrueNode}.
     */
    public boolean isTrue();

    /**
     * @return {@code true} if this is a {@link FalseNode}.
     */
    public boolean isFalse();

    /**
     * @return {@code true} if this is a {@link RefNode}.
     */
    public boolean isRef();

    public <R> R accept(Visitor<R> visitor);
}