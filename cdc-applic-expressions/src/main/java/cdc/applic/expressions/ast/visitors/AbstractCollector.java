package cdc.applic.expressions.ast.visitors;

import java.util.HashSet;
import java.util.Set;

/**
 * Base class of collectors.
 * <p>
 * They collect information and accumulate it in a set.
 * @author D. Carbonne
 *
 * @param <T> Type of the collected information.
 */
public class AbstractCollector<T> extends AbstractAnalyzer {
    protected final Set<T> set;

    protected AbstractCollector() {
        this.set = new HashSet<>();
    }

    protected AbstractCollector(Set<T> set) {
        this.set = set;
    }

    public Set<T> getSet() {
        return set;
    }
}