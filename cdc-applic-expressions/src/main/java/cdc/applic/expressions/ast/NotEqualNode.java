package cdc.applic.expressions.ast;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;

public final class NotEqualNode extends AbstractValueNode {
    public NotEqualNode(Name propertyName,
                        Value value) {
        super(propertyName,
              value);
    }

    @Override
    public NotEqualNode create(Name propertyName,
                               Value value) {
        return new NotEqualNode(propertyName, value);
    }

    @Override
    public NamedNodeKind getNameKind() {
        return NamedNodeKind.NOT_EQUAL;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof NotEqualNode)) {
            return false;
        }
        final NotEqualNode other = (NotEqualNode) object;
        return getName().equals(other.getName())
                && getValue().equals(other.getValue());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}