package cdc.applic.expressions;

import cdc.applic.expressions.ast.ParsingNode;
import cdc.applic.expressions.parsing.Parser;
import cdc.applic.expressions.parsing.Recognizer;

public class Expression {
    /**
     * String representation of the Expression, as defined at creation time.
     */
    private final String content;

    /**
     * The AST root node.
     */
    private ParsingNode root = null;

    public static final Expression TRUE = new Expression("true");
    public static final Expression FALSE = new Expression("false");

    public Expression(String content,
                      boolean parse) {
        if (content == null || content.trim().isEmpty()) {
            this.content = "true";
        } else {
            this.content = content;
        }
        if (parse) {
            getRootNode();
        }
    }

    public Expression(String content) {
        this(content, true);
    }

    /**
     * @return The text representation of this expression, as defined at creation.
     */
    public String getContent() {
        return content;
    }

    /**
     * @return The AST root node.
     */
    public ParsingNode getRootNode() {
        if (root == null) {
            final Parser parser = new Parser();
            root = parser.parse(content);
        }
        return root;
    }

    public boolean isValid() {
        return root != null || isValid(content);
    }

    public boolean isTrue() {
        return getRootNode().isTrue();
    }

    public boolean isFalse() {
        return getRootNode().isFalse();
    }

    public static boolean isValid(String expression) {
        final Recognizer recognizer = new Recognizer();
        return recognizer.isValid(expression);
    }

    public static Expression fromString(String content) {
        return new Expression(content);
    }

    public static String asString(Expression expression) {
        return expression.content;
    }

    @Override
    public int hashCode() {
        return content.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Expression)) {
            return false;
        }
        final Expression other = (Expression) object;
        return content.equals(other.content);
    }

    @Override
    public String toString() {
        return content;
    }
}