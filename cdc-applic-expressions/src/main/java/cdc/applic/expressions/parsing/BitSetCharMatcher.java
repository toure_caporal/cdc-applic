package cdc.applic.expressions.parsing;

import java.util.BitSet;

/**
 * CharSet implementation based on BitSet.
 * <p>
 * The implementation has a constant response time, slightly worse than {@link BooleanArrayCharMatcher}.
 * It is 8 times more memory efficient (1 bit per character.
 *
 * @author Damien Carbonne
 */
public final class BitSetCharMatcher implements CharMatcher {
    private final BitSet set;

    public BitSetCharMatcher(String chars) {
        this.set = new BitSet(CharMatcher.max(chars) + 1);
        for (int index = 0; index < chars.length(); index++) {
            this.set.set(chars.charAt(index));
        }
    }

    @Override
    public boolean matches(char c) {
        return set.get(c);
    }
}