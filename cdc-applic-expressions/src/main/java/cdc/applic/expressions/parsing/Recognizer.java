package cdc.applic.expressions.parsing;

import cdc.applic.expressions.issues.InvalidSyntaxException;

/**
 * Class used to check syntactical validity of an Expression.
 * <p>
 * This class is not reentrant.
 *
 * @author Damien Carbonne
 *
 */
public class Recognizer extends AbstractParser {
    public Recognizer() {
        super();
    }

    /**
     * Checks the syntactical validity of an expression.
     *
     * @param expression The expression to check.
     * @return {@code true} if the expression is syntactically valid.
     */
    public boolean isValid(String expression) {
        try {
            recognize(expression);
            return true;
        } catch (final InvalidSyntaxException e) {
            // Ignore e
            return false;
        }
    }

    /**
     * Recognizes an expression from a syntactical point of view.
     * <p>
     * If the expression is syntactically invalid, an exception is raised.
     *
     * @param expression The expression to recognize.
     * @throws InvalidSyntaxException When the expression is syntactically invalid.
     */
    public void recognize(String expression) {
        tokenizer.init(expression);
        tokenizer.next();
        parseExpr();
        expect("recognize", TokenType.EPSILON);
    }

    private void parseExpr() {
        parsePExpr();
        while (tokenizer.getTokenType().isBinary()) {
            tokenizer.next();
            parsePExpr();
        }
    }

    private void parsePExpr() {
        switch (tokenizer.getTokenType()) {
        case OPEN_PAREN:
            tokenizer.next();
            parseExpr();
            expect("parsePExpr", TokenType.CLOSE_PAREN);
            break;
        case NOT:
            tokenizer.next();
            parseExpr();
            break;
        case TRUE:
        case FALSE:
            tokenizer.next();
            break;
        case TEXT:
        case ESCAPED_TEXT:
            parseBaseExpr();
            break;
        default:
            throw newInvalidExpression(InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN,
                                       "Unexpected token: " + tokenizer.getToken() + " in: '" + tokenizer.getExpression() + "' parsePExpr");
        }
    }

    private void parseBaseExpr() {
        tokenizer.next(); // Property or Ref
        switch (tokenizer.getTokenType()) {
        case EQUAL:
        case NOT_EQUAL:
            tokenizer.next();
            expect("parseBaseEpr", ITEM_TOKEN_TYPES);
            break;
        case IN:
        case NOT_IN:
            tokenizer.next();
            parseItemSet();
            break;
        default:
            break;
        }
    }

    private void parseItemSet() {
        expect("parseItemSet", TokenType.OPEN_SET);
        if (tokenizer.getTokenType().isItem()) {
            parseItem();
        }

        while (tokenizer.getTokenType() == TokenType.VALUES_SEP) {
            tokenizer.next();
            parseItem();
        }
        expect("parseItemSet", TokenType.CLOSE_SET);
    }

    private void parseItem() {
        final TokenType type = tokenizer.getTokenType();
        expect("parseItem", ITEM_TOKEN_TYPES);
        if (type == TokenType.INTEGER && tokenizer.getTokenType() == TokenType.TO) {
            tokenizer.next();
            expect("parseItem", TokenType.INTEGER);
        } else if (type == TokenType.REAL && tokenizer.getTokenType() == TokenType.TO) {
            tokenizer.next();
            expect("parseItem", TokenType.REAL);
        }
    }
}