package cdc.applic.expressions.parsing;

public class MultiplyShiftCharMatcher implements CharMatcher {
    private final char[] chars;
    private final int multiply;
    private final int shift;

    public MultiplyShiftCharMatcher(String table,
                                int multiply,
                                int shift) {
        this.chars = table.toCharArray();
        this.multiply = multiply;
        this.shift = shift;
    }

    @Override
    public boolean matches(char c) {
        return chars[(c * multiply) >>> shift] == c;
    }
}