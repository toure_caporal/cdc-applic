package cdc.applic.expressions.parsing;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.expressions.issues.InvalidSyntaxException;
import cdc.applic.expressions.literals.EscapingUtils;

/**
 * Implementation of Tokenizer.
 * <p>
 * This class analyzes an expression or piece of expression and identifies its tokens.
 * It does not check conformity of the expression to a grammar.
 * <p>
 * A typical usage would be:
 * <pre><code>
 * final Tokenizer tokenizer = new Tokenizer();
 * tokenizer.init("my expression");
 * while (tokenizer.hasMoreTokens()) {
 *     final Token token = tokenizer.nextToken();
 *     // Do something with token
 * }
 * </code></pre>
 *
 * @author Damien Carbonne
 */
public final class Tokenizer {
    /** The expression to tokenize. */
    private String expression;

    private TokenType tokenType;
    /** Index of token first character (inclusive) **/
    private int begin;
    /** Index of token last character (exclusive) */
    private int end;

    /**
     * Equivalent character array.
     * <p>
     * Accessing this array is faster that accessing expression (less checks).
     */
    private char[] chars;

    /**
     * Length of chars (chars.length).
     */
    private int charsLength;

    /**
     * Current analysis position.
     */
    private int pos;

    public Tokenizer() {
        super();
    }

    private static boolean isDigit(char c) {
        return '0' <= c && c <= '9';
    }

    /**
     * Returns {@code true} when there is a char at an index.
     *
     * @param index The tested index.
     * @return {@code true} when there is a char at {@code index}.
     */
    private boolean hasCharAt(int index) {
        return index < charsLength;
    }

    /**
     * Returns {@code true} when a token boundary exists at a location.
     *
     * @param index The location.
     * @return {@code true} when a token boundary exists at {@code index}.
     */
    private boolean hasBoundaryAt(int index) {
        if (hasCharAt(index)) {
            if (OneCharSeparators.BEST_MATCHER.matches(chars[index])) {
                return true;
            } else {
                if (hasCharAt(index + 1)) {
                    if (matchesAt(index, '-')
                            && matchesAt(index + 1, '>')) {
                        // '->'
                        return true;
                    } else if (matchesAt(index, '<')
                            && matchesAt(index + 1, ':')) {
                        // '<:'
                        return true;
                    }
                    if (hasCharAt(index + 2)) {
                        if (matchesAt(index, '<')
                                && matchesAt(index + 1, '-')
                                && matchesAt(index + 2, '>')) {
                            // '<->'
                            return true;
                        }
                    }
                }
                return false;
            }
        } else {
            // End of chars
            return true;
        }
    }

    /**
     * Increments {@code pos} while the designated character is a space.
     * <p>
     * After that, the character designated by {@code pos}, if it exists, is not a
     * space.
     */
    private void skipSpaces() {
        while (pos < charsLength && Spaces.BEST_MATCHER.matches(chars[pos])) {
            pos++;
        }
    }

    /**
     * Increments {@code pos} while the designated character is a digit.
     * <p>
     * After that, the character designated by {@code pos}, if it exists, is not a
     * digit.
     *
     * @return The number of skipped digits.
     */
    private int skipDigits() {
        final int mem = pos;
        while (pos < charsLength && isDigit(chars[pos])) {
            pos++;
        }
        return pos - mem;
    }

    private enum NumberType {
        INTEGER(TokenType.INTEGER),
        REAL(TokenType.REAL);

        private final TokenType tokenType;

        private NumberType(TokenType tokenType) {
            this.tokenType = tokenType;
        }

        public TokenType getTokenType() {
            return tokenType;
        }
    }

    private NumberType skipPossibleNumber() {
        final int begin = pos;
        final NumberType result;
        skipDigits();

        if (hasCharAt(pos) && matchesAt(pos, '.')) {
            pos++;
            // Accept no decimal digits ?
            final int decimals = skipDigits();
            if (decimals == 0) {
                throw new InvalidSyntaxException(InvalidSyntaxException.Detail.TOKENIZE_INVALID_NUMBER,
                                                 "Real number must have decimals",
                                                 expression,
                                                 begin,
                                                 pos);
            }
            if (hasCharAt(pos) && matchesAt(pos, 'e', 'E')) {
                pos++;
                if (hasCharAt(pos) && matchesAt(pos, '+', '-')) {
                    pos++;
                }
                final int exponent = skipDigits();
                if (exponent > 0) {
                    result = NumberType.REAL;
                } else {
                    throw new InvalidSyntaxException(InvalidSyntaxException.Detail.TOKENIZE_INVALID_NUMBER,
                                                     "Exponent must be followed by digits",
                                                     expression,
                                                     begin,
                                                     pos);
                }
            } else {
                result = NumberType.REAL;
            }
        } else {
            result = NumberType.INTEGER;
        }
        if (hasBoundaryAt(pos)) {
            return result;
        } else {
            throw new InvalidSyntaxException(InvalidSyntaxException.Detail.TOKENIZE_MISSING_BOUNDARY,
                                             "A number must be followed by boundary",
                                             expression,
                                             begin,
                                             pos);
        }
    }

    private void skipText() {
        while (pos < charsLength && !hasBoundaryAt(pos)) {
            pos++;
        }
    }

    /**
     * Searches the closing '"' character.
     * <p>
     * It is a '"' not followed by another '"'.
     * Must be invoked with {@code pos} designating the first character after the opening '"'.
     * After that, {@code pos} designates the first character following the escaped text.
     *
     * @return {@code true} when closing '"' has been found.
     */
    private boolean skipEscapedText() {
        while (pos < charsLength) {
            if (chars[pos] == '"') {
                pos++;
                if (pos < charsLength && chars[pos] == '"') {
                    // One char after '""'
                    pos++;
                    // continue exploration
                } else {
                    // One char after closing '"'
                    return true;
                }
            } else {
                pos++;
            }
        }
        return false;
    }

    /**
     * Returns {@code true} when the character at a given index corresponds to a given
     * character.
     *
     * @param c The searched character.
     * @param index The tested index.
     * @return {@code true} when the character at {@code index} corresponds to {@code c}.
     */
    private boolean matchesAt(int index,
                              char c) {
        return chars[index] == c;
    }

    /**
     * Returns {@code true} when the character at a given index corresponds to one of 2
     * given characters.
     *
     * @param c1 The first searched character.
     * @param c2 The second searched character.
     * @param index The tested index.
     * @return {@code true} when the character at {@code index} corresponds to {@code c1} or {@code c2}.
     */
    private boolean matchesAt(int index,
                              char c1,
                              char c2) {
        return chars[index] == c1 || chars[index] == c2;
    }

    /**
     * Initializes this tokenizer with an expression.
     *
     * @param expression The expression to tokenize.
     */
    public void init(String expression) {
        this.expression = expression;
        this.chars = expression.toCharArray();
        this.charsLength = chars.length;
        this.pos = 0;
        this.begin = -1;
        this.end = -1;
        this.tokenType = null;
        skipSpaces();
    }

    /**
     * @return The expression being tokenized.
     */
    public String getExpression() {
        return expression;
    }

    /**
     * @return The current token type.
     */
    public TokenType getTokenType() {
        return tokenType;
    }

    /**
     * @return The begin index of current token.
     */
    public int getBeginIndex() {
        return begin;
    }

    /**
     * @return The end index of current token.
     */
    public int getEndIndex() {
        return end;
    }

    /**
     * @return The text of current token.
     */
    public String getText() {
        if (expression == null) {
            return null;
        } else if (end <= begin) {
            return "";
        } else {
            return expression.substring(getBeginIndex(), getEndIndex());
        }
    }

    /**
     * @return The unescaped text of current token.
     */
    public String getUnescapedText() {
        final String text = getText();
        if (tokenType == TokenType.ESCAPED_TEXT) {
            return EscapingUtils.unescape(text);
        } else {
            return text;
        }
    }

    /**
     * @return The current token.
     */
    public Token getToken() {
        return tokenType == TokenType.EPSILON
                ? Token.TOK_EPSILON
                : new Token(tokenType, expression, begin, end);
    }

    /**
     * @return {@code true} when more tokens follow.
     */
    public boolean hasMoreTokens() {
        return pos < charsLength;
    }

    /**
     * Moves to next token.
     */
    public void next() {
        if (hasCharAt(pos)) {
            begin = pos;
            // Current char
            final char c = chars[pos];
            // Advance reading position.
            pos++;
            // Index of token last character (exclusive)
            end = pos;

            if (OneCharTokens.BEST_MATCHER.matches(c)) {
                // Handling of special characters that correspond to a token
                // type without further reading.
                tokenType = OneCharTokens.BEST_MAPPER.get(c);
            } else {
                // Handling of other characters
                switch (c) {
                case '!':
                    // Recognize '!', '!<:' and '!='
                    if (hasCharAt(pos)) {
                        if (matchesAt(pos, '=')) {
                            tokenType = TokenType.NOT_EQUAL;
                            pos++;
                            end = pos;
                        } else if (hasCharAt(pos + 1)
                                && matchesAt(pos, '<')
                                && matchesAt(pos + 1, ':')) {
                            tokenType = TokenType.NOT_IN;
                            pos += 2;
                            end = pos;
                        } else {
                            tokenType = TokenType.NOT;
                        }
                    } else {
                        tokenType = TokenType.NOT;
                    }
                    break;

                case '-':
                    // Recognize '->' and negative numbers
                    if (hasCharAt(pos)) {
                        if (matchesAt(pos, '>')) {
                            tokenType = TokenType.IMPL;
                            pos++;
                            end = pos;
                        } else if (isDigit(chars[pos])) {
                            final NumberType type = skipPossibleNumber();
                            tokenType = type.getTokenType();
                            end = pos;
                        } else {
                            skipText();
                            tokenType = TokenType.TEXT;
                            end = pos;
                        }
                    } else {
                        tokenType = TokenType.TEXT;
                        end = pos;
                    }
                    break;

                case '+':
                    // Recognize positive numbers
                    if (hasCharAt(pos)) {
                        if (isDigit(chars[pos])) {
                            final NumberType type = skipPossibleNumber();
                            tokenType = type.getTokenType();
                            end = pos;
                        } else {
                            skipText();
                            tokenType = TokenType.TEXT;
                            end = pos;
                        }
                    } else {
                        tokenType = TokenType.TEXT;
                        end = pos;
                    }
                    break;

                case '<':
                    // Recognize '<:' and '<->'
                    if (hasCharAt(pos)) {
                        if (matchesAt(pos, ':')) {
                            tokenType = TokenType.IN;
                            pos++;
                            end = pos;
                        } else if (hasCharAt(pos + 1)
                                && matchesAt(pos, '-')
                                && matchesAt(pos + 1, '>')) {
                            tokenType = TokenType.EQUIV;
                            pos += 2;
                            end = pos;
                        } else {
                            skipText();
                            tokenType = TokenType.TEXT;
                            end = pos;
                        }
                    } else {
                        skipText();
                        tokenType = TokenType.TEXT;
                        end = pos;
                    }
                    break;

                case '"':
                    // Found an escaped text
                    final boolean closed = skipEscapedText();
                    if (!closed) {
                        throw new InvalidSyntaxException(InvalidSyntaxException.Detail.TOKENIZE_MISSING_CLOSING_QUOTES,
                                                         "Closing '\"' not found",
                                                         expression,
                                                         begin,
                                                         -1);
                    }
                    if (pos == begin + 2) {
                        throw new InvalidSyntaxException(InvalidSyntaxException.Detail.TOKENIZE_EMPTY_ESCAPED_TEXT,
                                                         "Invalid empty escaped text",
                                                         expression,
                                                         begin,
                                                         pos);
                    }
                    tokenType = TokenType.ESCAPED_TEXT;
                    end = pos;
                    break;

                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    final NumberType type = skipPossibleNumber();
                    tokenType = type.getTokenType();
                    end = pos;
                    break;

                case 'a':
                case 'A':
                    // Recognize [aA][nN][dD]
                    if (hasCharAt(pos + 1)
                            && matchesAt(pos, 'n', 'N')
                            && matchesAt(pos + 1, 'd', 'D')
                            && hasBoundaryAt(pos + 2)) {
                        tokenType = TokenType.AND;
                        pos += 2;
                    } else {
                        skipText();
                        tokenType = TokenType.TEXT;
                    }
                    end = pos;
                    break;

                case 'f':
                case 'F':
                    // Recognize [fF][aA][lL][sS][eE]
                    if (hasCharAt(pos + 3)
                            && matchesAt(pos, 'a', 'A')
                            && matchesAt(pos + 1, 'l', 'L')
                            && matchesAt(pos + 2, 's', 'S')
                            && matchesAt(pos + 3, 'e', 'E')
                            && hasBoundaryAt(pos + 4)) {
                        tokenType = TokenType.FALSE;
                        pos += 4;
                    } else {
                        skipText();
                        tokenType = TokenType.TEXT;
                    }
                    end = pos;
                    break;

                case 'i':
                case 'I':
                    // Recognize [iI][nN] and [iI][mM][pP] and [iI][fF][fF]
                    if (hasCharAt(pos)
                            && matchesAt(pos, 'n', 'N')
                            && hasBoundaryAt(pos + 1)) {
                        tokenType = TokenType.IN;
                        pos++;
                    } else if (hasCharAt(pos + 1)
                            && matchesAt(pos, 'm', 'M')
                            && matchesAt(pos + 1, 'p', 'P')
                            && hasBoundaryAt(pos + 2)) {
                        tokenType = TokenType.IMPL;
                        pos += 2;
                    } else if (hasCharAt(pos + 1)
                            && matchesAt(pos, 'f', 'F')
                            && matchesAt(pos + 1, 'f', 'F')
                            && hasBoundaryAt(pos + 2)) {
                        tokenType = TokenType.EQUIV;
                        pos += 2;
                    } else {
                        skipText();
                        tokenType = TokenType.TEXT;
                    }
                    end = pos;
                    break;

                case 'n':
                case 'N':
                    // Recognize [nN][oO][tT] and [nN][oO][tT] [iI][nN]
                    if (hasCharAt(pos + 1)
                            && matchesAt(pos, 'o', 'O')
                            && matchesAt(pos + 1, 't', 'T')
                            && hasBoundaryAt(pos + 2)) {
                        pos += 2;
                        end = pos;
                        skipSpaces();
                        if (hasCharAt(pos + 1)
                                && matchesAt(pos, 'i', 'I')
                                && matchesAt(pos + 1, 'n', 'N')
                                && hasBoundaryAt(pos + 2)) {
                            tokenType = TokenType.NOT_IN;
                            pos += 2;
                            end = pos;
                        } else {
                            tokenType = TokenType.NOT;
                        }
                    } else {
                        skipText();
                        tokenType = TokenType.TEXT;
                        end = pos;
                    }
                    break;

                case 'o':
                case 'O':
                    // Recognize [oO][rR]
                    if (hasCharAt(pos)
                            && matchesAt(pos, 'r', 'R')
                            && hasBoundaryAt(pos + 1)) {
                        tokenType = TokenType.OR;
                        pos++;
                    } else {
                        skipText();
                        tokenType = TokenType.TEXT;
                    }
                    end = pos;
                    break;

                case 't':
                case 'T':
                    // Recognize [tT][oO] and [tT][rR][uU][eE]
                    if (hasCharAt(pos)
                            && matchesAt(pos, 'o', 'O')
                            && hasBoundaryAt(pos + 1)) {
                        tokenType = TokenType.TO;
                        pos++;
                    } else if (hasCharAt(pos + 2)
                            && matchesAt(pos, 'r', 'R')
                            && matchesAt(pos + 1, 'u', 'U')
                            && matchesAt(pos + 2, 'e', 'E')
                            && hasBoundaryAt(pos + 3)) {
                        tokenType = TokenType.TRUE;
                        pos += 3;
                    } else {
                        skipText();
                        tokenType = TokenType.TEXT;
                    }
                    end = pos;
                    break;

                default:
                    skipText();
                    tokenType = TokenType.TEXT;
                    end = pos;
                    break;
                }
            }
            skipSpaces();
        } else {
            tokenType = TokenType.EPSILON;
            begin = -1;
            end = -1;
        }
    }

    /**
     * Moves to next token and returns it.
     * <p>
     * When no more token are available, returns {@link Token#TOK_EPSILON}.
     *
     * @return The following token.
     * @throws InvalidSyntaxException When the parsed expression is invalid.
     */
    public Token nextToken() {
        next();
        return getToken();
    }

    /**
     * Returns the list of tokens found in an expression.
     *
     * @param expression The expression.
     * @return The list of tokens of {@code expression}.
     * @throws InvalidSyntaxException When {@code expression} is invalid.
     */
    public static List<Token> tokenize(String expression) {
        final List<Token> tokens = new ArrayList<>();
        final Tokenizer tokenizer = new Tokenizer();
        tokenizer.init(expression);
        while (tokenizer.hasMoreTokens()) {
            tokens.add(tokenizer.nextToken());
        }
        return tokens;
    }

    /**
     * Returns a compressed equivalent of an expression.
     *
     * @param expression The string to compress.
     * @return A compressed equivalent if {@code expression}.
     */
    public static String compress(String expression) {
        final Tokenizer tokenizer = new Tokenizer();
        final StringBuilder builder = new StringBuilder();
        tokenizer.init(expression);
        while (tokenizer.hasMoreTokens()) {
            final Token token = tokenizer.nextToken();
            if (token.getType().getSymbol() != null) {
                builder.append(token.getType().getSymbol());
            } else {
                builder.append(token.getText());
            }
        }
        return builder.toString();
    }
}