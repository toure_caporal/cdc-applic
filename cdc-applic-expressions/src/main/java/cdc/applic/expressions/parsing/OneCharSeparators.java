package cdc.applic.expressions.parsing;

/**
 * Recognition of separators that hold on one character.
 *
 * @author Damien Carbonne
 */
public final class OneCharSeparators {
    /**
     * String containing characters that are considered as one character separators.
     */
    protected static final String CHARS = OneCharTokens.CHARS + "!" + Spaces.CHARS;

    /**
     * Perfect hash table for {@link #CHARS}.
     * <p>
     * ' ' is used as the filling character.<br>
     * <b>WARNING:</b> This is computed and must match {@link #CHARS}.
     */
    protected static final String STABLE =
            "           \t\n\u000b\f\r  \u1680   {| }~   \u303f≠  \u0085→ ↔ \u0020! \u205f   &  ()∈∉,\u2000\u2001\u2002 \u2003\u2004\u2005\u2006\u2007 \u2008\u2009\u200a\u200b\u00a0     =      \u3000¬      ∧∨            \u2028\u2029      \u202f  ⊤⊥    \ufeff\u180e      ";

    /**
     * Conversion of {@link #STABLE} as a char array.
     */
    protected static final char[] TABLE = STABLE.toCharArray();

    /**
     * The multiplier to use for {@link #TABLE}.
     */
    protected static final int MULTIPLY = 41117259;

    /**
     * The shift to use for {@link #TABLE}.
     */
    protected static final int SHIFT = 25;

    private OneCharSeparators() {
    }

    public static final CharMatcher MULTIPLY_SHIFT_MATCHER =
            new MultiplyShiftCharMatcher(STABLE,
                                         MULTIPLY,
                                         SHIFT);

    public static final CharMatcher MULTIPLY_SHIFT_INLINE_MATCHER =
            new CharMatcher() {
                @Override
                public boolean matches(char c) {
                    return TABLE[(c * MULTIPLY) >>> SHIFT] == c;
                }
            };

    public static final CharMatcher BIT_SET_MATCHER =
            new BitSetCharMatcher(CHARS);

    public static final CharMatcher BOOLEAN_ARRAY_MATCHER =
            new BooleanArrayCharMatcher(CHARS);

    /**
     * Matcher that must be used to identify one character separators.
     */
    public static final CharMatcher BEST_MATCHER = MULTIPLY_SHIFT_INLINE_MATCHER;
}