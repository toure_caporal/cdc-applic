package cdc.applic.expressions.parsing;

/**
 * Interface describing set of characters.
 * <p>
 * The only available method is contains.
 *
 * @author Damien Carbonne
 */
public interface CharMatcher {
    /**
     * Returns {@code true} if a character is contained in this set.
     * 
     * @param c The character.
     * @return {@code true} if {@code c} is contained in this set.
     */
    public boolean matches(char c);

    /**
     * Returns the position of the largest character.
     *
     * @param chars The characters.
     * @return The position of the largest character in {@code chars}.
     */
    static int max(String chars) {
        int max = -1;
        for (int index = 0; index < chars.length(); index++) {
            max = Math.max(max, chars.charAt(index));
        }
        return max;
    }
}