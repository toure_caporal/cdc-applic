package cdc.applic.expressions.parsing;

/**
 * Enumeration of possible token types found in an expression.
 * <p>
 * <b>WARNING:</b> Order of declarations matters.
 *
 * @author Damien Carbonne
 */
public enum TokenType {
    /** Open parenthesis: {@code '('} */
    OPEN_PAREN("("),

    /** Close parenthesis: {@code ')'} */
    CLOSE_PAREN(")"),

    // Start of binary operators
    /** Implication: 'imp' (case insensitive), {@code '->'} */ // TODO Add mathematical symbol
    IMPL("->"),

    /** Equivalence: 'iff" (case insensitive), {@code '<->'} */ // TODO Add mathematical symbol
    EQUIV("<->"),

    /** Logical or: 'or' (case insensitive), {@code '|'} */ // TODO Add mathematical symbol
    OR("|"),

    /** Logical and: 'and' (case insensitive), {@code '&'} */ // TODO Add mathematical symbol
    AND("&"),
    // End of binary operators

    /** Negation: 'not' (case insensitive), {@code '!'} */ // TODO Add mathematical symbol
    NOT("!"),

    /** Equality: {@code '='} */
    EQUAL("="),

    /** Inequality: {@code '!='} */ // TODO Add mathematical symbol
    NOT_EQUAL("!="),

    /** Is in set: {@code 'in'} (case insensitive), {@code '<:'} */ // TODO Add mathematical symbol
    IN("<:"),

    /** Is not in set: {@code 'not in'} (case insensitive), {@code '!<:'} */ // TODO Add mathematical symbol
    NOT_IN("!<:"),

    // Start of items
    /** Value, Reference of Property name. */
    TEXT(null),

    /** Value, Reference of Property name. */
    ESCAPED_TEXT(null),

    /** Integer value */
    INTEGER(null),

    /** Real value */
    REAL(null),

    /** Contradiction: : {@code 'false'} (case insensitive) */ // TODO Add mathematical symbol
    FALSE("false"),

    /** Tautology: {@code 'true'} (case insensitive) */ // TODO Add mathematical symbol
    TRUE("true"),
    // End of items

    /** Range bounds separator: {@code '~'}, {@code 'to'} */
    TO("~"),

    /** values and ranges separator in sets: {@code ','} */
    VALUES_SEP(","),

    /** Open set: <code>'{'</code> */
    OPEN_SET("{"),

    /** Close set: {@code '}'} */
    CLOSE_SET("}"),

    EPSILON(null);

    private final String symbol;

    private TokenType(String symbol) {
        this.symbol = symbol;
    }

    /**
     * @return The associated symbol or {@code null}.
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * Returns {@code true} when this TokenType is considered binary operator from a node perspective.
     * <p>
     * The answer is positive for AND, OR, IMPL and EQUIV.<br>
     * EQUAL, NOT_EQUAL, IN and NOT_IN are not handled as binary nodes by this
     * implementation.
     *
     * @return {@code true} when this TokenType is considered binary.
     */
    public boolean isBinary() {
        // This seems to be the fastest way to yield this result.
        // Accessing static variables seems longer.
        // Using switch was longer
        // Using explicit equality and || was also longer.
        // This type of implementation has a constant time.
        return this.ordinal() >= IMPL.ordinal() && this.ordinal() <= AND.ordinal();
    }

    /**
     * Returns {@code true} when this TokenType is a considered as an item.
     * <p>
     * The answer is positive for TEXT, ESCAPED_TEXT, INTEGER, REAL, FALSE and TRUE.
     *
     * @return {@code true} when this TokenType is a considered as an item.
     */
    public boolean isItem() {
        return this.ordinal() >= TEXT.ordinal() && this.ordinal() <= TRUE.ordinal();
    }

    /**
     * Returns {@code true} when this TokenType supports Range.
     * <p>
     * This is the case for: INTEGER and REAL.
     * TODO extend to Strings ?
     *
     * @return {@code true} when this TokenType supports Range.
     */
    public boolean supportsRange() {
        return this == INTEGER || this == REAL;
    }
}