package cdc.applic.expressions.parsing;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.ImplicationNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotEqualNode;
import cdc.applic.expressions.ast.NotInNode;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.ParsingNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.content.Item;
import cdc.applic.expressions.content.ItemSet;
import cdc.applic.expressions.content.ItemSetUtils;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.issues.InvalidSyntaxException;
import cdc.applic.expressions.literals.Name;

public class Parser extends AbstractParser {
    /** The operators stack. */
    private final Deque<OperatorType> operators = new ArrayDeque<>();
    /** The operands stack. */
    private final Deque<ParsingNode> operands = new ArrayDeque<>();

    public Parser() {
        super();
    }

    /**
     * Parses an expression and returns its syntax tree.
     *
     * @param expression The expression to parse.
     * @return The corresponding syntax tree, if expression is valid.
     * @throws InvalidSyntaxException When the expression is invalid.
     */
    public ParsingNode parse(String expression) {
        tokenizer.init(expression);
        operands.clear();
        operators.clear();
        operators.push(OperatorType.SENTINEL);
        tokenizer.next();
        parseExpr();
        expect("parse", TokenType.EPSILON);
        return operands.getFirst();
    }

    private static OperatorType convert(TokenType type) {
        switch (type) {
        case AND:
            return OperatorType.AND;
        case OR:
            return OperatorType.OR;
        case NOT:
            return OperatorType.NOT;
        case EQUIV:
            return OperatorType.EQUIVALENCE;
        case IMPL:
            return OperatorType.IMPLICATION;
        default:
            throw new IllegalArgumentException("Can not convert " + type + " to OperatorType");
        }
    }

    private void popOperator() {
        switch (operators.getFirst()) {
        case AND:
            popAnd();
            break;

        case OR:
            popOr();
            break;

        case IMPLICATION:
            popImplication();
            break;

        case EQUIVALENCE:
            popEquivalence();
            break;

        case NOT:
            popNot();
            break;

        default:
            throw new IllegalArgumentException("Can not pop " + operators.getFirst());
        }
    }

    private void popAnd() {
        operators.removeFirst();
        final Node right = operands.removeFirst();
        final Node left = operands.removeFirst();
        final AndNode node = new AndNode(left, right);
        operands.push(node);
    }

    private void popOr() {
        operators.removeFirst();
        final Node right = operands.removeFirst();
        final Node left = operands.removeFirst();
        final OrNode node = new OrNode(left, right);
        operands.push(node);
    }

    private void popImplication() {
        operators.removeFirst();
        final Node right = operands.removeFirst();
        final Node left = operands.removeFirst();
        final ImplicationNode node = new ImplicationNode(left, right);
        operands.push(node);
    }

    private void popEquivalence() {
        operators.removeFirst();
        final Node right = operands.removeFirst();
        final Node left = operands.removeFirst();
        final EquivalenceNode node = new EquivalenceNode(left, right);
        operands.push(node);
    }

    private void popNot() {
        operators.removeFirst();
        final Node right = operands.removeFirst();
        final NotNode node = new NotNode(right);
        operands.push(node);
    }

    private void pushOperator(TokenType type) {
        final OperatorType op = convert(type);
        while (operators.getFirst().getPrecedence() >= op.getPrecedence()) {
            popOperator();
        }
        operators.push(op);
    }

    private void parseExpr() {
        parsePExpr();
        while (tokenizer.getTokenType().isBinary()) {
            pushOperator(tokenizer.getTokenType());
            tokenizer.next();
            parsePExpr();
        }
        while (operators.getFirst() != OperatorType.SENTINEL) {
            popOperator();
        }
    }

    private void parsePExpr() {
        switch (tokenizer.getTokenType()) {
        case OPEN_PAREN:
            parseOpenParen();
            break;
        case NOT:
            operators.push(OperatorType.NOT);
            tokenizer.next();
            parseExpr();
            break;
        case TRUE:
            operands.push(TrueNode.INSTANCE);
            tokenizer.next();
            break;
        case FALSE:
            operands.push(FalseNode.INSTANCE);
            tokenizer.next();
            break;
        case TEXT:
        case ESCAPED_TEXT:
            parseBaseExpr();
            break;
        default:
            throw newInvalidExpression(InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN,
                                       "Unexpected token: " + tokenizer.getToken() + " in: '" + tokenizer.getExpression() + "' parsePExpr");
        }
    }

    private void parseOpenParen() {
        tokenizer.next();
        operators.push(OperatorType.SENTINEL);
        parseExpr();
        expect("parseOpenParen", TokenType.CLOSE_PAREN);
        operators.pop();
    }

    private void parseBaseExpr() {
        final Name label = new Name(tokenizer.getUnescapedText(), false);
        tokenizer.next();
        switch (tokenizer.getTokenType()) {
        case EQUAL:
            parseEqual(label);
            break;

        case NOT_EQUAL:
            parseNotEqual(label);
            break;

        case IN:
            parseIn(label);
            break;

        case NOT_IN:
            parseNotIn(label);
            break;

        default:
            operands.push(new RefNode(label));
            break;
        }
    }

    private void parseEqual(Name popertyName) {
        tokenizer.next();
        final Value value = ItemsParsing.createValue(tokenizer.getToken(), tokenizer.getExpression());
        expect("parseEqual", ITEM_TOKEN_TYPES);
        operands.push(new EqualNode(popertyName, value));
    }

    private void parseNotEqual(Name propertyName) {
        tokenizer.next();
        final Value value = ItemsParsing.createValue(tokenizer.getToken(), tokenizer.getExpression());
        expect("parseNotEqual", ITEM_TOKEN_TYPES);
        operands.push(new NotEqualNode(propertyName, value));
    }

    private void parseIn(Name propertyName) {
        tokenizer.next();
        final ItemSet items = parseItemSet();
        operands.push(new InNode(propertyName, items));
    }

    private void parseNotIn(Name propertyName) {
        tokenizer.next();
        final ItemSet items = parseItemSet();
        operands.push(new NotInNode(propertyName, items));
    }

    private ItemSet parseItemSet() {
        expect("parseItemSet", TokenType.OPEN_SET);
        final List<Item> items = new ArrayList<>();

        if (tokenizer.getTokenType().isItem()) {
            items.add(parseItem());
        }

        while (tokenizer.getTokenType() == TokenType.VALUES_SEP) {
            tokenizer.next();
            items.add(parseItem());
        }

        expect("parseItemSet", TokenType.CLOSE_SET);
        return ItemSetUtils.toBestSet(items);
    }

    private Item parseItem() {
        final TokenType lowType = tokenizer.getTokenType();
        final Value low = ItemsParsing.createValue(tokenizer.getToken(), tokenizer.getExpression());
        tokenizer.next();
        if (tokenizer.getTokenType() == TokenType.TO && lowType.supportsRange()) {
            tokenizer.next();
            final Value high = ItemsParsing.createValue(tokenizer.getToken(), tokenizer.getExpression());
            expect("parseItem", lowType);
            return ItemsParsing.createRange(low, high, tokenizer.getExpression());
        } else {
            return low;
        }
    }
}