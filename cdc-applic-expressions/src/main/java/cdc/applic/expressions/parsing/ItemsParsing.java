package cdc.applic.expressions.parsing;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.Item;
import cdc.applic.expressions.content.Range;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.issues.InvalidSyntaxException;

public final class ItemsParsing {
    private ItemsParsing() {
    }

    private static InvalidSyntaxException unexpectedToken(String content,
                                                          Token token) {
        return new InvalidSyntaxException(InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN,
                                          "[" + content + "] unexpected token: " + token);
    }

    private static InvalidSyntaxException unexpectedEnd(String content) {
        return new InvalidSyntaxException(InvalidSyntaxException.Detail.PARSE_UNEXPECTED_END,
                                          "[" + content + "] unexpected end");
    }

    private enum Status {
        /** At parsing start */
        START,
        /** After Range */
        RANGE,
        /** After (first) value */
        VALUE,
        /** After values Separator */
        VALUES_SEP,
        /** After Range symbol */
        TO
    }

    public static Value createValue(Token token,
                                    String expression) {
        try {
            return createValue(token);
        } catch (final IllegalArgumentException e) {
            throw new InvalidSyntaxException(InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN,
                                             e.getMessage(),
                                             expression);
        }
    }

    private static Value createValue(Token token) {
        switch (token.getType()) {
        case ESCAPED_TEXT:
        case TEXT:
            return StringValue.create(token.getUnescapedText(), false);
        case INTEGER:
            return IntegerValue.create(token.getUnescapedText());
        case REAL:
            return RealValue.create(token.getUnescapedText());
        case TRUE:
            return BooleanValue.TRUE;
        case FALSE:
            return BooleanValue.FALSE;
        default:
            throw new IllegalArgumentException("Can not create a value with " + token);
        }
    }

    public static Range createRange(Value low,
                                    Value high,
                                    String expression) {
        try {
            return createRange(low, high);
        } catch (final IllegalArgumentException e) {
            throw new InvalidSyntaxException(InvalidSyntaxException.Detail.PARSE_INVALID_RANGE,
                                             e.getMessage(),
                                             expression);
        }
    }

    public static Range createRange(Value low,
                                    Value high) {
        if (low.getKind() == high.getKind()) {
            switch (low.getKind()) {
            case INTEGER:
                return IntegerRange.create(((IntegerValue) low).getNumber(),
                                           ((IntegerValue) high).getNumber());
            case REAL:
                return RealRange.create(((RealValue) low).getNumber(),
                                        ((RealValue) high).getNumber());
            default:
                throw new IllegalArgumentException("Can not create a range with " + low + " and " + high);
            }
        } else {
            throw new IllegalArgumentException("Can not create a range with " + low + " and " + high);
        }
    }

    public static List<BooleanValue> toBooleanValues(String content) {
        final List<BooleanValue> result = new ArrayList<>();
        final Tokenizer tokenizer = new Tokenizer();
        tokenizer.init(content);
        Status status = Status.START;
        while (tokenizer.hasMoreTokens()) {
            final Token token = tokenizer.nextToken();
            final BooleanValue value;
            switch (token.getType()) {
            case FALSE:
                if (status == Status.VALUE) {
                    throw unexpectedToken(content, token);
                } else {
                    value = BooleanValue.FALSE;
                    status = Status.VALUE;
                }
                break;

            case TRUE:
                if (status == Status.VALUE) {
                    throw unexpectedToken(content, token);
                } else {
                    value = BooleanValue.TRUE;
                    status = Status.VALUE;
                }
                break;

            case VALUES_SEP:
                if (status == Status.VALUES_SEP || status == Status.START) {
                    throw unexpectedToken(content, token);
                } else {
                    value = null;
                    status = Status.VALUES_SEP;
                }
                break;
            default:
                throw unexpectedToken(content, token);
            }

            if (value != null && !result.contains(value)) {
                result.add(value);
            }
        }
        if (status == Status.VALUES_SEP) {
            throw unexpectedEnd(content);
        }
        return result;
    }

    public static List<StringValue> toStringValues(String content) {
        final List<StringValue> result = new ArrayList<>();

        Status status = Status.START;
        final Tokenizer tokenizer = new Tokenizer();
        tokenizer.init(content);
        while (tokenizer.hasMoreTokens()) {
            final Token token = tokenizer.nextToken();
            final StringValue value;
            switch (token.getType()) {
            case ESCAPED_TEXT:
            case TEXT:
                if (status == Status.VALUE) {
                    throw unexpectedToken(content, token);
                } else {
                    value = StringValue.create(token.getUnescapedText(), false);
                    status = Status.VALUE;
                }
                break;

            case VALUES_SEP:
                if (status == Status.VALUES_SEP || status == Status.START) {
                    throw unexpectedToken(content, token);
                } else {
                    value = null;
                    status = Status.VALUES_SEP;
                }
                break;

            default:
                throw unexpectedToken(content, token);
            }
            if (value != null && !result.contains(value)) {
                result.add(value);
            }
        }

        if (status == Status.VALUES_SEP) {
            throw unexpectedEnd(content);
        }

        return result;
    }

    public static List<IntegerRange> toIntegerItems(String content) {
        final List<IntegerRange> result = new ArrayList<>();

        Status status = Status.START;
        int low = 0;
        final Tokenizer tokenizer = new Tokenizer();
        tokenizer.init(content);
        while (tokenizer.hasMoreTokens()) {
            final Token token = tokenizer.nextToken();
            switch (token.getType()) {
            case INTEGER:
                if (status == Status.VALUE || status == Status.RANGE) {
                    throw unexpectedToken(content, token);
                } else {
                    final String text = token.getUnescapedText();
                    final IntegerValue value = IntegerValue.create(text);
                    final int number = value.getNumber();
                    if (status == Status.START || status == Status.VALUES_SEP) {
                        low = number;
                        status = Status.VALUE;
                    } else { // status == Status.TO
                        result.add(IntegerRange.create(low, number));
                        status = Status.RANGE;
                    }
                }
                break;

            case VALUES_SEP:
                if (status == Status.VALUE) {
                    result.add(IntegerRange.create(low));
                    status = Status.VALUES_SEP;
                } else if (status == Status.RANGE) {
                    status = Status.VALUES_SEP;
                } else {
                    throw unexpectedToken(content, token);
                }
                break;

            case TO:
                if (status == Status.VALUE) {
                    status = Status.TO;
                } else {
                    throw unexpectedToken(content, token);
                }
                break;

            default:
                throw unexpectedToken(content, token);
            }
        }

        if (status == Status.VALUE) {
            result.add(IntegerRange.create(low));
        } else if (status == Status.RANGE) {
            // Ignore
        } else if (status != Status.START) {
            throw unexpectedEnd(content);
        }
        return result;
    }

    public static List<RealRange> toRealItems(String content) {
        final List<RealRange> result = new ArrayList<>();

        Status status = Status.START;
        double low = 0.0;
        final Tokenizer tokenizer = new Tokenizer();
        tokenizer.init(content);
        while (tokenizer.hasMoreTokens()) {
            final Token token = tokenizer.nextToken();
            switch (token.getType()) {
            case REAL:
                if (status == Status.VALUE || status == Status.RANGE) {
                    throw unexpectedToken(content, token);
                } else {
                    final String text = token.getUnescapedText();
                    final RealValue value = RealValue.create(text);
                    final double number = value.getNumber();
                    if (status == Status.START || status == Status.VALUES_SEP) {
                        low = number;
                        status = Status.VALUE;
                    } else { // status == Status.TO
                        result.add(RealRange.create(low, number));
                        status = Status.RANGE;
                    }
                }
                break;

            case VALUES_SEP:
                if (status == Status.VALUE) {
                    result.add(RealRange.create(low));
                    status = Status.VALUES_SEP;
                } else if (status == Status.RANGE) {
                    status = Status.VALUES_SEP;
                } else {
                    throw unexpectedToken(content, token);
                }
                break;

            case TO:
                if (status == Status.VALUE) {
                    status = Status.TO;
                } else {
                    throw unexpectedToken(content, token);
                }
                break;

            default:
                throw unexpectedToken(content, token);
            }
        }

        if (status == Status.VALUE) {
            result.add(RealRange.create(low));
        } else if (status == Status.RANGE) {
            // Ignore
        } else if (status != Status.START) {
            throw unexpectedEnd(content);
        }
        return result;
    }

    public static List<Item> toItems(String content) {
        final List<Item> result = new ArrayList<>();

        Status status = Status.START;
        Value previousValue = null;
        final Tokenizer tokenizer = new Tokenizer();
        tokenizer.init(content);
        while (tokenizer.hasMoreTokens()) {
            final Token token = tokenizer.nextToken();
            switch (token.getType()) {
            case TEXT:
            case ESCAPED_TEXT:
            case TRUE:
            case FALSE:
            case INTEGER:
            case REAL:
                if (status == Status.VALUE) {
                    throw unexpectedToken(content, token);
                } else if (status == Status.START || status == Status.RANGE || status == Status.VALUES_SEP) {
                    previousValue = createValue(token, content);
                    status = Status.VALUE;
                } else { // status == Status.TO
                    final Value value = createValue(token, content);
                    final Range range = createRange(previousValue, value, content);
                    result.add(range);
                    status = Status.RANGE;
                }
                break;

            case VALUES_SEP:
                if (status == Status.VALUE) {
                    result.add(previousValue);
                    status = Status.VALUES_SEP;
                } else if (status == Status.RANGE) {
                    // Ignore
                } else {
                    throw unexpectedToken(content, token);
                }
                break;

            case TO:
                if (status == Status.VALUE) {
                    status = Status.TO;
                } else {
                    throw unexpectedToken(content, token);
                }
                break;

            default:
                throw unexpectedToken(content, token);
            }
        }

        if (status == Status.VALUE) {
            result.add(previousValue);
        } else if (status == Status.RANGE) {
            // Ignore
        } else if (status != Status.START) {
            throw unexpectedEnd(content);
        }
        return result;
    }
}