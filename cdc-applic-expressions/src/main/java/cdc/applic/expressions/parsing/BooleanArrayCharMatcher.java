package cdc.applic.expressions.parsing;

/**
 * CharSet implementation based on BitSet.
 * <p>
 * This is the fastest implementation, but the worst memory efficient.
 *
 * @author Damien Carbonne
 */
public final class BooleanArrayCharMatcher implements CharMatcher {
    private final boolean[] mask;
    private final int maskLength;

    public BooleanArrayCharMatcher(String chars) {
        mask = build(chars);
        maskLength = mask.length;
    }

    private static boolean[] build(String chars) {
        final int max = CharMatcher.max(chars);
        final boolean[] tmp = new boolean[max + 1];
        for (int index = 0; index < chars.length(); index++) {
            tmp[chars.charAt(index)] = true;
        }
        return tmp;
    }

    @Override
    public boolean matches(char c) {
        return c < maskLength ? mask[c] : false;
    }
}