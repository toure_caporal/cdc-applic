package cdc.applic.expressions.parsing;

import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import cdc.applic.expressions.issues.InvalidSyntaxException;

public abstract class AbstractParser {
    /** The tokenizer. */
    protected final Tokenizer tokenizer = new Tokenizer();

    /**
     * Tokens corresponding to items.
     * <p>
     * They can be used inside a set or as right member of an equality operator.
     */
    protected static final Set<TokenType> ITEM_TOKEN_TYPES;

    static {
        final Set<TokenType> tmp = new HashSet<>();
        for (final TokenType type : TokenType.values()) {
            if (type.isItem()) {
                tmp.add(type);
            }
        }
        ITEM_TOKEN_TYPES = Collections.unmodifiableSet(EnumSet.copyOf(tmp));
    }

    /**
     * Creates an InvalidExpression.
     *
     * @param detail The exception detail.
     * @param info Information describing the error.
     * @return A new InvalidExpression instance initialized with {@code detail} and {@code info}.
     */
    protected final InvalidSyntaxException newInvalidExpression(InvalidSyntaxException.Detail detail,
                                                                String info) {
        return new InvalidSyntaxException(detail,
                                          info,
                                          tokenizer.getExpression(),
                                          tokenizer.getBeginIndex(),
                                          tokenizer.getEndIndex());
    }

    /**
     * Checks that next token has an expected type.
     * <p>
     * If so, consumes it. Otherwise, generates an error.
     *
     * @param context A string describing the context where this method is
     *            called. Used in case of error.
     * @param type The expected token type.
     * @throws InvalidSyntaxException When the expression is invalid.
     */
    protected final void expect(String context,
                                TokenType type) {
        if (tokenizer.getTokenType() == type) {
            tokenizer.next();
        } else {
            throw newInvalidExpression(InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN,
                                       "[" + context + "] Unexpected token: " + tokenizer.getToken() + ", expected: " + type);
        }
    }

    /**
     * Checks that next token has an expected type among several ones.
     * <p>
     * If so, consumes it. Otherwise,generates an error.
     *
     * @param context A string describing the context where this method is
     *            called. Used in case of error.
     * @param types The expected token types.
     * @throws InvalidSyntaxException When the expression is invalid.
     */
    protected final void expect(String context,
                                Set<TokenType> types) {
        if (types.contains(tokenizer.getTokenType())) {
            tokenizer.next();
            return;
        }

        // No matching token found
        final StringBuilder builder = new StringBuilder();
        builder.append('[');
        builder.append(context);
        builder.append("] Unexpected token: ");
        builder.append(tokenizer.getToken());
        builder.append(", expected one of:");
        for (final TokenType type : types) {
            builder.append(" ");
            builder.append(type);
        }
        throw newInvalidExpression(InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN,
                                   builder.toString());
    }
}