package cdc.applic.expressions.parsing;

public interface CharMapper<V> {
    public V get(char c);
}