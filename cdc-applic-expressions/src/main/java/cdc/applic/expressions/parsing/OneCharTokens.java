package cdc.applic.expressions.parsing;

import cdc.util.lang.UnexpectedValueException;

/**
 * Recognition of tokens that hold on one character.
 *
 * @author Damien Carbonne
 */
public final class OneCharTokens {
    /**
     * String containing characters that are considered as a representing a token.
     */
    protected static final String CHARS = "(){},~=&|¬∧∨∈∉≠→↔⊤⊥";

    /**
     * Perfect hash table for {@link #CHARS}.
     * <p>
     * '(' is used as the filling character.<br>
     * <b>WARNING:</b> This is computed and must match {@link #CHARS}.
     */
    protected static final String STABLE = "≠()(,({|}~(((((¬∧∨(=→↔∈∉(⊤⊥((((&";

    /**
     * Conversion of {@link #STABLE} as a char array.
     */
    protected static final char[] TABLE = STABLE.toCharArray();

    /**
     * Mapping from {@link #STABLE} to token types.
     * <p>
     * <b>WARNING:</b> This is computed and must match {@link #CHARS} and {@link #STABLE}.
     */
    protected static final TokenType[] SMAP = {
            TokenType.NOT_EQUAL,
            TokenType.OPEN_PAREN,
            TokenType.CLOSE_PAREN,
            null,
            TokenType.VALUES_SEP,
            null,
            TokenType.OPEN_SET,
            TokenType.OR,
            TokenType.CLOSE_SET,
            TokenType.TO,
            null,
            null,
            null,
            null,
            null,
            TokenType.NOT,
            TokenType.AND,
            TokenType.OR,
            null,
            TokenType.EQUAL,
            TokenType.IMPL,
            TokenType.EQUIV,
            TokenType.IN,
            TokenType.NOT_IN,
            null,
            TokenType.TRUE,
            TokenType.FALSE,
            null,
            null,
            null,
            null,
            TokenType.AND
    };

    /**
     * The multiplier to use for {@link #TABLE}.
     */
    protected static final int MULTIPLY = 112259371;

    /**
     * The shift to use for {@link #TABLE}.
     */
    protected static final int SHIFT = 27;

    private OneCharTokens() {
    }

    public static final CharMatcher MULTIPLY_SHIFT_MATCHER =
            new MultiplyShiftCharMatcher(STABLE,
                                         MULTIPLY,
                                         SHIFT);

    public static final CharMatcher MULTIPLY_SHIFT_INLINE_MATCHER =
            new CharMatcher() {
                @Override
                public boolean matches(char c) {
                    return TABLE[(c * MULTIPLY) >>> SHIFT] == c;
                }
            };

    public static final CharMatcher BIT_SET_MATCHER =
            new BitSetCharMatcher(CHARS);

    public static final CharMatcher BOOLEAN_ARRAY_MATCHER =
            new BooleanArrayCharMatcher(CHARS);

    /**
     * Matcher that must be used to identify one character tokens.
     */
    public static final CharMatcher BEST_MATCHER = MULTIPLY_SHIFT_INLINE_MATCHER;

    public static final CharMapper<TokenType> SWITCH_MAPPER =
            new CharMapper<TokenType>() {
                @Override
                public TokenType get(char c) {
                    switch (c) {
                    case '(':
                        return TokenType.OPEN_PAREN;
                    case ')':
                        return TokenType.CLOSE_PAREN;
                    case '{':
                        return TokenType.OPEN_SET;
                    case '}':
                        return TokenType.CLOSE_SET;
                    case ',':
                        return TokenType.VALUES_SEP;
                    case '~':
                        return TokenType.TO;
                    case '=':
                        return TokenType.EQUAL;
                    case '≠':
                        return TokenType.NOT_EQUAL;
                    case '&':
                    case '∧':
                        return TokenType.AND;
                    case '|':
                    case '∨':
                        return TokenType.OR;
                    case '¬':
                        return TokenType.NOT;
                    case '∈':
                        return TokenType.IN;
                    case '∉':
                        return TokenType.NOT_IN;
                    case '→':
                        return TokenType.IMPL;
                    case '↔':
                        return TokenType.EQUIV;
                    case '⊤':
                        return TokenType.TRUE;
                    case '⊥':
                        return TokenType.FALSE;
                    default:
                        throw new UnexpectedValueException("No token type associated to " + c);
                    }
                }
            };

    public static final CharMapper<TokenType> MULTIPLY_SHIFT_INLINE_MAPPER =
            new CharMapper<TokenType>() {
                @Override
                public TokenType get(char c) {
                    return SMAP[(c * MULTIPLY) >>> SHIFT];
                }
            };

    /**
     * Mapper that must be used to map one character token type to their corresponding token type.
     */
    public static final CharMapper<TokenType> BEST_MAPPER = MULTIPLY_SHIFT_INLINE_MAPPER;
}