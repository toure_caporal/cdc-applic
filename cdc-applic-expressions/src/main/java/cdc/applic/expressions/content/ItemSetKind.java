package cdc.applic.expressions.content;

public enum ItemSetKind {
    UNCHECKED,
    BOOLEAN,
    INTEGER,
    REAL,
    STRING
}