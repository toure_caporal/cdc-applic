package cdc.applic.expressions.content;

import cdc.applic.expressions.literals.EscapingContext;
import cdc.applic.expressions.literals.EscapingUtils;

public final class StringValue implements StringItem, Value {
    /** Non escaped literal. */
    private final String nonEscaped;

    private StringValue(String literal,
                        boolean escaped) {
        this.nonEscaped = EscapingUtils.toNonEscaped(literal, escaped);
    }

    public static StringValue create(String literal,
                                     boolean escaped) {
        return new StringValue(literal, escaped);
    }

    @Override
    public ItemKind getKind() {
        return ItemKind.STRING;
    }

    @Override
    public String getProtectedLiteral() {
        return EscapingUtils.escapeIfNeeded(nonEscaped, EscapingContext.NAME);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof StringValue)) {
            return false;
        }
        final StringValue other = (StringValue) object;
        return nonEscaped.equals(other.nonEscaped);
    }

    @Override
    public int hashCode() {
        return nonEscaped.hashCode();
    }

    @Override
    public String toString() {
        return getProtectedLiteral();
    }
}