package cdc.applic.expressions.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import cdc.applic.expressions.parsing.ItemsParsing;
import cdc.util.lang.Checks;

public final class StringSet extends AbstractItemSet {
    private static final List<StringValue> NO_VALUES = Collections.unmodifiableList(new ArrayList<>());

    /**
     * Unmodifiable list (check this in constructors).
     */
    private final List<StringValue> values;

    public static final StringSet EMPTY = new StringSet(NO_VALUES);

    private StringSet(String content) {
        this(ItemsParsing.toStringValues(content));
    }

    private StringSet(StringValue... values) {
        Checks.isNotNull(values, "values");

        if (values.length == 0) {
            this.values = NO_VALUES;
        } else {
            final List<StringValue> tmp = new ArrayList<>();
            for (final StringValue value : values) {
                if (value == null) {
                    throw new IllegalArgumentException("Null value");
                }
                if (!tmp.contains(value)) {
                    tmp.add(value);
                }
            }
            this.values = Collections.unmodifiableList(tmp);
        }
    }

    private StringSet(Collection<? extends StringValue> values) {
        this(values.toArray(new StringValue[values.size()]));
    }

    private StringSet(List<StringValue> values) {
        this.values = Collections.unmodifiableList(values);
    }

    public static StringSet convert(ItemSet set) {
        if (set instanceof StringSet) {
            return (StringSet) set;
        } else if (set.isEmpty()) {
            return EMPTY;
        } else if (set.isCompliantWith(ItemSetKind.STRING)) {
            return new StringSet(convert(set.getItems(), StringValue.class));
        } else {
            throw new IllegalArgumentException("Can not convert this set to " + StringSet.class.getSimpleName());
        }
    }

    public static StringSet create(String content) {
        return new StringSet(content);
    }

    public static StringSet create(StringValue... values) {
        return new StringSet(values);
    }

    public static StringSet create(Collection<? extends StringValue> values) {
        return new StringSet(values);
    }

    @Override
    public ItemSetKind getKind() {
        return ItemSetKind.STRING;
    }

    @Override
    public StringSet getChecked() {
        return this;
    }

    @Override
    public boolean isEmpty() {
        return values.isEmpty();
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public boolean isChecked() {
        return true;
    }

    @Override
    public List<StringValue> getItems() {
        return values;
    }

    @Override
    public boolean contains(Item item) {
        Checks.isNotNull(item, ITEM);

        if (item instanceof StringValue) {
            return contains((StringValue) item);
        } else {
            throw nonSupportedItem(item);
        }
    }

    @Override
    public StringSet union(Item item) {
        Checks.isNotNull(item, ITEM);

        if (item instanceof StringValue) {
            return union((StringValue) item);
        } else {
            throw nonSupportedItem(item);
        }
    }

    @Override
    public StringSet union(ItemSet other) {
        Checks.isNotNull(other, OTHER);

        if (other.isCompliantWith(getKind())) {
            return union(convert(other));
        } else {
            throw nonSupportedSet(other);
        }
    }

    @Override
    public StringSet intersection(Item item) {
        Checks.isNotNull(item, ITEM);

        if (item instanceof StringValue) {
            return intersection((StringValue) item);
        } else {
            throw nonSupportedItem(item);
        }
    }

    @Override
    public StringSet intersection(ItemSet other) {
        Checks.isNotNull(other, OTHER);

        if (other.isCompliantWith(getKind())) {
            return intersection(convert(other));
        } else {
            throw nonSupportedSet(other);
        }
    }

    @Override
    public StringSet remove(Item item) {
        Checks.isNotNull(item, ITEM);

        if (item instanceof StringValue) {
            return remove((StringValue) item);
        } else {
            throw nonSupportedItem(item);
        }
    }

    @Override
    public StringSet remove(ItemSet other) {
        Checks.isNotNull(other, OTHER);

        if (other.isCompliantWith(getKind())) {
            return remove(convert(other));
        } else {
            throw nonSupportedSet(other);
        }
    }

    public boolean contains(StringValue value) {
        return values.contains(value);
    }

    public boolean contains(StringSet other) {
        return this.values.containsAll(other.values);
    }

    public StringSet union(StringValue value) {
        Checks.isNotNull(value, VALUE);

        if (contains(value)) {
            return this;
        }
        final List<StringValue> tmp = new ArrayList<>();
        tmp.addAll(this.values);
        tmp.add(value);
        return new StringSet(tmp);
    }

    public StringSet union(StringSet other) {
        Checks.isNotNull(other, OTHER);

        if (contains(other)) {
            return this;
        }
        final List<StringValue> tmp = new ArrayList<>();
        tmp.addAll(this.values);
        for (final StringValue value : other.values) {
            if (!tmp.contains(value)) {
                tmp.add(value);
            }
        }
        return new StringSet(tmp);
    }

    public StringSet intersection(StringValue value) {
        Checks.isNotNull(value, VALUE);

        if (contains(value)) {
            return new StringSet(value);
        } else {
            return EMPTY;
        }
    }

    public StringSet intersection(StringSet other) {
        Checks.isNotNull(other, OTHER);

        final List<StringValue> tmp = new ArrayList<>();
        for (final StringValue value : getItems()) {
            if (other.contains(value)) {
                tmp.add(value);
            }
        }
        if (tmp.isEmpty()) {
            return EMPTY;
        } else {
            return new StringSet(tmp);
        }
    }

    public StringSet remove(StringValue value) {
        Checks.isNotNull(value, VALUE);

        if (!contains(value)) {
            return this;
        }
        final List<StringValue> tmp = new ArrayList<>();
        tmp.addAll(this.values);
        tmp.remove(value);
        return new StringSet(tmp);
    }

    public StringSet remove(StringSet other) {
        Checks.isNotNull(other, OTHER);

        final List<StringValue> tmp = new ArrayList<>();
        tmp.addAll(this.values);
        tmp.removeAll(other.values);
        if (tmp.isEmpty()) {
            return EMPTY;
        } else {
            return new StringSet(tmp);
        }
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (isEmpty()
                && object instanceof ItemSet
                && ((ItemSet) object).isEmpty()) {
            return true;
        }
        if (!(object instanceof StringSet)) {
            return false;
        }
        final StringSet other = (StringSet) object;
        return values.equals(other.values);
    }

    @Override
    public int hashCode() {
        return values.hashCode();
    }
}