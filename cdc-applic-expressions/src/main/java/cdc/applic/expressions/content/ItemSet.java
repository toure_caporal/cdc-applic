package cdc.applic.expressions.content;

import java.util.Collection;

/**
 * Interface implemented by set of items.
 *
 * @author Damien Carbonne
 */
public interface ItemSet {

    /**
     * @return The item set kind.
     */
    public ItemSetKind getKind();

    /**
     * @return The checked version of this set.
     */
    public ItemSet getChecked();

    /**
     * @return A BooleanSet view of this set.
     */
    public BooleanSet toBooleanSet();

    /**
     * @return An IntegerSet view of this set.
     */
    public IntegerSet toIntegerSet();

    /**
     * @return A RealSet view of this set.
     */
    public RealSet toRealSet();

    /**
     * @return A StringSet view of this set.
     */
    public StringSet toStringSet();

    /**
     * @return {@code true} if this set is empty.
     */
    public boolean isEmpty();

    /**
     * @return {@code true} if this set is a singleton.
     */
    public boolean isSingleton();

    /**
     * Returns {@code true} if this set is compliant with an item set kind.
     *
     * @param kind The kind.
     * @return {@code true} if this set is compliant with {@code kind}.
     */
    public boolean isCompliantWith(ItemSetKind kind);

    /**
     * Returns {@code true} if this set is valid.
     * <p>
     * This is the case for checked sets.
     *
     * @return {@code true} if this set is valid.
     */
    public boolean isValid();

    /**
     * @return {@code true} if this set is checked.
     */
    public boolean isChecked();

    /**
     * @return A collection of items contained in this set.
     */
    public Collection<? extends Item> getItems();

    /**
     * @return The string representation of the content of this set.
     */
    public String getContent();

    /**
     * Returns {@code true} if this set contains an item.
     *
     * @param item The item.
     * @return {@code true} if this set contains {@code item}.
     */
    public boolean contains(Item item);

    /**
     * Returns {@code true} if this set contains an item set.
     *
     * @param other The item set.
     * @return {@code true} if this set contains {@code other}.
     */
    public boolean contains(ItemSet other);

    /**
     * Returns the union of this set with an item.
     *
     * @param item The item.
     * @return the union of this set with {@code item}.
     */
    public ItemSet union(Item item);

    /**
     * Returns the union of this set with an item set.
     *
     * @param other The item set.
     * @return the union of this set with {@code other}.
     */
    public ItemSet union(ItemSet other);

    /**
     * Returns the intersection of this set with an item.
     *
     * @param item The item.
     * @return the intersection of this set with {@code item}.
     */
    public ItemSet intersection(Item item);

    /**
     * Returns the intersection of this set with an item set.
     *
     * @param other The item set.
     * @return the intersection of this set with {@code other}.
     */
    public ItemSet intersection(ItemSet other);

    /**
     * Returns the content of this set excluding an item.
     *
     * @param item The item.
     * @return The content of this set excluding {@code item}.
     */
    public ItemSet remove(Item item);

    /**
     * Returns the content of this set excluding an item set.
     *
     * @param other The item set.
     * @return The content of this set excluding {@code other}.
     */
    public ItemSet remove(ItemSet other);
}