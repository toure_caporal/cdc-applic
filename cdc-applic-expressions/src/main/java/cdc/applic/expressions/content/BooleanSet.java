package cdc.applic.expressions.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cdc.applic.expressions.parsing.ItemsParsing;
import cdc.util.lang.Checks;

public final class BooleanSet extends AbstractItemSet {
    private final Set<BooleanValue> values;

    public static final BooleanSet EMPTY = new BooleanSet();
    public static final BooleanSet FALSE = new BooleanSet(BooleanValue.FALSE);
    public static final BooleanSet TRUE = new BooleanSet(BooleanValue.TRUE);
    public static final BooleanSet FALSE_TRUE = new BooleanSet(BooleanValue.FALSE, BooleanValue.TRUE);

    private BooleanSet() {
        this.values = Collections.emptySet();
    }

    public BooleanSet(BooleanValue... values) {
        final Set<BooleanValue> tmp = new HashSet<>();
        for (final BooleanValue value : values) {
            tmp.add(value);
        }
        this.values = Collections.unmodifiableSet(tmp);
    }

    private static BooleanSet create(boolean hasFalse,
                                     boolean hasTrue) {
        if (hasTrue) {
            if (hasFalse) {
                return FALSE_TRUE;
            } else {
                return TRUE;
            }
        } else {
            if (hasFalse) {
                return FALSE;
            } else {
                return EMPTY;
            }
        }
    }

    public static BooleanSet create(String content) {
        return create(ItemsParsing.toBooleanValues(content));
    }

    public static BooleanSet create(BooleanValue... values) {
        boolean hasFalse = false;
        boolean hasTrue = false;
        for (final BooleanValue value : values) {
            if (BooleanValue.TRUE.equals(value)) {
                hasTrue = true;
            } else if (BooleanValue.FALSE.equals(value)) {
                hasFalse = true;
            }
        }
        return create(hasFalse, hasTrue);
    }

    public static BooleanSet create(Collection<BooleanValue> values) {
        final boolean hasTrue = values.contains(BooleanValue.TRUE);
        final boolean hasFalse = values.contains(BooleanValue.FALSE);
        return create(hasFalse, hasTrue);
    }

    public static BooleanSet convert(ItemSet set) {
        Checks.isNotNull(set, "set");
        if (set instanceof BooleanSet) {
            return (BooleanSet) set;
        } else if (set.isCompliantWith(ItemSetKind.BOOLEAN)) {
            return create(convert(set.getItems(), BooleanValue.class));
        } else {
            throw new IllegalArgumentException("Can not convert this set to " + BooleanSet.class.getSimpleName());
        }
    }

    @Override
    public ItemSetKind getKind() {
        return ItemSetKind.BOOLEAN;
    }

    @Override
    public ItemSet getChecked() {
        return this;
    }

    @Override
    public boolean isEmpty() {
        return values.isEmpty();
    }

    public boolean isFull() {
        return values.size() == 2;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public boolean isChecked() {
        return true;
    }

    @Override
    public Collection<? extends BooleanValue> getItems() {
        return values;
    }

    @Override
    public boolean contains(Item item) {
        Checks.isNotNull(item, ITEM);

        if (item instanceof BooleanValue) {
            return contains((BooleanValue) item);
        } else {
            return false;
        }
    }

    @Override
    public BooleanSet union(Item item) {
        Checks.isNotNull(item, ITEM);

        if (item instanceof BooleanValue) {
            return union((BooleanValue) item);
        } else {
            throw nonSupportedItem(item);
        }
    }

    @Override
    public BooleanSet union(ItemSet other) {
        Checks.isNotNull(other, OTHER);

        if (other.isCompliantWith(getKind())) {
            return union(convert(other));
        } else {
            throw nonSupportedSet(other);
        }
    }

    @Override
    public BooleanSet intersection(Item item) {
        Checks.isNotNull(item, ITEM);

        if (item instanceof BooleanValue) {
            return intersection((BooleanValue) item);
        } else {
            throw nonSupportedItem(item);
        }
    }

    @Override
    public BooleanSet intersection(ItemSet other) {
        Checks.isNotNull(other, OTHER);

        if (other.isCompliantWith(getKind())) {
            return intersection(convert(other));
        } else {
            throw nonSupportedSet(other);
        }
    }

    @Override
    public BooleanSet remove(Item item) {
        Checks.isNotNull(item, ITEM);

        if (item instanceof BooleanValue) {
            return remove((BooleanValue) item);
        } else {
            throw nonSupportedItem(item);
        }
    }

    @Override
    public BooleanSet remove(ItemSet other) {
        Checks.isNotNull(other, OTHER);

        if (other.isCompliantWith(getKind())) {
            return remove(convert(other));
        } else {
            throw nonSupportedSet(other);
        }
    }

    public boolean contains(BooleanValue value) {
        Checks.isNotNull(value, VALUE);

        return values.contains(value);
    }

    public boolean contains(BooleanSet other) {
        Checks.isNotNull(other, OTHER);

        return this.values.containsAll(other.values);
    }

    public BooleanSet union(BooleanValue value) {
        Checks.isNotNull(value, VALUE);

        if (contains(value)) {
            return this;
        }
        final List<BooleanValue> tmp = new ArrayList<>();
        tmp.addAll(this.values);
        tmp.add(value);
        return create(tmp);
    }

    public BooleanSet union(BooleanSet other) {
        Checks.isNotNull(other, OTHER);

        if (contains(other)) {
            return this;
        }
        final List<BooleanValue> tmp = new ArrayList<>();
        tmp.addAll(this.values);
        for (final BooleanValue value : other.values) {
            if (!tmp.contains(value)) {
                tmp.add(value);
            }
        }
        return create(tmp);
    }

    public BooleanSet intersection(BooleanValue value) {
        Checks.isNotNull(value, VALUE);

        if (contains(value)) {
            return create(value);
        } else {
            return EMPTY;
        }
    }

    public BooleanSet intersection(BooleanSet other) {
        Checks.isNotNull(other, OTHER);

        final List<BooleanValue> tmp = new ArrayList<>();
        for (final BooleanValue value : getItems()) {
            if (other.contains(value)) {
                tmp.add(value);
            }
        }
        if (tmp.isEmpty()) {
            return EMPTY;
        } else {
            return create(tmp);
        }
    }

    public BooleanSet remove(BooleanValue value) {
        Checks.isNotNull(value, VALUE);

        if (!contains(value)) {
            return this;
        }
        final Set<BooleanValue> tmp = new HashSet<>();
        tmp.addAll(this.values);
        tmp.remove(value);
        return create(tmp);
    }

    public BooleanSet remove(BooleanSet other) {
        Checks.isNotNull(other, OTHER);

        final List<BooleanValue> tmp = new ArrayList<>();
        tmp.addAll(this.values);
        tmp.removeAll(other.values);
        if (tmp.isEmpty()) {
            return EMPTY;
        } else {
            return create(tmp);
        }
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (isEmpty() && object instanceof ItemSet) {
            final ItemSet other = (ItemSet) object;
            if (other.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return isEmpty() ? 0 : values.hashCode();
    }
}