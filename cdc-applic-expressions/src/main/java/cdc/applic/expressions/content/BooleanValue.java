package cdc.applic.expressions.content;

import cdc.applic.expressions.literals.LiteralUtils;

public final class BooleanValue implements BooleanItem, Value {
    private final boolean value;

    public static final BooleanValue FALSE = new BooleanValue(false);
    public static final BooleanValue TRUE = new BooleanValue(true);
    public static final BooleanValue[] VALUES = { FALSE, TRUE };

    private BooleanValue(boolean value) {
        this.value = value;
    }

    public static BooleanValue create(boolean value) {
        return value ? TRUE : FALSE;
    }

    public static BooleanValue create(String literal) {
        if (!LiteralUtils.isBooleanLiteral(literal)) {
            throw new IllegalArgumentException("Illegal boolean literal (" + literal + ")");
        }
        return create(Boolean.parseBoolean(literal));
    }

    public BooleanValue negate() {
        return this == TRUE ? FALSE : TRUE;
    }

    @Override
    public ItemKind getKind() {
        return ItemKind.BOOLEAN;
    }

    @Override
    public String getProtectedLiteral() {
        return value ? "true" : "false";
    }

    public boolean getValue() {
        return value;
    }

    @Override
    public boolean equals(Object object) {
        // Booleans are cached and there are only 2 values.
        // So we can use this simple approach
        return this == object;
    }

    @Override
    public int hashCode() {
        return Boolean.hashCode(value);
    }

    @Override
    public String toString() {
        return getProtectedLiteral();
    }
}