package cdc.applic.expressions.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cdc.applic.expressions.parsing.TokenType;
import cdc.util.lang.Checks;

public abstract class AbstractItemSet implements ItemSet {
    protected static final String ITEM = "item";
    protected static final String OTHER = "other";
    protected static final String VALUE = "value";

    protected final IllegalArgumentException nonSupportedItem(Item item) {
        return new IllegalArgumentException("Non supported item " + item.getKind() + " (" + item + ") by "
                + getClass().getSimpleName() + " " + this);
    }

    protected final IllegalArgumentException nonSupportedSet(ItemSet set) {
        return new IllegalArgumentException("Non supported set (" + set + ") by " + getClass().getSimpleName() + " " + this);
    }

    protected static <T> List<T> convert(Collection<? extends Item> items,
                                         Class<T> targetClass) {
        final List<T> result = new ArrayList<>();
        for (final Item item : items) {
            result.add(targetClass.cast(item));
        }
        return result;
    }

    @Override
    public final boolean isCompliantWith(ItemSetKind kind) {
        if (isEmpty() || kind == getKind()) {
            return true;
        } else {
            for (final Item item : getItems()) {
                if (!item.isCompliantWith(kind)) {
                    return false;
                }
            }
            return true;
        }
    }

    @Override
    public final BooleanSet toBooleanSet() {
        return BooleanSet.convert(this);
    }

    @Override
    public final IntegerSet toIntegerSet() {
        return IntegerSet.convert(this);
    }

    @Override
    public final RealSet toRealSet() {
        return RealSet.convert(this);
    }

    @Override
    public final StringSet toStringSet() {
        return StringSet.convert(this);
    }

    @Override
    public final boolean isSingleton() {
        return getItems().size() == 1;
    }

    @Override
    public String getContent() {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final Item item : getItems()) {
            if (!first) {
                builder.append(TokenType.VALUES_SEP.getSymbol());
            }
            builder.append(item.getProtectedLiteral());
            first = false;
        }
        return builder.toString();
    }

    @Override
    public final boolean contains(ItemSet other) {
        Checks.isNotNull(other, OTHER);
        if (other.isEmpty()) {
            return true;
        } else {
            for (final Item item : other.getItems()) {
                if (!contains(item)) {
                    return false;
                }
            }
            return true;
        }
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getContent());
        builder.append("}");
        return builder.toString();
    }
}