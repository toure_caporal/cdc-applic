package cdc.applic.expressions.content;

public enum ItemKind {
    BOOLEAN(ItemSetKind.BOOLEAN),
    INTEGER(ItemSetKind.INTEGER),
    INTEGER_RANGE(ItemSetKind.INTEGER),
    REAL(ItemSetKind.REAL),
    REAL_RANGE(ItemSetKind.REAL),
    STRING(ItemSetKind.STRING);

    private final ItemSetKind kind;

    private ItemKind(ItemSetKind kind) {
        this.kind = kind;
    }

    public boolean isCompliantWith(ItemSetKind kind) {
        return kind == this.kind || kind == ItemSetKind.UNCHECKED;
    }
}