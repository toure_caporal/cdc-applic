package cdc.applic.expressions.content;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import cdc.applic.expressions.parsing.ItemsParsing;
import cdc.util.lang.Checks;

public class UncheckedSet extends AbstractItemSet {
    private final Item[] items;
    private static final Item[] NO_ITEMS = new Item[0];

    public static final UncheckedSet EMPTY = new UncheckedSet();

    /**
     * Checked view of this set.
     */
    private ItemSet checked = null;

    private UncheckedSet(String content) {
        this(ItemsParsing.toItems(content));
        check();
    }

    private UncheckedSet(Item... items) {
        Checks.isNotNull(items, "items");

        this.items = items.length == 0 ? NO_ITEMS : items.clone();
        check();
    }

    private UncheckedSet(Collection<? extends Item> items) {
        Checks.isNotNull(items, "items");

        if (items.isEmpty()) {
            this.items = NO_ITEMS;
        } else {
            this.items = new Item[items.size()];
            items.toArray(this.items);
        }
        check();
    }

    public static UncheckedSet create(String content) {
        return new UncheckedSet(content);
    }

    public static UncheckedSet create(Item... items) {
        return new UncheckedSet(items);
    }

    public static UncheckedSet create(Collection<? extends Item> items) {
        return new UncheckedSet(items);
    }

    private IllegalArgumentException invalidSet() {
        return new IllegalArgumentException("Invalid set: " + this);
    }

    private void check() {
        for (final Item item : items) {
            if (item == null) {
                throw new IllegalArgumentException("Null item");
            }
        }
    }

    private void buildChecked() {
        if (checked == null) {
            if (isEmpty()) {
                checked = this;
            } else {
                if (isCompliantWith(ItemSetKind.BOOLEAN)) {
                    final BooleanSet result = BooleanSet.convert(this);
                    checked = result;
                } else if (isCompliantWith(ItemSetKind.INTEGER)) {
                    final IntegerSet result = IntegerSet.convert(this);
                    checked = result;
                } else if (isCompliantWith(ItemSetKind.REAL)) {
                    final RealSet result = RealSet.convert(this);
                    checked = result;
                } else if (isCompliantWith(ItemSetKind.STRING)) {
                    final StringSet result = StringSet.convert(this);
                    checked = result;
                }
                // else conversion to checked set is not possible
            }
        }
    }

    // public UncheckedSet add(Item item) {
    // final List<Item> tmp = new ArrayList<>();
    // for (final Item e : items) {
    // tmp.add(e);
    // }
    // tmp.add(item);
    //
    // return new UncheckedSet(tmp);
    // }

    @Override
    public ItemSetKind getKind() {
        return ItemSetKind.UNCHECKED;
    }

    @Override
    public ItemSet getChecked() {
        buildChecked();
        return checked;
    }

    @Override
    public boolean isEmpty() {
        return items.length == 0;
    }

    @Override
    public boolean isValid() {
        // Ignore MIXED kind
        return isEmpty()
                || isCompliantWith(ItemSetKind.BOOLEAN)
                || isCompliantWith(ItemSetKind.INTEGER)
                || isCompliantWith(ItemSetKind.REAL)
                || isCompliantWith(ItemSetKind.STRING);
    }

    @Override
    public boolean isChecked() {
        return false;
    }

    @Override
    public List<Item> getItems() {
        // TODO avoid cloning
        return Arrays.asList(items);
    }

    @Override
    public boolean contains(Item item) {
        Checks.isNotNull(item, ITEM);

        if (isEmpty()) {
            return false;
        } else {
            buildChecked();
            if (checked != null) {
                return checked.contains(item);
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public ItemSet union(Item item) {
        Checks.isNotNull(item, ITEM);

        if (isEmpty()) {
            return ItemSetUtils.toBestSet(item);
        } else {
            buildChecked();
            if (checked != null) {
                return checked.union(item);
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public ItemSet union(ItemSet other) {
        Checks.isNotNull(other, OTHER);

        if (isEmpty()) {
            return other;
        } else if (other.isEmpty()) {
            return this;
        } else {
            buildChecked();
            if (checked != null) {
                return checked.union(other.getChecked());
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public ItemSet intersection(Item item) {
        Checks.isNotNull(item, ITEM);

        if (isEmpty()) {
            return this;
        } else {
            buildChecked();
            if (checked != null) {
                return checked.intersection(item);
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public ItemSet intersection(ItemSet other) {
        Checks.isNotNull(other, OTHER);

        if (isEmpty()) {
            return this;
        } else if (other.isEmpty()) {
            return other;
        } else {
            buildChecked();
            if (checked != null) {
                return checked.intersection(other.getChecked());
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public ItemSet remove(Item item) {
        Checks.isNotNull(item, ITEM);

        if (isEmpty()) {
            return this;
        } else {
            buildChecked();
            if (checked != null) {
                return checked.remove(item);
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public ItemSet remove(ItemSet other) {
        Checks.isNotNull(other, OTHER);

        if (isEmpty() || other.isEmpty()) {
            return this;
        } else {
            buildChecked();
            if (checked != null) {
                return checked.remove(other.getChecked());
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (isEmpty() && object instanceof ItemSet) {
            final ItemSet other = (ItemSet) object;
            if (other.isEmpty()) {
                return true;
            }
        }
        if (!(object instanceof UncheckedSet)) {
            return false;
        }
        final UncheckedSet other = (UncheckedSet) object;
        return Arrays.deepEquals(items, other.items);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(items);
    }
}