package cdc.applic.expressions.content;

import java.util.Collection;

public final class ItemSetUtils {
    private ItemSetUtils() {
    }

    public static ItemSet toBestSet(Collection<? extends Item> items) {
        final UncheckedSet unchecked = UncheckedSet.create(items);
        final ItemSet checked = unchecked.getChecked();
        return checked == null ? unchecked : checked;
    }

    public static ItemSet toBestSet(Item... items) {
        final UncheckedSet unchecked = UncheckedSet.create(items);
        final ItemSet checked = unchecked.getChecked();
        return checked == null ? unchecked : checked;
    }
}