package cdc.applic.expressions.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cdc.applic.expressions.parsing.ItemsParsing;
import cdc.util.lang.Checks;

public final class IntegerSet extends AbstractRangeSet<IntegerValue, IntegerRange, IntegerSet> {
    public static final IntegerSet EMPTY = new IntegerSet();

    private IntegerSet(List<IntegerRange> ranges) {
        super(ranges,
              IntegerDomain.INSTANCE);
    }

    private IntegerSet() {
        super(IntegerDomain.INSTANCE);
    }

    private IntegerSet(Collection<? extends IntegerItem> items) {
        super(IntegerDomain.INSTANCE,
              toRanges(items));
    }

    private IntegerSet(String content) {
        this(ItemsParsing.toIntegerItems(content));
    }

    @SafeVarargs
    private IntegerSet(IntegerItem... items) {
        super(IntegerDomain.INSTANCE,
              toRanges(items));
    }

    private static List<IntegerRange> toRanges(Collection<? extends IntegerItem> items) {
        final List<IntegerRange> result = new ArrayList<>();
        for (final IntegerItem item : items) {
            if (item instanceof IntegerValue) {
                result.add(IntegerRange.create((IntegerValue) item));
            } else {
                result.add((IntegerRange) item);
            }
        }
        return result;
    }

    private static List<IntegerRange> toRanges(IntegerItem... items) {
        final List<IntegerRange> result = new ArrayList<>();
        for (final IntegerItem item : items) {
            if (item instanceof IntegerValue) {
                result.add(IntegerRange.create((IntegerValue) item));
            } else {
                result.add((IntegerRange) item);
            }
        }
        return result;
    }

    @Override
    protected IntegerSet create(List<IntegerRange> ranges) {
        return new IntegerSet(ranges);
    }

    @Override
    protected IntegerSet adapt(ItemSet set) {
        return convert(set);
    }

    @Override
    protected IntegerSet asS() {
        return this;
    }

    public static IntegerSet convert(ItemSet set) {
        Checks.isNotNull(set, "set");
        if (set instanceof IntegerSet) {
            return (IntegerSet) set;
        } else if (set.isEmpty()) {
            return EMPTY;
        } else if (set.isCompliantWith(ItemSetKind.INTEGER)) {
            return new IntegerSet(convert(set.getItems(), IntegerItem.class));
        } else {
            throw new IllegalArgumentException("Can not convert this set to " + IntegerSet.class.getSimpleName());
        }
    }

    public static IntegerSet create(Collection<? extends IntegerItem> items) {
        return new IntegerSet(items);
    }

    public static IntegerSet create(IntegerItem... items) {
        return new IntegerSet(items);
    }

    public static IntegerSet create(String content) {
        return new IntegerSet(content);
    }

    @Override
    public ItemSetKind getKind() {
        return ItemSetKind.INTEGER;
    }

    public boolean contains(int value) {
        return contains(IntegerValue.create(value));
    }

    public IntegerSet union(int value) {
        return union(IntegerValue.create(value));
    }

    public IntegerSet intersection(int value) {
        return intersection(IntegerValue.create(value));
    }

    public IntegerSet remove(int value) {
        return remove(IntegerValue.create(value));
    }
}