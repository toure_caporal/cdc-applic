package cdc.applic.expressions.content;

/**
 * Base interface of items that are ranges.
 *
 * @author Damien Carbonne
 */
public interface Range extends Item {
    public static final String SEPARATOR = "~";

    /**
     * @return {@code true} if this range is empty.
     */
    public boolean isEmpty();

    /**
     * @return {@code true} if this range is a singleton.
     */
    public boolean isSingleton();

    /**
     * Returns {@code true} if this range contains a value.
     *
     * @param value The value.
     * @return {@code true} if this range contains value.{@code value}.
     */
    public boolean contains(Value value);
}