package cdc.applic.expressions.content;

import java.util.Comparator;

public class RealRange extends AbstractRange<RealValue> implements RealItem {
    public static final Comparator<RealRange> MIN_COMPARATOR = minComparator();
    public static final Comparator<RealRange> MAX_COMPARATOR = maxComparator();

    private RealRange(RealValue value) {
        super(value);
    }

    private RealRange(RealValue min,
                      RealValue max) {
        super(min, max);
    }

    private RealRange(double value) {
        this(RealValue.create(value));
    }

    private RealRange(double min,
                      double max) {
        this(RealValue.create(min),
             RealValue.create(max));
    }

    public static RealRange create(RealValue value) {
        return new RealRange(value);
    }

    public static RealRange create(RealValue min,
                                   RealValue max) {
        return new RealRange(min, max);
    }

    public static RealRange create(double value) {
        return new RealRange(value);
    }

    public static RealRange create(double min,
                                   double max) {
        return new RealRange(min, max);
    }

    public boolean contains(double value) {
        return contains(RealValue.create(value));
    }

    @Override
    public boolean contains(Value value) {
        if (value instanceof RealValue) {
            return contains((RealValue) value);
        } else {
            throw new IllegalArgumentException("Non compliant value: " + value);
        }
    }

    @Override
    public ItemKind getKind() {
        return ItemKind.REAL_RANGE;
    }

    @Override
    public String getProtectedLiteral() {
        if (getMin().equals(getMax())) {
            return getMin().toString();
        } else {
            final StringBuilder builder = new StringBuilder();
            builder.append(getMin());
            builder.append(SEPARATOR);
            builder.append(getMax());
            return builder.toString();
        }
    }
}