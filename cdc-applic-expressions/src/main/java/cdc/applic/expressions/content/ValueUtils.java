package cdc.applic.expressions.content;

import cdc.applic.expressions.literals.EscapingUtils;
import cdc.applic.expressions.literals.LiteralUtils;

public final class ValueUtils {
    private ValueUtils() {
    }

    /**
     * Creates a Value corresponding to a literal.
     * <p>
     * <b>WARNING:</b> literal <em>MUST</em> be escaped if necessary.
     *
     * @param literal The literal, escaped if necessary.
     * @return The Value corresponding to literal.
     * @throws IllegalArgumentException When literal is invalid.
     */
    public static Value create(String literal) {
        if (EscapingUtils.isValidEscapedText(literal)) {
            return StringValue.create(EscapingUtils.unescape(literal), false);
        } else if (LiteralUtils.isBooleanLiteral(literal)) {
            return BooleanValue.create(literal);
        } else if (LiteralUtils.isIntegerLiteral(literal)) {
            return IntegerValue.create(literal);
        } else if (LiteralUtils.isRealLiteral(literal)) {
            return RealValue.create(literal);
        } else if (LiteralUtils.isRestrictedStringLiteral(literal)) {
            return StringValue.create(literal, false);
        } else {
            throw new IllegalArgumentException("Illegal literal ('" + literal + "')");
        }
    }
}