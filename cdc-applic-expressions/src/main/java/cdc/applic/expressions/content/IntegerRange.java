package cdc.applic.expressions.content;

import java.util.Comparator;

public class IntegerRange extends AbstractRange<IntegerValue> implements IntegerItem {
    public static final Comparator<IntegerRange> MIN_COMPARATOR = minComparator();
    public static final Comparator<IntegerRange> MAX_COMPARATOR = maxComparator();

    private IntegerRange(IntegerValue value) {
        super(value);
    }

    private IntegerRange(IntegerValue min,
                         IntegerValue max) {
        super(min, max);
    }

    private IntegerRange(int value) {
        this(IntegerValue.create(value));
    }

    private IntegerRange(int min,
                         int max) {
        this(IntegerValue.create(min),
             IntegerValue.create(max));
    }

    public static IntegerRange create(IntegerValue value) {
        return new IntegerRange(value);
    }

    public static IntegerRange create(IntegerValue min,
                                      IntegerValue max) {
        return new IntegerRange(min, max);
    }

    public static IntegerRange create(int value) {
        return new IntegerRange(value);
    }

    public static IntegerRange create(int min,
                                      int max) {
        return new IntegerRange(min, max);
    }

    public boolean contains(int value) {
        return contains(IntegerValue.create(value));
    }

    @Override
    public boolean contains(Value value) {
        if (value instanceof IntegerValue) {
            return contains((IntegerValue) value);
        } else {
            throw new IllegalArgumentException("Non compliant value: " + value);
        }
    }

    @Override
    public ItemKind getKind() {
        return ItemKind.INTEGER_RANGE;
    }

    @Override
    public String getProtectedLiteral() {
        if (getMin().equals(getMax())) {
            return getMin().toString();
        } else {
            final StringBuilder builder = new StringBuilder();
            builder.append(getMin());
            builder.append(SEPARATOR);
            builder.append(getMax());
            return builder.toString();
        }
    }
}