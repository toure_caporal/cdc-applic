package cdc.applic.expressions.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cdc.applic.expressions.parsing.ItemsParsing;
import cdc.util.lang.Checks;

public final class RealSet extends AbstractRangeSet<RealValue, RealRange, RealSet> {
    public static final RealSet EMPTY = new RealSet();

    private RealSet(List<RealRange> ranges) {
        super(ranges,
              RealDomain.INSTANCE);
    }

    private RealSet() {
        super(RealDomain.INSTANCE);
    }

    private RealSet(Collection<? extends RealItem> items) {
        super(RealDomain.INSTANCE,
              toRanges(items));
    }

    private RealSet(String content) {
        this(ItemsParsing.toRealItems(content));
    }

    @SafeVarargs
    private RealSet(RealItem... items) {
        super(RealDomain.INSTANCE,
              toRanges(items));
    }

    private static List<RealRange> toRanges(Collection<? extends RealItem> items) {
        final List<RealRange> result = new ArrayList<>();
        for (final RealItem item : items) {
            if (item instanceof RealValue) {
                result.add(RealRange.create((RealValue) item));
            } else {
                result.add((RealRange) item);
            }
        }
        return result;
    }

    private static List<RealRange> toRanges(RealItem... items) {
        final List<RealRange> result = new ArrayList<>();
        for (final RealItem item : items) {
            if (item instanceof RealValue) {
                result.add(RealRange.create((RealValue) item));
            } else {
                result.add((RealRange) item);
            }
        }
        return result;
    }

    @Override
    protected RealSet create(List<RealRange> ranges) {
        return new RealSet(ranges);
    }

    @Override
    protected RealSet adapt(ItemSet set) {
        return convert(set);
    }

    @Override
    protected RealSet asS() {
        return this;
    }

    public static RealSet convert(ItemSet set) {
        Checks.isNotNull(set, "set");
        if (set instanceof RealSet) {
            return (RealSet) set;
        } else if (set.isEmpty()) {
            return EMPTY;
        } else if (set.isCompliantWith(ItemSetKind.REAL)) {
            return new RealSet(convert(set.getItems(), RealItem.class));
        } else {
            throw new IllegalArgumentException("Can not convert this set to " + RealSet.class.getSimpleName());
        }
    }

    public static RealSet create(Collection<? extends RealItem> items) {
        return new RealSet(items);
    }

    public static RealSet create(RealItem... items) {
        return new RealSet(items);
    }

    public static RealSet create(String content) {
        return new RealSet(content);
    }

    @Override
    public ItemSetKind getKind() {
        return ItemSetKind.REAL;
    }

    public boolean contains(int value) {
        return contains(RealValue.create(value));
    }

    public RealSet union(int value) {
        return union(RealValue.create(value));
    }

    public RealSet intersection(int value) {
        return intersection(RealValue.create(value));
    }

    public RealSet remove(int value) {
        return remove(RealValue.create(value));
    }
}