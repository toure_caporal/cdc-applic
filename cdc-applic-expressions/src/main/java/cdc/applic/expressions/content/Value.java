package cdc.applic.expressions.content;

/**
 * Base interface of simple values.
 *
 * @author Damien Carbonne
 */
public interface Value extends Item {
    // Ignore
}