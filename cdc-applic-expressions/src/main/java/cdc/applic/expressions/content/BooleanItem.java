package cdc.applic.expressions.content;

/**
 * Base interface of boolean items.
 *
 * @author Damien Carbonne
 */
public interface BooleanItem extends Item {
    // Ignore
}