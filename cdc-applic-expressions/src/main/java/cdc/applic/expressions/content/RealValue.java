package cdc.applic.expressions.content;

import cdc.applic.expressions.literals.LiteralUtils;

public final class RealValue implements RealItem, Value, Comparable<RealValue> {
    private final double number;

    public static final RealValue MIN_VALUE = new RealValue(-Double.MAX_VALUE);
    public static final RealValue MAX_VALUE = new RealValue(Double.MAX_VALUE);

    private RealValue(double number) {
        // Special handling of -0.0
        // -0.0 != 0.0 in Java (and ...)
        this.number = number == -0.0 ? 0.0 : number;
    }

    private RealValue(String literal) {
        if (!LiteralUtils.isRealLiteral(literal)) {
            throw new IllegalArgumentException("Illegal real literal (" + literal + ")");
        }
        final double x = Double.parseDouble(literal);
        this.number = x == -0.0 ? 0.0 : x;
    }

    public static RealValue create(double number) {
        return new RealValue(number);
    }

    public static RealValue create(String literal) {
        return new RealValue(literal);
    }

    public double getNumber() {
        return number;
    }

    @Override
    public ItemKind getKind() {
        return ItemKind.REAL;
    }

    @Override
    public String getProtectedLiteral() {
        return Double.toString(number);
    }

    @Override
    public int compareTo(RealValue o) {
        return Double.compare(number, o.number);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof RealValue)) {
            return false;
        }
        final RealValue other = (RealValue) object;
        return number == other.number;
    }

    @Override
    public int hashCode() {
        return Double.hashCode(number);
    }

    @Override
    public String toString() {
        return getProtectedLiteral();
    }
}