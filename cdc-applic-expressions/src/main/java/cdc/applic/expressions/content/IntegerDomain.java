package cdc.applic.expressions.content;

import cdc.util.lang.Checks;

/**
 * Domain implementation dedicated to IntegerValue and IntegerRange.
 *
 * @author Damien Carbonne
 */
public final class IntegerDomain implements Domain<IntegerValue, IntegerRange> {
    public static final IntegerDomain INSTANCE = new IntegerDomain();

    private IntegerDomain() {
    }

    @Override
    public Class<IntegerValue> getValueClass() {
        return IntegerValue.class;
    }

    @Override
    public Class<IntegerRange> getRangeClass() {
        return IntegerRange.class;
    }

    @Override
    public IntegerValue min() {
        return IntegerValue.MIN_VALUE;
    }

    @Override
    public IntegerValue max() {
        return IntegerValue.MAX_VALUE;
    }

    @Override
    public IntegerValue pred(IntegerValue value) {
        Checks.isTrue(!value.equals(min()), value + " has no predecessor");
        return IntegerValue.create(value.getNumber() - 1);
    }

    @Override
    public IntegerValue succ(IntegerValue value) {
        Checks.isTrue(!value.equals(max()), value + " has no successor");
        return IntegerValue.create(value.getNumber() + 1);
    }

    @Override
    public IntegerRange create(IntegerValue value) {
        return IntegerRange.create(value);
    }

    @Override
    public IntegerRange create(IntegerValue min,
                               IntegerValue max) {
        return IntegerRange.create(min, max);
    }

    public IntegerValue pred(int value) {
        return pred(IntegerValue.create(value));
    }

    public IntegerValue succ(int value) {
        return succ(IntegerValue.create(value));
    }

    public boolean adjoint(int x,
                           int y) {
        final Position pos = position(IntegerValue.create(x), IntegerValue.create(y));
        return pos == Position.ADJOINT_PRED || pos == Position.ADJOINT_SUCC;
    }

    public Position position(int x,
                             int y) {
        return position(IntegerValue.create(x), IntegerValue.create(y));
    }
}