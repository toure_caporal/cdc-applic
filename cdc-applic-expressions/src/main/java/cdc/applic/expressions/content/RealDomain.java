package cdc.applic.expressions.content;

import cdc.util.lang.Checks;

/**
 * Domain implementation dedicated to RealValue and RealRange.
 *
 * @author Damien Carbonne
 */
public final class RealDomain implements Domain<RealValue, RealRange> {
    public static final RealDomain INSTANCE = new RealDomain();

    private RealDomain() {
    }

    @Override
    public Class<RealValue> getValueClass() {
        return RealValue.class;
    }

    @Override
    public Class<RealRange> getRangeClass() {
        return RealRange.class;
    }

    @Override
    public RealValue min() {
        return RealValue.MIN_VALUE;
    }

    @Override
    public RealValue max() {
        return RealValue.MAX_VALUE;
    }

    @Override
    public RealValue pred(RealValue value) {
        Checks.isTrue(!value.equals(min()), value + " has no predecessor");
        return RealValue.create(Math.nextDown(value.getNumber()));
    }

    @Override
    public RealValue succ(RealValue value) {
        Checks.isTrue(!value.equals(max()), value + " has no successor");
        return RealValue.create(Math.nextUp(value.getNumber()));
    }

    @Override
    public RealRange create(RealValue value) {
        return RealRange.create(value);
    }

    @Override
    public RealRange create(RealValue min,
                            RealValue max) {
        return RealRange.create(min, max);
    }

    public RealValue pred(double value) {
        return pred(RealValue.create(value));
    }

    public RealValue succ(double value) {
        return succ(RealValue.create(value));
    }

    public boolean adjoint(double x,
                           double y) {
        final Position pos = position(RealValue.create(x), RealValue.create(y));
        return pos == Position.ADJOINT_PRED || pos == Position.ADJOINT_SUCC;
    }

    public Position position(double x,
                             double y) {
        return position(RealValue.create(x), RealValue.create(y));
    }
}