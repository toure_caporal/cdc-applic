package cdc.applic.expressions.content;

import cdc.applic.expressions.literals.LiteralUtils;

public final class IntegerValue implements IntegerItem, Value, Comparable<IntegerValue> {
    private final int number;

    public static final IntegerValue MIN_VALUE = new IntegerValue(Integer.MIN_VALUE);
    public static final IntegerValue MAX_VALUE = new IntegerValue(Integer.MAX_VALUE);

    private IntegerValue(int number) {
        this.number = number;
    }

    private IntegerValue(String literal) {
        if (!LiteralUtils.isIntegerLiteral(literal)) {
            throw new IllegalArgumentException("Illegal integer literal (" + literal + ")");
        }
        this.number = Integer.parseInt(literal);
    }

    public static IntegerValue create(int number) {
        return new IntegerValue(number);
    }

    public static IntegerValue create(String literal) {
        return new IntegerValue(literal);
    }

    public int getNumber() {
        return number;
    }

    @Override
    public ItemKind getKind() {
        return ItemKind.INTEGER;
    }

    @Override
    public String getProtectedLiteral() {
        return Integer.toString(number);
    }

    @Override
    public int compareTo(IntegerValue o) {
        return Integer.compare(number, o.number);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof IntegerValue)) {
            return false;
        }
        final IntegerValue other = (IntegerValue) object;
        return number == other.number;
    }

    @Override
    public int hashCode() {
        return number;
    }

    @Override
    public String toString() {
        return getProtectedLiteral();
    }
}