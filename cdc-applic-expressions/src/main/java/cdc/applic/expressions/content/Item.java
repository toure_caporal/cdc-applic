package cdc.applic.expressions.content;

/**
 * Base interface of things that can be used to define a set.
 *
 * @author Damien Carbonne
 */
public interface Item {
    /**
     * @return The kind of this item.
     */
    public ItemKind getKind();

    /**
     * Returns {@code true} if this item is compliant with a kind of set.
     *
     * @param kind The kind of set.
     * @return {@code true} if this item is compliant with {@code kind}.
     */
    public default boolean isCompliantWith(ItemSetKind kind) {
        return getKind().isCompliantWith(kind);
    }

    public String getProtectedLiteral();

    public static IllegalArgumentException newIllegalConversion(Item item,
                                                                ItemSetKind kind) {
        return new IllegalArgumentException("Can not convert " + item + " to: " + kind);
    }
}