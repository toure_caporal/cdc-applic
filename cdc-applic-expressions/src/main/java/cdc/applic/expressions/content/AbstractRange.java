package cdc.applic.expressions.content;

import java.util.Comparator;
import java.util.Objects;

import cdc.util.lang.Checks;

/**
 * Base range implementation.
 * <p>
 * The minimum value can be greater than the maximum one. In that case, the range is empty.<br>
 * Two empty ranges are considered as equal, whatever the boudns are.
 *
 * @author Damien Carbonne
 *
 * @param <V> The value type.
 */
public abstract class AbstractRange<V extends Comparable<? super V>> implements Range, Comparable<AbstractRange<V>> {
    private final V min;
    private final V max;

    public AbstractRange(V min,
                         V max) {
        this.min = min;
        this.max = max;
    }

    public AbstractRange(V value) {
        this.min = value;
        this.max = value;
    }

    public static <R extends AbstractRange<V>, V extends Comparable<? super V>> Comparator<R> minComparator() {
        return new Comparator<R>() {
            @Override
            public int compare(R r1,
                               R r2) {
                return r1.getMin().compareTo(r2.getMin());
            }
        };
    }

    public static <R extends AbstractRange<V>, V extends Comparable<? super V>> Comparator<R> maxComparator() {
        return new Comparator<R>() {
            @Override
            public int compare(R r1,
                               R r2) {
                return r1.getMax().compareTo(r2.getMax());
            }
        };
    }

    /**
     * @return The minimum value.
     */
    public final V getMin() {
        return min;
    }

    /**
     * @return The maximum value.
     */
    public final V getMax() {
        return max;
    }

    @Override
    public final boolean isEmpty() {
        return min.compareTo(max) > 0;
    }

    @Override
    public final boolean isSingleton() {
        return min.equals(max);
    }

    public final boolean contains(V v) {
        return min.compareTo(v) <= 0 && max.compareTo(v) >= 0;
    }

    public final boolean contains(AbstractRange<V> r) {
        return (min.compareTo(r.min) <= 0 && max.compareTo(r.max) >= 0) || r.isEmpty();
    }

    @Override
    public int compareTo(AbstractRange<V> o) {
        Checks.isFalse(isEmpty(), "Can not compare empty range");
        Checks.isFalse(o.isEmpty(), "Can not compare empty range");
        if (ComparableUtils.lessThan(max, o.min)) {
            return -1;
        } else if (ComparableUtils.greaterThan(min, o.max)) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AbstractRange)) {
            return false;
        }
        final AbstractRange<?> other = (AbstractRange<?>) object;
        return min.equals(other.min) && max.equals(other.max)
                || isEmpty() && other.isEmpty();
    }

    @Override
    public int hashCode() {
        return isEmpty() ? 0 : Objects.hash(min, max);
    }

    @Override
    public String toString() {
        return getProtectedLiteral();
    }
}