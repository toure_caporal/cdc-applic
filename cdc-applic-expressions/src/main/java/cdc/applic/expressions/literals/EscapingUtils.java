package cdc.applic.expressions.literals;

/**
 * Escape handling.
 * <ul>
 * <li>Two characters are forbidden in literals by S1000D: '~' and '|'.
 * <li>Some characters ({@link #ESCAPES}) play a special role.
 * When used, they must be escaped.
 * <li>Integers and Reals must be escaped when used as string values (but this is not recommended).
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class EscapingUtils {

    /**
     * String containing characters that always need to be escaped.
     * <p>
     * Illegal characters ('~' and '|') don't need to be escaped.<br>
     * '-' and '+' must not be escaped in a NUMBER context.
     */
    public static final String ESCAPES = " {}()!<>=&,\"";

    /**
     * Array of booleans indexed by characters that need to be escaped.
     * <p>
     * When c is a character, NEEDS_ESCAPE[c] indicates if c needs escaping or not.
     */
    private static final boolean[] NEEDS_ESCAPE = build(ESCAPES);
    private static final int NEEDS_ESCAPE_LENGH = NEEDS_ESCAPE.length;

    private EscapingUtils() {
    }

    /**
     * Returns {@code true} when a character is legal (S1000D rules) in a literal.
     * <ul>
     * <li>It <em>MUST NOT</em> be '~'.
     * <li>It <em>MUST NOT</em> be '|'.
     * </ul>
     *
     * @param c The character.
     * @return {@code true} when {@code c} is a legal literal character.
     */
    public static boolean isLegal(char c) {
        return c != '~' && c != '|';
    }

    /**
     * Returns {@code true} when a string is legal literal.
     * <p>
     * <b>WARNING:</b> A legal literal is not necessarily valid.
     * <ul>
     * <li>It <em>MUST NOT</em> be null.
     * <li>It <em>MUST NOT</em> be empty.
     * <li>It <em>MUST NOT</em> contain illegal characters ('~' and '|').
     * </ul>
     *
     * @param s The string.
     * @return {@code true} when {@code s} is legal.
     */
    public static boolean isLegalLiteral(String s) {
        if (s == null || s.isEmpty()) {
            return false;
        } else {
            for (int index = 0; index < s.length(); index++) {
                final char c = s.charAt(index);
                if (!isLegal(c)) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * Returns {@code true} when a character needs to be escaped.
     * <p>
     * Presence of certain characters in a literal necessitates escaping whatever the context is.
     *
     * @param c The character to test
     * @return {@code true} when {@code c} needs to be escaped.
     */
    private static boolean needsEscape(char c) {
        return c < NEEDS_ESCAPE_LENGH ? NEEDS_ESCAPE[c] : false;
    }

    /**
     * Creates a boolean array where values associated to an index corresponding
     * to a character that belongs to the passed string are set to true.
     * <p>
     * With: <code>String x = build("abc");</code>
     * <br>
     * We get:
     * <code>
     * x['a'] == true and x['b'] == true x['c'] == true;
     * </code>
     *
     * @param chars The set of characters to set to true.
     * @return The corresponding array.
     */
    private static boolean[] build(String chars) {
        int max = 0;
        for (int index = 0; index < chars.length(); index++) {
            max = Math.max(max, chars.charAt(index));
        }
        final boolean[] tmp = new boolean[max + 1];
        for (int index = 0; index < tmp.length; index++) {
            tmp[index] = false;
        }
        for (int index = 0; index < chars.length(); index++) {
            tmp[chars.charAt(index)] = true;
        }
        return tmp;
    }

    /**
     * Returns {@code true} when a string needs to be escaped in a context.
     * <p>
     * Result is independent of the legality of the string as a literal.
     *
     * @param s The tested string.
     * @param context The escaping context.
     * @return {@code true} when {@code s} needs to be escaped in {@code context}.
     */
    public static boolean needsEscape(String s,
                                      EscapingContext context) {
        // Checks.isNotNull(context, "context");
        if (s != null) {
            for (int index = 0; index < s.length(); index++) {
                if (needsEscape(s.charAt(index))) {
                    return true;
                }
            }
            if (context == EscapingContext.NAME && !s.isEmpty()) {
                return LiteralUtils.isBooleanLiteral(s)
                        || LiteralUtils.isSpecialStringLiteral(s)
                        || LiteralUtils.startsAsNumber(s);

                // || LiteralUtils.isIntegerLiteral(s)
                // || LiteralUtils.isRealLiteral(s);
            }
        }
        return false;
    }

    /**
     * Escapes a string, whether it is needed or not.
     * <p>
     * The escaped version of s is built by replacing all occurrences of '"' by '""'
     * and by prepending and appending '"'.
     * <br>
     * <code>
     * escape("abc") = "\"abc\""<br>
     * escape("ab\"c") = "\"ab\"\"c\""
     * </code>
     *
     * @param s The string to escape.
     * @return The escaped version of {@code s}.
     */
    public static String escape(String s) {
        final StringBuilder builder = new StringBuilder();
        builder.append('"');
        for (int index = 0; index < s.length(); index++) {
            final char c = s.charAt(index);
            if (c == '"') {
                builder.append("\"\"");
            } else {
                builder.append(c);
            }
        }
        builder.append('"');
        return builder.toString();
    }

    /**
     * Escapes a string only of it is needed.
     *
     * @param s The string to possibly escape.
     * @param context The escaping context.
     * @return {@code s} or the escaped version of {@code s} for {@code context}.
     */
    public static String escapeIfNeeded(String s,
                                        EscapingContext context) {
        return needsEscape(s, context) ? escape(s) : s;
    }

    /**
     * Returns the unescaped version of an escaped string.
     * <p>
     * This removes first and last character (supposedly '"') and all characters that
     * follow '"' (supposedly another '"').
     * <p>
     * {@code unescape("abc") = "b"} (Invalid call)<br>
     * {@code unescape("\"ab\"\"c\"" = "ab\"c"} (Valid call)
     *
     * @param s The escaped string.
     * @return The unescaped version of s.
     * @throws IllegalArgumentException When s is {@code null} or not an escaped string.
     */
    public static String unescape(String s) {
        // Checks.isNotNull(s, "s");
        // Checks.isTrue(s.length() >= 2, "'" + s + "' is too short for unescape");
        // Checks.isTrue(s.charAt(0) == '"' && s.charAt(s.length() - 1) == '"', "'" + s + "' is not un escaped string");

        final StringBuilder builder = new StringBuilder();
        int index = 1;
        while (index < s.length() - 1) {
            final char c = s.charAt(index);
            builder.append(c);
            if (c == '"') {
                index++;
            }
            index++;
        }
        return builder.toString();
    }

    /**
     * Returns {@code true} when a string is a valid escaped or unescaped text in a context.
     * <p>
     * <b>WARNING:</b> This does not mean it is a valid literal in the context.
     *
     * @param s The string to test.
     * @param context The escaping context.
     * @return {@code true} when {@code s} is a valid escaped or unescaped text in {@code context}.
     */
    public static boolean isValidText(String s,
                                      EscapingContext context) {
        return isValidUnescapedText(s, context) || isValidEscapedText(s);
    }

    /**
     * Returns {@code true} when a string is a valid escaped or unescaped text in at least one context.
     *
     * @param s The string to test.
     * @return {@code true} when {@code s} is a valid escaped or unescaped text in at least one context.
     */
    public static boolean isValidTextInAtLeastOneContext(String s) {
        return isValidUnescapedText(s, EscapingContext.BOOLEAN)
                || isValidUnescapedText(s, EscapingContext.NUMBER)
                || isValidUnescapedText(s, EscapingContext.NAME)
                || isValidEscapedText(s);
    }

    /**
     * Returns {@code true} when a string is a valid unescaped text.
     * <p>
     * <b>WARNING:</b> This does not mean it is a valid literal.<br>
     * Such a text must:
     * <ul>
     * <li>not be null,</li>
     * <li>not be empty,</li>
     * <li>not use any character that needs escape.</li>
     * </ul>
     *
     * @param s The string to test.
     * @param context The escaping context.
     * @return {@code true} when {@code s} is a valid unescaped text.
     */
    public static boolean isValidUnescapedText(String s,
                                               EscapingContext context) {
        return isLegalLiteral(s) && !needsEscape(s, context);
    }

    /**
     * Returns true when a string is a valid escaped text.
     * <p>
     * <b>WARNING:</b> This does not mean it is a valid literal.<br>
     * Such a text must:
     * <ul>
     * <li>not be null,</li>
     * <li>start and finish with '"',</li>
     * <li>not be empty (excluding surrounding '"'),</li>
     * <li>contain pairs of successive '"' (excluding surrounding '"').</li>
     * </ul>
     *
     * @param s The string to test.
     * @return {@code true} when a {@code s} is a valid escaped text.
     */
    public static boolean isValidEscapedText(String s) {
        if (!isLegalLiteral(s) || s.length() < 3 || s.charAt(0) != '"' || s.charAt(s.length() - 1) != '"') {
            return false;
        }
        int count = 0;
        for (int index = 1; index < s.length() - 1; index++) {
            final char c = s.charAt(index);
            if (c == '"') {
                count++;
            } else {
                if ((count % 2) != 0) {
                    return false;
                }
                count = 0;
            }
        }
        return (count % 2) == 0;
    }

    public static String getString(String text,
                                   EscapingMode mode,
                                   EscapingContext context) {
        if (mode == EscapingMode.ESCAPED || (mode == EscapingMode.PROTECTED && needsEscape(text, context))) {
            return EscapingUtils.escape(text);
        } else {
            return text;
        }
    }

    public static String toNonEscaped(String literal,
                                      boolean escaped) {
        if (escaped) {
            if (EscapingUtils.isValidEscapedText(literal)) {
                return unescape(literal);
            } else {
                throw new IllegalArgumentException("Invalid escaped text: '" + literal + "'");
            }
        } else {
            if (isLegalLiteral(literal)) {
                return literal;
            } else {
                throw new IllegalArgumentException("Invalid non-escaped text: '" + literal + "'");
            }
        }
    }
}