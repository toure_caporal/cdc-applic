package cdc.applic.expressions.literals;

/**
 * Enumeration of literal escaping modes.
 *
 * @author Damien Carbonne
 */
public enum EscapingMode {
    /**
     * The literal is not escaped.
     */
    NON_ESCAPED,

    /**
     * The literal is escaped.
     */
    ESCAPED,

    /**
     * The literal is safe to be used in a particular context.
     * <p>
     * It can be escaped or not escaped.
     */
    PROTECTED
}