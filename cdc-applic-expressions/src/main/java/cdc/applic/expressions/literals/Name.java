package cdc.applic.expressions.literals;

import cdc.util.lang.Checks;

/**
 * Class used to safely encapsulate a string in a {@link EscapingContext#NAME} context.
 *
 * @author Damien Carbonne
 */
public class Name {
    /** The non-escaped literal. */
    private final String nonEscaped;

    /**
     * Creates a Name.
     *
     * @param literal The escaped or non-escaped literal.
     * @param escaped If {@code true}, {@code literal} is escaped.
     * @throws IllegalArgumentException When {@code literal} and {@code escaped} don't match.
     */
    public Name(String literal,
                boolean escaped) {
        this.nonEscaped = EscapingUtils.toNonEscaped(literal, escaped);
    }

    public String getEscapedLiteral() {
        return EscapingUtils.escape(nonEscaped);
    }

    public String getNonEscapedLiteral() {
        return nonEscaped;
    }

    public boolean needsEscape() {
        return EscapingUtils.needsEscape(nonEscaped, EscapingContext.NAME);
    }

    public String getLiteral(EscapingMode mode) {
        Checks.isNotNull(mode, "mode");

        if (mode == EscapingMode.ESCAPED) {
            return EscapingUtils.escape(nonEscaped);
        } else if (mode == EscapingMode.NON_ESCAPED) {
            return nonEscaped;
        } else {
            return needsEscape()
                    ? EscapingUtils.escape(nonEscaped)
                    : nonEscaped;
        }
    }

    @Override
    public int hashCode() {
        return nonEscaped.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Name)) {
            return false;
        }
        final Name other = (Name) object;
        return this.nonEscaped.equals(other.nonEscaped);
    }

    @Override
    public String toString() {
        return getLiteral(EscapingMode.PROTECTED);
    }
}