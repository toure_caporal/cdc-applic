package cdc.applic.expressions.literals;

public interface Named {
    public Name getName();
}