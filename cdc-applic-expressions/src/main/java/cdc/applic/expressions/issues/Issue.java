package cdc.applic.expressions.issues;

import java.util.List;

/**
 * Description of an issue.
 *
 * @author Damien Carbonne
 */
public abstract class Issue {
    /** Severity of the issue. */
    private final IssueType type;
    /** Textual description of the issue. */
    private final String description;

    private final Throwable cause;

    protected Issue(IssueType type,
                    String description,
                    Throwable cause) {
        super();
        this.type = type;
        this.description = description;
        this.cause = cause;
    }

    protected Issue(IssueType type,
                    String description) {
        this(type,
             description,
             null);
    }

    public IssueType getType() {
        return type;
    }

    public final String getDescription() {
        return description;
    }

    public Throwable getCause() {
        return cause;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[");
        builder.append(getClass().getSimpleName());
        builder.append(" ");
        builder.append(getType());
        builder.append(" ");
        builder.append(getDescription());
        builder.append("]");
        return builder.toString();
    }

    public static IssueType getMostSevereType(List<Issue> issues) {
        if (issues == null) {
            return IssueType.NO_ISSUE;
        } else {
            IssueType max = IssueType.NO_ISSUE;
            for (final Issue issue : issues) {
                max = IssueType.mostSevere(max, issue.getType());
            }
            return max;
        }
    }
}