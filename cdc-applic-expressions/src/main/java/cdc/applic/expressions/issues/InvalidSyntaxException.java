package cdc.applic.expressions.issues;

/**
 * Exception raised to indicate that an expression is invalid.
 * <p>
 * Information on the location of the problem is embedded.
 * <p>
 * Error is found during tokenizing or parsing.
 *
 * @author Damien Carbonne
 */
public class InvalidSyntaxException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private final Detail detail;
    private final String info;
    private final String expression;
    private final int beginIndex;
    private final int endIndex;

    /**
     * Enumeration of exception details.
     *
     * @author Damien Carbonne
     */
    public enum Detail {
        /** During tokenization, an invalid (real or integer) number is found. */
        TOKENIZE_INVALID_NUMBER,
        /** During tokenization, a (real or integer) number is not followed by a boundary. */
        TOKENIZE_MISSING_BOUNDARY,
        /** During tokenization, closing quotes are missing. */
        TOKENIZE_MISSING_CLOSING_QUOTES,
        /** During tokenization, an empty escaped text is found. */
        TOKENIZE_EMPTY_ESCAPED_TEXT,
        /** During parsing, an invalid range is found. */
        PARSE_INVALID_RANGE,
        /** During parsing, an unexpected token is found. */
        PARSE_UNEXPECTED_TOKEN,
        /** During parsing, end of expression is reached and more tokens are expected. */
        PARSE_UNEXPECTED_END
    }

    public InvalidSyntaxException(Detail detail,
                                  String info,
                                  String expression,
                                  int beginIndex,
                                  int endIndex) {
        super(info + ": '" + expression + "'");
        this.info = info;
        this.detail = detail;
        this.expression = expression;
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
    }

    public InvalidSyntaxException(Detail detail,
                                  String info,
                                  String expression) {
        this(detail,
             info,
             expression,
             0,
             expression.length());
    }

    public InvalidSyntaxException(Detail detail,
                                  String expression) {
        this(detail,
             "Invalid expression",
             expression);
    }

    public String getInfo() {
        return info;
    }

    /**
     * @return The exception detail.
     */
    public Detail getDetail() {
        return detail;
    }

    /**
     * @return The expression text.
     */
    public String getExpression() {
        return expression;
    }

    /**
     * @return The begin index (inclusive) in {@code expression} of the error location.
     */
    public int getBeginIndex() {
        return beginIndex;
    }

    /**
     * @return The end index (exclusive) in {@code expression} of the error location.
     */
    public int getEndIndex() {
        return endIndex;
    }

    public String getSlice() {
        final StringBuilder builder = new StringBuilder();
        final int begin = getBeginIndex() >= 0 ? getBeginIndex() : getExpression().length();
        final int end = getEndIndex() >= 0 ? getEndIndex() : getExpression().length();
        for (int index = 0; index < begin; index++) {
            builder.append(" ");
        }
        for (int index = begin; index < end; index++) {
            builder.append("^");
        }
        return builder.toString();
    }

    public String getFullMessage() {
        final StringBuilder builder = new StringBuilder();
        builder.append(getDetail());
        builder.append(" ");
        builder.append(getInfo());
        builder.append("\n");
        builder.append("   '" + getExpression() + "'\n");
        builder.append("    " + getSlice());
        return builder.toString();
    }
}