package cdc.applic.expressions.issues;

/**
 * Specialization of Issue dedicated to Syntax problems.
 * <p>
 * They are associated to a {@link InvalidSyntaxException}.
 *
 * @author Damien Carbonne
 */
public class InvalidSyntaxIssue extends Issue {
    public InvalidSyntaxIssue(InvalidSyntaxException cause) {
        super(IssueType.SYNTACTIC_ISSUE,
              cause.getFullMessage(),
              cause);
    }

    @Override
    public InvalidSyntaxException getCause() {
        return (InvalidSyntaxException) super.getCause();
    }

    public InvalidSyntaxException.Detail getDetail() {
        return getCause().getDetail();
    }
}