package cdc.applic.expressions.issues;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import cdc.util.lang.Checks;

/**
 * A Diagnosis is a list of issues.
 *
 * @author Damien Carbonne
 */
public final class Diagnosis {
    private final List<Issue> issues;

    public static final Diagnosis OK_DIAGNOSIS = new Diagnosis();

    public Diagnosis(Issue... issues) {
        Checks.isNotNull(issues, "issues");

        if (issues.length == 0) {
            this.issues = Collections.emptyList();
        } else {
            final List<Issue> tmp = new ArrayList<>();
            for (final Issue issue : issues) {
                tmp.add(issue);
            }
            this.issues = Collections.unmodifiableList(tmp);
        }
    }

    public Diagnosis(Collection<Issue> issues) {
        Checks.isNotNull(issues, "issues");

        if (issues.isEmpty()) {
            this.issues = Collections.emptyList();
        } else {
            final List<Issue> tmp = new ArrayList<>();
            for (final Issue issue : issues) {
                tmp.add(issue);
            }
            this.issues = Collections.unmodifiableList(tmp);
        }
    }

    public Diagnosis merge(Diagnosis other) {
        Checks.isNotNull(other, "other");

        final List<Issue> tmp = new ArrayList<>();
        tmp.addAll(issues);
        tmp.addAll(other.issues);
        return new Diagnosis(tmp);
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public boolean isOk() {
        return issues.isEmpty();
    }

    public boolean isAtMost(IssueType type) {
        Checks.isNotNull(type, "type");

        return IssueType.mostSevere(type, getMostSevereType()) == type;
    }

    /**
     * @return The most severe type of issues contained in this Diagnosis.
     */
    public IssueType getMostSevereType() {
        IssueType max = IssueType.NO_ISSUE;
        for (final Issue issue : issues) {
            max = IssueType.mostSevere(max, issue.getType());
        }
        return max;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        if (isOk()) {
            builder.append("OK");
        } else {
            builder.append("KO ");
            for (final Issue issue : issues) {
                builder.append(issue);
            }
        }
        return builder.toString();
    }
}