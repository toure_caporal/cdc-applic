package cdc.applic.expressions.issues;

import cdc.util.lang.Checks;

/**
 * Enumeration of issue types.
 * <p>
 * <b>WARNING:</b> order of declarations matters.
 *
 * @author Damien Carbonne
 */
public enum IssueType {
    /**
     * No issue was detected.
     */
    NO_ISSUE,

    /**
     * The expression does not respect writing rules.
     */
    WRITING_ISSUE,

    /**
     * A system of expressions has no solutions.
     * <p>
     * The expression is syntactically and semantically correct.<br>
     * It may be fatal or not, depending on context.
     */
    SATISFIABILITY_ISSUE,

    /**
     * The expression contains a semantic error.
     * <p>
     * The expression is syntactically correct.<br>
     * It is usually fatal.
     */
    SEMANTIC_ISSUE,

    /**
     * The expression contains a syntactic error.
     * <p>
     * This is the most severe level.<br>
     * It is always fatal.
     */
    SYNTACTIC_ISSUE;

    /**
     * Returns The most severe type.
     *
     * @param left The first type.
     * @param right The second type.
     * @return The most sever type among {@code left} and {@code right}.
     */
    public static IssueType mostSevere(IssueType left,
                                       IssueType right) {
        Checks.isNotNull(left, "left");
        Checks.isNotNull(right, "right");

        final int ordinal = Math.max(left.ordinal(), right.ordinal());
        return IssueType.values()[ordinal];
    }
}