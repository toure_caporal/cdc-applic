package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.BitSetCharMatcher;
import cdc.applic.expressions.parsing.BooleanArrayCharMatcher;
import cdc.applic.expressions.parsing.CharMatcher;

public class CharMatcherTest {
    @Test
    public void testBitSet() {
        final CharMatcher set = new BitSetCharMatcher("abc");
        assertTrue(set.matches('a'));
        assertTrue(set.matches('b'));
        assertTrue(set.matches('c'));
        assertFalse(set.matches('d'));
    }

    @Test
    public void testBooleanArray() {
        final CharMatcher set = new BooleanArrayCharMatcher("abc");
        assertTrue(set.matches('a'));
        assertTrue(set.matches('b'));
        assertTrue(set.matches('c'));
        assertFalse(set.matches('d'));
    }
}