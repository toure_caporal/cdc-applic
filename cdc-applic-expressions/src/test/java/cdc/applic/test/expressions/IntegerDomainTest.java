package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.IntegerDomain;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.Position;

public class IntegerDomainTest {

    private static IntegerValue v(int value) {
        return IntegerValue.create(value);
    }

    @Test
    public void test() {
        assertEquals(v(-1), IntegerDomain.INSTANCE.pred(0));
        assertEquals(v(1), IntegerDomain.INSTANCE.succ(0));
        assertEquals(IntegerValue.MIN_VALUE, IntegerDomain.INSTANCE.min());
        assertEquals(IntegerValue.MAX_VALUE, IntegerDomain.INSTANCE.max());

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         IntegerDomain.INSTANCE.succ(IntegerDomain.INSTANCE.max());
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         IntegerDomain.INSTANCE.pred(IntegerDomain.INSTANCE.min());
                     });
        assertEquals(IntegerValue.class, IntegerDomain.INSTANCE.getValueClass());
        assertEquals(IntegerRange.class, IntegerDomain.INSTANCE.getRangeClass());
    }

    @Test
    public void testAdjoint() {
        assertFalse(IntegerDomain.INSTANCE.adjoint(0, -2));
        assertTrue(IntegerDomain.INSTANCE.adjoint(0, -1));
        assertFalse(IntegerDomain.INSTANCE.adjoint(0, 0));
        assertTrue(IntegerDomain.INSTANCE.adjoint(0, 1));
        assertFalse(IntegerDomain.INSTANCE.adjoint(0, 2));
    }

    @Test
    public void testPosition() {
        assertEquals(Position.DISJOINT_PRED, IntegerDomain.INSTANCE.position(-2, 0));
        assertEquals(Position.ADJOINT_PRED, IntegerDomain.INSTANCE.position(-1, 0));
        assertEquals(Position.EQUIVALENT, IntegerDomain.INSTANCE.position(0, 0));
        assertEquals(Position.ADJOINT_SUCC, IntegerDomain.INSTANCE.position(1, 0));
        assertEquals(Position.DISJOINT_SUCC, IntegerDomain.INSTANCE.position(2, 0));
    }
}