package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.OneCharTokens;
import cdc.applic.expressions.parsing.TokenType;

public class OneCharTokensTest {
    @Test
    public void testMatcher() {
        for (char c = 0; c < 65535; c++) {
            final boolean bbs = OneCharTokens.BIT_SET_MATCHER.matches(c);
            final boolean bba = OneCharTokens.BOOLEAN_ARRAY_MATCHER.matches(c);
            final boolean bms = OneCharTokens.MULTIPLY_SHIFT_MATCHER.matches(c);
            final boolean bms2 = OneCharTokens.MULTIPLY_SHIFT_INLINE_MATCHER.matches(c);
            assertEquals(bbs, bba);
            assertEquals(bbs, bms);
            assertEquals(bbs, bms2);
        }
    }

    @Test
    public void testMapper() {
        for (char c = 0; c < 65535; c++) {
            if (OneCharTokens.BEST_MATCHER.matches(c)) {
                final TokenType st = OneCharTokens.SWITCH_MAPPER.get(c);
                final TokenType mst = OneCharTokens.MULTIPLY_SHIFT_INLINE_MAPPER.get(c);
                assertEquals(st, mst);
            }
        }
    }
}