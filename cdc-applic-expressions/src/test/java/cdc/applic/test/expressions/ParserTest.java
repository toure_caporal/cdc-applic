package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NodePrinter;
import cdc.applic.expressions.issues.InvalidSyntaxException;
import cdc.applic.expressions.parsing.Parser;
import cdc.applic.expressions.parsing.Tokenizer;

public class ParserTest {
    private static final Logger LOGGER = LogManager.getLogger(ParserTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    private final Parser parser = new Parser();

    private void checkValid(String expression) {
        try {
            final Node node = parser.parse(expression);
            LOGGER.debug("-------------------------------");
            LOGGER.debug(expression);
            NodePrinter.print(node, OUT);

        } catch (final InvalidSyntaxException e) {
            LOGGER.error("Failed (unexpected) to parse: '" + expression + "'", e);
            assertFalse(true);
        }
    }

    private void checkInvalid(String expression,
                              InvalidSyntaxException.Detail expectedDetail) {
        try {
            final Node node = parser.parse(expression);
            LOGGER.debug("-------------------------------");
            LOGGER.debug(expression);
            NodePrinter.print(node, OUT);
            assertTrue(false);
        } catch (final InvalidSyntaxException e) {
            LOGGER.debug("Failed (expected) to parse: '" + expression + "'", e);
            assertEquals(expectedDetail, e.getDetail());
        }
    }

    private void check(String expression,
                       String expected) {
        try {
            final Node node = parser.parse(expression);
            LOGGER.debug("-------------------------------");
            LOGGER.debug(expression);
            LOGGER.debug(node);
            LOGGER.debug(Tokenizer.tokenize(expression));
            NodePrinter.print(node, OUT);
            assertEquals(expected, node.toString());
        } catch (final InvalidSyntaxException e) {
            LOGGER.error("Failed to parse: '" + expression + "'", e);
            assertTrue(expected == null);
        }
    }

    @Test
    public void testValid() {
        check("\"TRue\"", "REF(\"TRue\")");
        check("\"A B\"", "REF(\"A B\")");
        check("M1000 = \"TRue\"", "EQUAL(M1000,\"TRue\")");
        check("Biplace", "REF(Biplace)");
        check("\"Bi-Place\"", "REF(Bi-Place)");
        check("\"to\" = \"to\"", "EQUAL(\"to\",\"to\")");
        check("\"and\" = \"and\"", "EQUAL(\"and\",\"and\")");

        check("Version = V1", "EQUAL(Version,V1)");
        check("\"La Version\" = V1", "EQUAL(\"La Version\",V1)");
        check("Not (Version = V1)", "NOT(EQUAL(Version,V1))");
        check("Version in {}", "IN(Version,{})");
        check("Version in {V1}", "IN(Version,{V1})");
        check("Version in {V1, V2}", "IN(Version,{V1,V2})");
        check("Version in {1, 2, 3~4}", "IN(Version,{1~4})");
        check("Version in {-1, 2, 3~4}", "IN(Version,{-1,2~4})");
        check("Version in {-1, 2, -10~4}", "IN(Version,{-10~4})");
        check("Version = V1 or Version = V2", "OR(EQUAL(Version,V1),EQUAL(Version,V2))");
        check("Version = V1 or Version = V2 and Standard = S1",
              "OR(EQUAL(Version,V1),AND(EQUAL(Version,V2),EQUAL(Standard,S1)))");
        check("(Version = V1 or Version = V2) and Standard = S1",
              "AND(OR(EQUAL(Version,V1),EQUAL(Version,V2)),EQUAL(Standard,S1))");
        check("M1000 = true", "EQUAL(M1000,true)");
        check("TRUE", "TRUE");
        check("true", "TRUE");
        check("FALSE", "FALSE");
        check("(FALSE)", "FALSE");
        check("(((M1000)))", "REF(M1000)");
        check("Version = V1 or true", "OR(EQUAL(Version,V1),TRUE)");
        checkValid("Version = V1 or Version = V2 or Version = V3");
        check("Version = V1 or Version = V2 or Version = V3",
              "OR(OR(EQUAL(Version,V1),EQUAL(Version,V2)),EQUAL(Version,V3))");
        check("(Version = V1 or Version = V2) or Version = V3",
              "OR(OR(EQUAL(Version,V1),EQUAL(Version,V2)),EQUAL(Version,V3))");
        check("Version = V1 or (Version = V2 or Version = V3)",
              "OR(EQUAL(Version,V1),OR(EQUAL(Version,V2),EQUAL(Version,V3)))");
        check("A -> B", "IMPLICATION(REF(A),REF(B))");
        check("A <-> B", "EQUIVALENCE(REF(A),REF(B))");
        check("(A -> B) -> C", "IMPLICATION(IMPLICATION(REF(A),REF(B)),REF(C))");
        check("A -> (B -> C)", "IMPLICATION(REF(A),IMPLICATION(REF(B),REF(C)))");
        check("A <-> B <-> C", "EQUIVALENCE(EQUIVALENCE(REF(A),REF(B)),REF(C))");
        check("A <-> (B <-> C)", "EQUIVALENCE(REF(A),EQUIVALENCE(REF(B),REF(C)))");
        check("\"A\" = B", "EQUAL(A,B)");
        check("\"A \" = B", "EQUAL(\"A \",B)");
        check("\"A,\" = B", "EQUAL(\"A,\",B)");
        check("\"A A\" = B", "EQUAL(\"A A\",B)");
        check("A = \" B \"", "EQUAL(A,\" B \")");
        check("A in { \" B \" }", "IN(A,{\" B \"})");
        check("A in { \"Hello World!\" , \"How Are You?\" }", "IN(A,{\"Hello World!\",\"How Are You?\"})");
        check("not A", "NOT(REF(A))");
        check("A != X", "NOT_EQUAL(A,X)");
        check("A !<: {X}", "NOT_IN(A,{X})");
        check("not (A)", "NOT(REF(A))");
    }

    @Test
    public void testPrecedence() {
        check("A or B or C", "OR(OR(REF(A),REF(B)),REF(C))");
        check("A and B and C", "AND(AND(REF(A),REF(B)),REF(C))");
        check("A -> B -> C", "IMPLICATION(IMPLICATION(REF(A),REF(B)),REF(C))");
        check("(A <-> B) <-> C", "EQUIVALENCE(EQUIVALENCE(REF(A),REF(B)),REF(C))");
    }

    @Test
    public void testInvalid() {
        checkInvalid("Value in { A to B }", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("Value in { 1~B }", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("Value in { 1~BBBB }", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("Version in {M}}", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("Version in {M", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("()", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("(FALSE))", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("((FALSE)", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("FALSE = FALSE", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("XXX = ", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("XXX = {}", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("x = and", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("x = or", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("x = to", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("x = imp", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("x = iff", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("x = not", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalid("x in {1.0~10}", InvalidSyntaxException.Detail.PARSE_UNEXPECTED_TOKEN);
    }
}