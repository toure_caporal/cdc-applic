package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.ComparableUtils;

public class ComparableUtilsTest {
    @Test
    public void test() {
        assertTrue(ComparableUtils.equal(1, 1));
        assertFalse(ComparableUtils.equal(1, 2));

        assertFalse(ComparableUtils.lessThan(1, 0));
        assertFalse(ComparableUtils.lessThan(1, 1));
        assertTrue(ComparableUtils.lessThan(1, 2));

        assertFalse(ComparableUtils.lessOrEqual(1, 0));
        assertTrue(ComparableUtils.lessOrEqual(1, 1));
        assertTrue(ComparableUtils.lessOrEqual(1, 2));

        assertTrue(ComparableUtils.greaterThan(1, 0));
        assertFalse(ComparableUtils.greaterThan(1, 1));
        assertFalse(ComparableUtils.greaterThan(1, 2));

        assertTrue(ComparableUtils.greaterOrEqual(1, 0));
        assertTrue(ComparableUtils.greaterOrEqual(1, 1));
        assertFalse(ComparableUtils.greaterOrEqual(1, 2));
    }
}