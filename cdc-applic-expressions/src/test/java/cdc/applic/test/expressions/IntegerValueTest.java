package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.ItemSetKind;

public class IntegerValueTest {

    private static void testConstructor(int value,
                                        String literal) {
        final IntegerValue fromValue = IntegerValue.create(value);
        final IntegerValue fromLiteral = IntegerValue.create(literal);

        assertEquals(fromValue, fromLiteral);
        assertEquals(value, fromValue.getNumber());
        assertEquals(value, fromLiteral.getNumber());
        assertNotEquals(value, fromValue);
        assertNotEquals(value, fromLiteral);

        assertTrue(fromValue.isCompliantWith(ItemSetKind.INTEGER));
        assertTrue(fromValue.isCompliantWith(ItemSetKind.UNCHECKED));
        assertFalse(fromValue.isCompliantWith(ItemSetKind.BOOLEAN));
        assertFalse(fromValue.isCompliantWith(ItemSetKind.REAL));
        assertFalse(fromValue.isCompliantWith(ItemSetKind.STRING));
    }

    @Test
    public void testConstructor() {
        testConstructor(1, "1");
        testConstructor(-1, "-1");
    }

    @Test
    public void testInvalidArg() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         IntegerValue.create("x");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         IntegerValue.create("1.0");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         IntegerValue.create("1 ");
                     });
    }

    @Test
    public void testEquals() {
        final IntegerValue zero = IntegerValue.create(0);
        final IntegerValue one = IntegerValue.create(1);
        assertEquals(zero, zero);
        assertEquals(one, one);
        assertNotEquals(zero, one);
        assertNotEquals(one, zero);
        assertNotEquals(one, null);
        assertNotEquals(one, "1");
    }

    @Test
    public void testGetProtectedLiteral() {
        assertEquals("1", IntegerValue.create(1).getProtectedLiteral());
    }

    @Test
    public void testToString() {
        assertEquals("1", IntegerValue.create(1).toString());
    }

    @Test
    public void testHashCode() {
        final IntegerValue zero = IntegerValue.create(0);
        final IntegerValue one = IntegerValue.create(1);
        assertNotEquals(zero.hashCode(), one.hashCode());
    }
}