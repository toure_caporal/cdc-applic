package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.OneCharSeparators;

public class OneCharSeparatorsTest {
    @Test
    public void testMatcher() {
        for (char c = 0; c < 65535; c++) {
            final boolean bbs = OneCharSeparators.BIT_SET_MATCHER.matches(c);
            final boolean bba = OneCharSeparators.BOOLEAN_ARRAY_MATCHER.matches(c);
            final boolean bms = OneCharSeparators.MULTIPLY_SHIFT_MATCHER.matches(c);
            final boolean bms2 = OneCharSeparators.MULTIPLY_SHIFT_INLINE_MATCHER.matches(c);
            assertEquals(bbs, bba);
            assertEquals(bbs, bms);
            assertEquals(bbs, bms2);
        }
    }
}