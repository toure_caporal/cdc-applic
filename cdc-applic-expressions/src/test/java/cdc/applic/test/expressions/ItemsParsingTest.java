package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.Item;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.issues.InvalidSyntaxException;
import cdc.applic.expressions.issues.InvalidSyntaxException.Detail;
import cdc.applic.expressions.parsing.ItemsParsing;

public class ItemsParsingTest {
    @SafeVarargs
    private static <T> List<T> toList(T... items) {
        final List<T> list = new ArrayList<>();
        for (final T item : items) {
            list.add(item);
        }
        return list;
    }

    private static void checkInvalidContent(Function<String, ?> f,
                                            String content,
                                            Detail expectedDetail) {
        try {
            f.apply(content);
            assertFalse(true, "Expected invalid content");
        } catch (final InvalidSyntaxException e) {
            assertEquals(expectedDetail, e.getDetail());
        }
    }

    private static <T> void checkValidContent(Function<String, List<T>> f,
                                              String content,
                                              List<T> expected) {
        final List<T> result = f.apply(content);
        assertEquals(expected, result);
    }

    private static void checkBooleansSetParsing(String content,
                                                List<BooleanValue> expected) {
        checkValidContent(ItemsParsing::toBooleanValues, content, expected);
    }

    private static void checkIntegersSetParsing(String content,
                                                List<IntegerRange> expected) {
        checkValidContent(ItemsParsing::toIntegerItems, content, expected);
    }

    private static void checkRealsSetParsing(String content,
                                             List<RealRange> expected) {
        checkValidContent(ItemsParsing::toRealItems, content, expected);
    }

    private static void checkStringsSetParsing(String content,
                                               List<StringValue> expected) {
        checkValidContent(ItemsParsing::toStringValues, content, expected);
    }

    private static void checkUncheckedSetParsing(String content,
                                                 List<Item> expected) {
        checkValidContent(ItemsParsing::toItems, content, expected);
    }

    @Test
    public void testBooleansSetParsing() {
        checkBooleansSetParsing("true", toList(BooleanValue.TRUE));
        checkBooleansSetParsing("true, false", toList(BooleanValue.TRUE, BooleanValue.FALSE));
    }

    @Test
    public void testInvalidBooleanContent() {
        checkInvalidContent(ItemsParsing::toBooleanValues, "a", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toBooleanValues, ",", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toBooleanValues, ",true", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toBooleanValues, "~", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toBooleanValues, "true,,", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toBooleanValues, "true true", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toBooleanValues, "true false", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toBooleanValues, "false true", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toBooleanValues, "false false", Detail.PARSE_UNEXPECTED_TOKEN);

        checkInvalidContent(ItemsParsing::toBooleanValues, "true, ", Detail.PARSE_UNEXPECTED_END);

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         ItemsParsing.createRange(BooleanValue.TRUE, BooleanValue.TRUE);
                     });
    }

    @Test
    public void testIntegersSetParsing() {
        checkIntegersSetParsing("", toList());
        checkIntegersSetParsing("10", toList(IntegerRange.create(10)));
        checkIntegersSetParsing("10~11", toList(IntegerRange.create(10, 11)));
        checkIntegersSetParsing("10~11, 12", toList(IntegerRange.create(10, 11), IntegerRange.create(12)));
    }

    @Test
    public void testInvalidIntegerContent() {
        checkInvalidContent(ItemsParsing::toIntegerItems, "a", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toIntegerItems, "10.0", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toIntegerItems, ",", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toIntegerItems, "~", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toIntegerItems, "10,,", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toIntegerItems, "10 10", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toIntegerItems, "10~10.0", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toIntegerItems, "10~20 10", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toIntegerItems, "10~20~", Detail.PARSE_UNEXPECTED_TOKEN);

        checkInvalidContent(ItemsParsing::toIntegerItems, "10,", Detail.PARSE_UNEXPECTED_END);
        checkInvalidContent(ItemsParsing::toIntegerItems, "10~", Detail.PARSE_UNEXPECTED_END);
        checkInvalidContent(ItemsParsing::toIntegerItems, "10~20,", Detail.PARSE_UNEXPECTED_END);
    }

    @Test
    public void testRealsSetParsing() {
        checkRealsSetParsing("", toList());
        checkRealsSetParsing("10.0", toList(RealRange.create(10.0)));
        checkRealsSetParsing("10.0~11.0", toList(RealRange.create(10.0, 11.0)));
        checkRealsSetParsing("10.0~11.0, 12.0", toList(RealRange.create(10.0, 11.0), RealRange.create(12.0)));
    }

    @Test
    public void testInvalidRealContent() {
        checkInvalidContent(ItemsParsing::toRealItems, "a", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toRealItems, "10", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toRealItems, ",", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toRealItems, "~", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toRealItems, "10.0,,", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toRealItems, "10.0 10.0", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toRealItems, "10.0~10,", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toRealItems, "10.0~20.0 10.0", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toRealItems, "10.0~20.0~", Detail.PARSE_UNEXPECTED_TOKEN);

        checkInvalidContent(ItemsParsing::toRealItems, "10.0,", Detail.PARSE_UNEXPECTED_END);
        checkInvalidContent(ItemsParsing::toRealItems, "10.0~", Detail.PARSE_UNEXPECTED_END);
        checkInvalidContent(ItemsParsing::toRealItems, "10.0~20.0,", Detail.PARSE_UNEXPECTED_END);
    }

    @Test
    public void testStringsSetParsing() {
        checkStringsSetParsing("", toList());
        checkStringsSetParsing("A", toList(StringValue.create("A", false)));
        checkStringsSetParsing("\"A\"", toList(StringValue.create("A", false)));
        checkStringsSetParsing("A,B", toList(StringValue.create("A", false), StringValue.create("B", false)));
        checkStringsSetParsing("A,B,A", toList(StringValue.create("A", false), StringValue.create("B", false)));
    }

    @Test
    public void testInvalidStringContent() {
        checkInvalidContent(ItemsParsing::toStringValues, "10", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toStringValues, ",", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toStringValues, "~", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toStringValues, "A A", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toStringValues, "A,,", Detail.PARSE_UNEXPECTED_TOKEN);

        checkInvalidContent(ItemsParsing::toStringValues, "A,", Detail.PARSE_UNEXPECTED_END);
    }

    @Test
    public void testUncheckedSetParsing() {
        checkUncheckedSetParsing("", toList());
        checkUncheckedSetParsing("10.0, 10, 10  ~  20, 10.0  ~  20.0, A, true, \"and\", FALSE",
                                 toList(RealValue.create(10.0),
                                        IntegerValue.create(10),
                                        IntegerRange.create(10, 20),
                                        RealRange.create(10.0, 20.0),
                                        StringValue.create("A", false),
                                        BooleanValue.TRUE,
                                        StringValue.create("and", false),
                                        BooleanValue.FALSE));
        checkUncheckedSetParsing("10  ~  20",
                                 toList(IntegerRange.create(10, 20)));
    }

    @Test
    public void testInvalidUncheckedContent() {
        checkInvalidContent(ItemsParsing::toItems, "{", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toItems, ",", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toItems, "~", Detail.PARSE_UNEXPECTED_TOKEN);
        checkInvalidContent(ItemsParsing::toItems, "a a", Detail.PARSE_UNEXPECTED_TOKEN);

        checkInvalidContent(ItemsParsing::toItems, "a, 10~10.0", Detail.PARSE_INVALID_RANGE);

        checkInvalidContent(ItemsParsing::toItems, "A,", Detail.PARSE_UNEXPECTED_END);
        checkInvalidContent(ItemsParsing::toItems, "A~", Detail.PARSE_UNEXPECTED_END);
    }
}