package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.Spaces;

public class SpacesTest {
    @Test
    public void testMatcher() {
        for (char c = 0; c < 65535; c++) {
            final boolean bbs = Spaces.BIT_SET_MATCHER.matches(c);
            final boolean bba = Spaces.BOOLEAN_ARRAY_MATCHER.matches(c);
            final boolean bms = Spaces.MULTIPLY_SHIFT_MATCHER.matches(c);
            final boolean bms2 = Spaces.MULTIPLY_SHIFT_INLINE_MATCHER.matches(c);
            assertEquals(bbs, bba);
            assertEquals(bbs, bms);
            assertEquals(bbs, bms2);
        }
    }
}