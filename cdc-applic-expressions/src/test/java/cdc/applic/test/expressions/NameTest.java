package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.literals.EscapingMode;
import cdc.applic.expressions.literals.Name;

public class NameTest {
    private static void testConstructor(String unescaped,
                                        String escaped) {
        final Name escapedName = new Name(escaped, true);
        final Name unescapedName = new Name(unescaped, false);

        assertEquals(unescaped, escapedName.getNonEscapedLiteral());
        assertEquals(escaped, escapedName.getEscapedLiteral());
        assertEquals(escaped, escapedName.getLiteral(EscapingMode.ESCAPED));
        assertEquals(unescaped, escapedName.getLiteral(EscapingMode.NON_ESCAPED));

        assertEquals(unescaped, unescapedName.getNonEscapedLiteral());
        assertEquals(escaped, unescapedName.getEscapedLiteral());
        assertEquals(escaped, unescapedName.getLiteral(EscapingMode.ESCAPED));
        assertEquals(unescaped, unescapedName.getLiteral(EscapingMode.NON_ESCAPED));

        assertEquals(escapedName, escapedName);
        assertEquals(escapedName, unescapedName);
        assertNotEquals(escapedName, null);

        if (escapedName.needsEscape()) {
            assertEquals(escaped, escapedName.getLiteral(EscapingMode.PROTECTED));
        } else {
            assertEquals(unescaped, escapedName.getLiteral(EscapingMode.PROTECTED));
        }
    }

    @Test
    public void testConstructor() {
        testConstructor("a", "\"a\"");
        testConstructor("10", "\"10\"");
    }

    @Test
    public void testInvalidArg() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new Name(null, true);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new Name("", true);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new Name(null, false);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new Name("", false);
                     });
    }

    @Test
    public void testHashCode() {
        assertEquals(new Name("Hello", false).hashCode(), new Name("Hello", false).hashCode());
    }

}