package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.IntegerDomain;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.Position;
import cdc.applic.expressions.content.RangeUtils;

public class RangeUtilsTest {

    private static IntegerRange[] a(IntegerRange... ranges) {
        return ranges;
    }

    private static IntegerRange r(int value) {
        return IntegerRange.create(value);
    }

    private static IntegerRange r(int min,
                                  int max) {
        return IntegerRange.create(min, max);
    }

    @Test
    public void testRemoveSimple() {
        assertEquals(r(1, 0), RangeUtils.removeSimple(IntegerDomain.INSTANCE, r(1, 0), r(2)));
        assertEquals(r(1), RangeUtils.removeSimple(IntegerDomain.INSTANCE, r(1), r(1, 0)));

        assertEquals(r(1), RangeUtils.removeSimple(IntegerDomain.INSTANCE, r(1), r(2)));
        assertEquals(r(1), RangeUtils.removeSimple(IntegerDomain.INSTANCE, r(1, 3), r(2, 3)));
        assertEquals(r(1, 2), RangeUtils.removeSimple(IntegerDomain.INSTANCE, r(1, 3), r(3)));
        assertEquals(r(1, 0), RangeUtils.removeSimple(IntegerDomain.INSTANCE, r(1, 3), r(1, 3)));
        assertEquals(r(1, 0), RangeUtils.removeSimple(IntegerDomain.INSTANCE, r(1, 3), r(0, 3)));
        assertEquals(r(1, 0), RangeUtils.removeSimple(IntegerDomain.INSTANCE, r(1, 3), r(0, 4)));

        assertEquals(r(2, 3), RangeUtils.removeSimple(IntegerDomain.INSTANCE, r(1, 3), r(1, 1)));
        assertEquals(r(3), RangeUtils.removeSimple(IntegerDomain.INSTANCE, r(1, 3), r(1, 2)));
        assertEquals(r(1, 0), RangeUtils.removeSimple(IntegerDomain.INSTANCE, r(1, 3), r(1, 3)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RangeUtils.removeSimple(IntegerDomain.INSTANCE, r(1, 3), r(2));
                     });
    }

    @Test
    public void testRemove() {
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 0), r(2)));
        assertArrayEquals(a(r(1, 2)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 2), r(1, 0)));
        assertArrayEquals(a(r(1, 2)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 2), r(3, 4)));

        assertArrayEquals(a(r(1)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1), r(0)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1), r(0, 1)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1), r(0, 2)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1), r(1)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1), r(1, 2)));
        assertArrayEquals(a(r(1)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1), r(2)));

        assertArrayEquals(a(r(1, 2)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 2), r(0)));
        assertArrayEquals(a(r(2)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 2), r(0, 1)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 2), r(0, 2)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 2), r(0, 3)));
        assertArrayEquals(a(r(2)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 2), r(1)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 2), r(1, 2)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 2), r(1, 3)));
        assertArrayEquals(a(r(1)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 2), r(2)));
        assertArrayEquals(a(r(1)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 2), r(2, 3)));
        assertArrayEquals(a(r(1, 2)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 2), r(3)));

        assertArrayEquals(a(r(1, 3)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(0)));
        assertArrayEquals(a(r(2, 3)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(0, 1)));
        assertArrayEquals(a(r(3)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(0, 2)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(0, 3)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(0, 4)));
        assertArrayEquals(a(r(2, 3)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(1)));
        assertArrayEquals(a(r(3)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(1, 2)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(1, 3)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(1, 4)));
        assertArrayEquals(a(r(1), r(3)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(2)));
        assertArrayEquals(a(r(1)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(2, 3)));
        assertArrayEquals(a(r(1)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(2, 4)));
        assertArrayEquals(a(r(1, 2)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(3)));
        assertArrayEquals(a(r(1, 3)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(4)));
        assertArrayEquals(a(r(1, 3)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 3), r(4)));

        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(0, 6)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(0, 5)));
        assertArrayEquals(a(r(5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(0, 4)));
        assertArrayEquals(a(r(4, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(0, 3)));
        assertArrayEquals(a(r(3, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(0, 2)));
        assertArrayEquals(a(r(2, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(0, 1)));
        assertArrayEquals(a(r(1, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(0, 0)));

        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(1, 6)));
        assertArrayEquals(a(), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(1, 5)));
        assertArrayEquals(a(r(5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(1, 4)));
        assertArrayEquals(a(r(4, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(1, 3)));
        assertArrayEquals(a(r(3, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(1, 2)));
        assertArrayEquals(a(r(2, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(1, 1)));
        assertArrayEquals(a(r(1, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(1, 0)));

        assertArrayEquals(a(r(1)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(2, 6)));
        assertArrayEquals(a(r(1)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(2, 5)));
        assertArrayEquals(a(r(1), r(5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(2, 4)));
        assertArrayEquals(a(r(1), r(4, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(2, 3)));
        assertArrayEquals(a(r(1), r(3, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(2, 2)));
        assertArrayEquals(a(r(1, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(2, 1)));

        assertArrayEquals(a(r(1, 2)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(3, 6)));
        assertArrayEquals(a(r(1, 2)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(3, 5)));
        assertArrayEquals(a(r(1, 2), r(5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(3, 4)));
        assertArrayEquals(a(r(1, 2), r(4, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(3, 3)));
        assertArrayEquals(a(r(1, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(3, 2)));

        assertArrayEquals(a(r(1, 3)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(4, 6)));
        assertArrayEquals(a(r(1, 3)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(4, 5)));
        assertArrayEquals(a(r(1, 3), r(5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(4, 4)));
        assertArrayEquals(a(r(1, 5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(4, 3)));

        assertArrayEquals(a(r(1)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(2, 6)));
        assertArrayEquals(a(r(5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(1, 4)));
        assertArrayEquals(a(r(1)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(2, 5)));
        assertArrayEquals(a(r(1), r(5)), RangeUtils.remove(IntegerDomain.INSTANCE, r(1, 5), r(2, 4)));
    }

    @Test
    public void testIntersected() {
        assertTrue(RangeUtils.intersected(r(1, 2), r(1)));
        assertFalse(RangeUtils.intersected(r(1, 2), r(0)));
        assertFalse(RangeUtils.intersected(r(1, 2), r(3, 4)));
        assertTrue(RangeUtils.intersected(r(1, 2), r(2, 4)));
        assertFalse(RangeUtils.intersected(r(1, 2), r(1, 0)));
        assertFalse(RangeUtils.intersected(r(1, 0), r(1, 0)));
        assertFalse(RangeUtils.intersected(r(1, 0), r(10, 9)));
    }

    @Test
    public void testIntersection() {
        assertEquals(r(1), RangeUtils.intersection(IntegerDomain.INSTANCE, r(1, 2), r(0, 1)));
        assertEquals(r(3, 2), RangeUtils.intersection(IntegerDomain.INSTANCE, r(1, 2), r(3, 4)));
    }

    @Test
    public void testMergeable() {
        assertFalse(RangeUtils.mergeable(IntegerDomain.INSTANCE, r(1, 2), r(-1)));
        assertTrue(RangeUtils.mergeable(IntegerDomain.INSTANCE, r(1, 2), r(0)));
        assertTrue(RangeUtils.mergeable(IntegerDomain.INSTANCE, r(1, 2), r(1)));
        assertTrue(RangeUtils.mergeable(IntegerDomain.INSTANCE, r(1, 2), r(2)));
        assertTrue(RangeUtils.mergeable(IntegerDomain.INSTANCE, r(1, 2), r(3)));
        assertFalse(RangeUtils.mergeable(IntegerDomain.INSTANCE, r(1, 2), r(4)));
    }

    @Test
    public void testUnion() {
        assertEquals(r(10, 0), RangeUtils.union(IntegerDomain.INSTANCE, r(1, 0), r(1, 0)));
        assertEquals(r(1), RangeUtils.union(IntegerDomain.INSTANCE, r(1, 1), r(1, 0)));
        assertEquals(r(1, 20), RangeUtils.union(IntegerDomain.INSTANCE, r(1, 10), r(9, 20)));
        assertEquals(r(1, 20), RangeUtils.union(IntegerDomain.INSTANCE, r(1, 10), r(10, 20)));
        assertEquals(r(1, 20), RangeUtils.union(IntegerDomain.INSTANCE, r(1, 10), r(11, 20)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RangeUtils.union(IntegerDomain.INSTANCE, r(1, 10), r(12, 20));
                     });
    }

    @Test
    public void testPosition() {
        assertEquals(Position.EQUIVALENT, RangeUtils.position(IntegerDomain.INSTANCE, r(1), r(1)));
        assertEquals(Position.EQUIVALENT, RangeUtils.position(IntegerDomain.INSTANCE, r(1), r(1, 2)));
        assertEquals(Position.EQUIVALENT, RangeUtils.position(IntegerDomain.INSTANCE, r(1), r(0, 1)));
        assertEquals(Position.EQUIVALENT, RangeUtils.position(IntegerDomain.INSTANCE, r(1), r(0, 2)));

        assertEquals(Position.ADJOINT_PRED, RangeUtils.position(IntegerDomain.INSTANCE, r(1), r(2)));
        assertEquals(Position.ADJOINT_PRED, RangeUtils.position(IntegerDomain.INSTANCE, r(1), r(2, 3)));

        assertEquals(Position.DISJOINT_PRED, RangeUtils.position(IntegerDomain.INSTANCE, r(1), r(3)));

        assertEquals(Position.ADJOINT_SUCC, RangeUtils.position(IntegerDomain.INSTANCE, r(2), r(1)));
        assertEquals(Position.ADJOINT_SUCC, RangeUtils.position(IntegerDomain.INSTANCE, r(2, 3), r(1)));

        assertEquals(Position.DISJOINT_SUCC, RangeUtils.position(IntegerDomain.INSTANCE, r(3), r(1)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RangeUtils.position(IntegerDomain.INSTANCE, r(1, 0), r(1));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RangeUtils.position(IntegerDomain.INSTANCE, r(1), r(1, 0));
                     });

    }
}