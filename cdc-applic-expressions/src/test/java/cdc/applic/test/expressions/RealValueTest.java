package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.ItemSetKind;
import cdc.applic.expressions.content.RealValue;

public class RealValueTest {

    private static void testConstructor(double value,
                                        String literal) {
        final RealValue fromValue = RealValue.create(value);
        final RealValue fromLiteral = RealValue.create(literal);

        assertEquals(fromValue, fromLiteral);
        assertEquals(value, fromValue.getNumber());
        assertEquals(value, fromLiteral.getNumber());
        assertNotEquals(value, fromValue);
        assertNotEquals(value, fromLiteral);

        assertTrue(fromValue.isCompliantWith(ItemSetKind.REAL));
        assertTrue(fromValue.isCompliantWith(ItemSetKind.UNCHECKED));
        assertFalse(fromValue.isCompliantWith(ItemSetKind.BOOLEAN));
        assertFalse(fromValue.isCompliantWith(ItemSetKind.INTEGER));
        assertFalse(fromValue.isCompliantWith(ItemSetKind.STRING));
    }

    @Test
    public void testConstructor() {
        testConstructor(0.0, "0.0");
        testConstructor(0.0, "-0.0");
        testConstructor(1.0, "1.0");
        testConstructor(-1.0, "-1.0");
    }

    @Test
    public void testInvalidArg() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealValue.create("x");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealValue.create("1");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealValue.create("1.0 ");
                     });
    }

    @Test
    public void testEquals() {
        final RealValue zero = RealValue.create(0.0);
        final RealValue one = RealValue.create(1.0);
        assertEquals(zero, zero);
        assertEquals(one, one);
        assertNotEquals(zero, one);
        assertNotEquals(one, zero);
        assertNotEquals(one, null);
        assertNotEquals(one, "1.0");
    }

    @Test
    public void testGetProtectedLiteral() {
        assertEquals("1.0", RealValue.create(1.0).getProtectedLiteral());
    }

    @Test
    public void testToString() {
        assertEquals("1.0", RealValue.create(1.0).toString());
    }

    @Test
    public void testHashCode() {
        final RealValue zero = RealValue.create(0.0);
        final RealValue one = RealValue.create(1.0);
        assertNotEquals(zero.hashCode(), one.hashCode());
    }
}