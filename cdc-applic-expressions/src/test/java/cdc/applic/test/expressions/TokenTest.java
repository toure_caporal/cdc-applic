package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.Token;
import cdc.applic.expressions.parsing.TokenType;

public class TokenTest {
    public static void check(TokenType type,
                             String expression,
                             int beginIndex,
                             int endIndex,
                             String unescaped) {
        final Token token = new Token(type, expression, beginIndex, endIndex);
        assertEquals(expression, token.getExpression());
        assertEquals(type, token.getType());
        assertEquals(beginIndex, token.getBeginIndex());
        assertEquals(endIndex, token.getEndIndex());
        assertEquals(unescaped, token.getUnescapedText());
        assertEquals(type == TokenType.ESCAPED_TEXT, token.isEscaped());
    }

    @Test
    public void testConstructor() {
        check(TokenType.EPSILON, null, -1, -1, null);
        check(TokenType.EPSILON, "", -1, -1, "");
        check(TokenType.AND, "and", 0, 3, "and");
        check(TokenType.AND, "  and  ", 2, 5, "and");
        check(TokenType.ESCAPED_TEXT, "  \"and\"  ", 2, 7, "and");
    }

    @Test
    public void testEquals() {
        final Token token1 = new Token(TokenType.AND, null, 1, 2);
        final Token token2 = new Token(TokenType.AND, null, 1, 2);
        final Token token3 = new Token(TokenType.AND, null, 1, 3);
        final Token token4 = new Token(TokenType.AND, null, 2, 2);
        final Token token5 = new Token(TokenType.OR, null, 2, 2);
        final Token token6 = new Token(TokenType.OR, "Hello", 2, 2);
        assertEquals(token1, token1);
        assertEquals(token1, token2);
        assertNotEquals(token1, null);
        assertNotEquals(token1, "hello");
        assertNotEquals(token1, token3);
        assertNotEquals(token1, token4);
        assertNotEquals(token1, token5);
        assertNotEquals(token6, token5);
    }

    @Test
    public void testHashCode() {
        final Token token1 = new Token(TokenType.AND, null, 1, 2);
        final Token token2 = new Token(TokenType.AND, null, 1, 2);
        assertEquals(token1.hashCode(), token2.hashCode());
    }

}