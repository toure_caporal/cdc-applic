package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.ItemSetKind;

public class BooleanValueTest {

    private static void testConstructor(boolean value,
                                        String literal) {
        final BooleanValue fromValue = BooleanValue.create(value);
        final BooleanValue fromLiteral = BooleanValue.create(literal);

        assertEquals(fromValue, fromLiteral);
        assertEquals(value, fromValue.getValue());
        assertEquals(value, fromLiteral.getValue());
        assertNotEquals(value, fromValue);
        assertNotEquals(value, fromLiteral);
        assertTrue(fromValue.isCompliantWith(ItemSetKind.BOOLEAN));
        assertTrue(fromValue.isCompliantWith(ItemSetKind.UNCHECKED));
        assertFalse(fromValue.isCompliantWith(ItemSetKind.INTEGER));
        assertFalse(fromValue.isCompliantWith(ItemSetKind.REAL));
        assertFalse(fromValue.isCompliantWith(ItemSetKind.STRING));
    }

    @Test
    public void testConstructor() {
        testConstructor(true, "true");
        testConstructor(true, "TRUE");
        testConstructor(false, "false");
        testConstructor(false, "FALSE");
    }

    @Test
    public void testInvalidArg() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanValue.create("0");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanValue.create("1");
                     });
    }

    @Test
    public void testEquals() {
        assertEquals(BooleanValue.TRUE, BooleanValue.TRUE);
        assertEquals(BooleanValue.FALSE, BooleanValue.FALSE);
        assertNotEquals(BooleanValue.FALSE, BooleanValue.TRUE);
        assertNotEquals(BooleanValue.TRUE, BooleanValue.FALSE);
        assertNotEquals(BooleanValue.TRUE, true);
        assertNotEquals(BooleanValue.TRUE, "true");
    }

    @Test
    public void testNegate() {
        assertEquals(BooleanValue.FALSE, BooleanValue.TRUE.negate());
        assertEquals(BooleanValue.TRUE, BooleanValue.FALSE.negate());
    }

    @Test
    public void testGetProtectedLiteral() {
        assertEquals("true", BooleanValue.TRUE.getProtectedLiteral());
        assertEquals("false", BooleanValue.FALSE.getProtectedLiteral());
    }

    @Test
    public void testToString() {
        assertEquals("true", BooleanValue.TRUE.toString());
        assertEquals("false", BooleanValue.FALSE.toString());
    }

    @Test
    public void testHashCode() {
        assertNotEquals(BooleanValue.TRUE.hashCode(), BooleanValue.FALSE.hashCode());
    }
}