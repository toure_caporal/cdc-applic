package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractNamedNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractOperatorNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.AbstractValueNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.ImplicationNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotEqualNode;
import cdc.applic.expressions.ast.NotInNode;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.ValueUtils;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.CollectionUtils;

public class NodeTest {
    private final AndNode nBinaryAnd = new AndNode(TrueNode.INSTANCE, FalseNode.INSTANCE);
    private final OrNode nBinaryOr = new OrNode(TrueNode.INSTANCE, FalseNode.INSTANCE);
    private final EqualNode nEqual = new EqualNode(new Name("Prop", false), ValueUtils.create("1"));
    private final EquivalenceNode nEquivalence = new EquivalenceNode(TrueNode.INSTANCE, FalseNode.INSTANCE);
    private final FalseNode nFalse = FalseNode.INSTANCE;
    private final ImplicationNode nImplication = new ImplicationNode(TrueNode.INSTANCE, FalseNode.INSTANCE);
    private final InNode nIn = new InNode(new Name("Prop", false), BooleanSet.TRUE);
    private final NaryAndNode nNaryAnd = new NaryAndNode(TrueNode.INSTANCE, nFalse);
    private final NaryOrNode nNaryOr = new NaryOrNode(TrueNode.INSTANCE, nFalse);
    private final NotNode nNot = new NotNode(TrueNode.INSTANCE);
    private final NotEqualNode nNotEqual = new NotEqualNode(new Name("Prop", false), ValueUtils.create("1"));
    private final NotInNode nNotIn = new NotInNode(new Name("Prop", false), BooleanSet.TRUE);
    private final RefNode nRef = new RefNode(new Name("Ref", false));
    private final TrueNode nTrue = TrueNode.INSTANCE;

    private final Node[] nodes = {
            nBinaryAnd,
            nBinaryOr,
            nEqual,
            nEquivalence,
            nFalse,
            nImplication,
            nIn,
            nNaryAnd,
            nNaryOr,
            nNot,
            nNotEqual,
            nNotIn,
            nRef,
            nTrue
    };

    @Test
    public void testIsX() {
        for (final Node node : nodes) {
            assertEquals(node.isBinary(), node instanceof AbstractBinaryNode, node.toString());
            assertEquals(node.isBinaryAnd(), node instanceof AndNode);
            assertEquals(node.isBinaryOr(), node instanceof OrNode);
            assertEquals(node.isFalse(), node instanceof FalseNode);
            assertEquals(node.isLeaf(), node instanceof AbstractLeafNode);
            assertEquals(node.isNamed(), node instanceof AbstractNamedNode);
            assertEquals(node.isNary(), node instanceof AbstractNaryNode);
            assertEquals(node.isNaryAnd(), node instanceof NaryAndNode);
            assertEquals(node.isNaryOr(), node instanceof NaryOrNode);
            assertEquals(node.isNot(), node instanceof NotNode);
            assertEquals(node.isOperator(), node instanceof AbstractOperatorNode);
            assertEquals(node.isProperty(), node instanceof AbstractPropertyNode);
            assertEquals(node.isRef(), node instanceof RefNode);
            assertEquals(node.isSet(), node instanceof AbstractSetNode);
            assertEquals(node.isTrue(), node instanceof TrueNode);
            assertEquals(node.isUnary(), node instanceof AbstractUnaryNode);
            assertEquals(node.isValue(), node instanceof AbstractValueNode);
        }
    }

    @Test
    public void testChildren() {
        for (final Node node : nodes) {
            assertThrows(IllegalArgumentException.class,
                         () -> {
                             node.getChildAt(-1);
                         });
            if (node.isBinary()) {
                assertEquals(2, node.getChildrenCount());
                assertTrue(node.getChildAt(0) != null);
                assertTrue(node.getChildAt(1) != null);
                assertThrows(IllegalArgumentException.class,
                             () -> {
                                 node.getChildAt(2);
                             });
                assertTrue(node.getHeight() > 1);
            } else if (node.isNary()) {
                assertTrue(node.getChildrenCount() >= 1);
                assertTrue(node.getChildAt(0) != null);
                assertThrows(IllegalArgumentException.class,
                             () -> {
                                 node.getChildAt(node.getChildrenCount());
                             });
                assertTrue(node.getHeight() > 1);
            } else if (node.isUnary()) {
                assertEquals(1, node.getChildrenCount());
                assertTrue(node.getChildAt(0) != null);
                assertThrows(IllegalArgumentException.class,
                             () -> {
                                 node.getChildAt(1);
                             });
                assertTrue(node.getHeight() > 1);
            } else if (node.isLeaf()) {
                assertEquals(0, node.getChildrenCount());
                assertThrows(IllegalArgumentException.class,
                             () -> {
                                 node.getChildAt(0);
                             });
                assertEquals(1, node.getHeight());
            }
        }
    }

    @Test
    public void testCreate() {
        assertEquals(nNot, nNot.create(nNot.getAlpha()));
        assertEquals(nBinaryAnd, nBinaryAnd.create(nBinaryAnd.getAlpha(), nBinaryAnd.getBeta()));
        assertEquals(nBinaryOr, nBinaryOr.create(nBinaryOr.getAlpha(), nBinaryOr.getBeta()));
        assertEquals(nNaryAnd, nNaryAnd.create(nNaryAnd.getChildren()));
        assertEquals(nNaryAnd, nNaryAnd.create(nNaryAnd.getChildrenAsList()));
        assertEquals(nNaryOr, nNaryOr.create(nNaryAnd.getChildren()));
        assertEquals(nNaryOr, nNaryOr.create(nNaryAnd.getChildrenAsList()));
        assertEquals(nImplication, nImplication.create(nImplication.getAlpha(), nImplication.getBeta()));
        assertEquals(nEquivalence, nEquivalence.create(nEquivalence.getAlpha(), nEquivalence.getBeta()));
        assertEquals(nIn, nIn.create(nIn.getName(), nIn.getItems()));
        assertEquals(nNotIn, nNotIn.create(nIn.getName(), nNotIn.getItems()));
        assertEquals(nEqual, nEqual.create(nEqual.getName(), nEqual.getValue()));
        assertEquals(nNotEqual, nNotEqual.create(nNotEqual.getName(), nNotEqual.getValue()));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new NaryAndNode();
                     });
    }

    @Test
    public void testEqual() {
        assertEquals(TrueNode.INSTANCE, TrueNode.INSTANCE);
        assertNotEquals(TrueNode.INSTANCE, FalseNode.INSTANCE);

        assertEquals(FalseNode.INSTANCE, FalseNode.INSTANCE);
        assertNotEquals(FalseNode.INSTANCE, TrueNode.INSTANCE);

        assertEquals(nRef, nRef);
        assertEquals(nRef, new RefNode(new Name("Ref", false)));
        assertNotEquals(nRef, null);

        assertEquals(nIn, nIn);
        assertNotEquals(nIn, null);
        assertEquals(nIn, new InNode(new Name("Prop", false), BooleanSet.TRUE));
        assertNotEquals(nIn, new InNode(new Name("Prop1", false), BooleanSet.TRUE));
        assertNotEquals(nIn, new InNode(new Name("Prop", false), BooleanSet.FALSE));

        assertEquals(nNotIn, nNotIn);
        assertNotEquals(nNotIn, null);
        assertEquals(nNotIn, new NotInNode(new Name("Prop", false), BooleanSet.TRUE));
        assertNotEquals(nNotIn, new NotInNode(new Name("Prop1", false), BooleanSet.TRUE));
        assertNotEquals(nNotIn, new NotInNode(new Name("Prop", false), BooleanSet.FALSE));

        assertEquals(nEqual, nEqual);
        assertNotEquals(nEqual, null);
        assertEquals(nEqual, new EqualNode(new Name("Prop", false), ValueUtils.create("1")));
        assertNotEquals(nEqual, new EqualNode(new Name("Prop", false), ValueUtils.create("2")));
        assertNotEquals(nEqual, new EqualNode(new Name("Prop1", false), ValueUtils.create("1")));

        assertEquals(nNotEqual, nNotEqual);
        assertNotEquals(nNotEqual, null);
        assertEquals(nNotEqual, new NotEqualNode(new Name("Prop", false), ValueUtils.create("1")));
        assertNotEquals(nNotEqual, new NotEqualNode(new Name("Prop", false), ValueUtils.create("2")));
        assertNotEquals(nNotEqual, new NotEqualNode(new Name("Prop1", false), ValueUtils.create("1")));

        assertEquals(nNot, nNot);
        assertNotEquals(nNot, null);
        assertEquals(nNot, new NotNode(TrueNode.INSTANCE));
        assertNotEquals(nNot, new NotNode(FalseNode.INSTANCE));

        assertEquals(nNaryAnd, nNaryAnd);
        assertNotEquals(nNaryAnd, nNaryOr);
        assertEquals(nNaryAnd, new NaryAndNode(nNaryAnd.getChildren()));
        assertNotEquals(nNaryAnd, null);
        assertNotEquals(nNaryAnd, new NaryAndNode(TrueNode.INSTANCE));
        assertNotEquals(new NaryAndNode(FalseNode.INSTANCE), new NaryAndNode(TrueNode.INSTANCE));

        assertEquals(nBinaryAnd, nBinaryAnd);
        assertNotEquals(nBinaryAnd, nBinaryOr);
        assertEquals(nBinaryAnd, new AndNode(nBinaryAnd.getAlpha(), nBinaryAnd.getBeta()));
        assertNotEquals(nBinaryAnd, null);
        assertNotEquals(nBinaryAnd, new AndNode(TrueNode.INSTANCE, TrueNode.INSTANCE));
        assertNotEquals(new AndNode(FalseNode.INSTANCE, FalseNode.INSTANCE), new AndNode(TrueNode.INSTANCE, TrueNode.INSTANCE));
    }

    @Test
    public void testHashCode() {
        assertEquals(nBinaryAnd.hashCode(), nBinaryAnd.hashCode());
        assertEquals(nBinaryOr.hashCode(), nBinaryOr.hashCode());
        assertEquals(nNaryAnd.hashCode(), nNaryAnd.hashCode());
        assertEquals(nNaryOr.hashCode(), nNaryOr.hashCode());
        assertEquals(nTrue.hashCode(), nTrue.hashCode());
        assertEquals(nFalse.hashCode(), nFalse.hashCode());
        assertEquals(nIn.hashCode(), nIn.hashCode());
        assertEquals(nNotIn.hashCode(), nNotIn.hashCode());
        assertEquals(nEqual.hashCode(), nEqual.hashCode());
        assertEquals(nNotEqual.hashCode(), nNotEqual.hashCode());
        assertEquals(nRef.hashCode(), nRef.hashCode());
        assertEquals(nNot.hashCode(), nNot.hashCode());
    }

    @Test
    public void testNot() {
        assertEquals(TrueNode.INSTANCE, NotNode.simplestNot(new NotNode(TrueNode.INSTANCE)));
        assertEquals(NotNode.simplestNot(TrueNode.INSTANCE), NotNode.simplestNot(TrueNode.INSTANCE));
    }

    @Test
    public void testNaryOr() {
        final Node n = TrueNode.INSTANCE;
        assertEquals(n, NaryOrNode.createSimplestOr(n));
        assertEquals(new OrNode(n, n), NaryOrNode.createSimplestOr(n, n));
        assertEquals(new NaryOrNode(n, n, n), NaryOrNode.createSimplestOr(n, n, n));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         NaryOrNode.createSimplestOr();
                     });

        assertEquals(n, NaryOrNode.createSimplestOr(CollectionUtils.toList(n)));
        assertEquals(new OrNode(n, n), NaryOrNode.createSimplestOr(CollectionUtils.toList(n, n)));
        assertEquals(new NaryOrNode(n, n, n), NaryOrNode.createSimplestOr(CollectionUtils.toList(n, n, n)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         NaryOrNode.createSimplestOr(Collections.emptyList());
                     });

        assertEquals(new NaryOrNode(n, n, n, n), NaryOrNode.mergeSimplestOr(n, new OrNode(n, n), new NaryOrNode(n)));
    }

    @Test
    public void testNaryAnd() {
        final Node n = TrueNode.INSTANCE;
        assertEquals(n, NaryAndNode.createSimplestAnd(n));
        assertEquals(new AndNode(n, n), NaryAndNode.createSimplestAnd(n, n));
        assertEquals(new NaryAndNode(n, n, n), NaryAndNode.createSimplestAnd(n, n, n));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         NaryAndNode.createSimplestAnd();
                     });

        assertEquals(n, NaryAndNode.createSimplestAnd(CollectionUtils.toList(n)));
        assertEquals(new AndNode(n, n), NaryAndNode.createSimplestAnd(CollectionUtils.toList(n, n)));
        assertEquals(new NaryAndNode(n, n, n), NaryAndNode.createSimplestAnd(CollectionUtils.toList(n, n, n)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         NaryAndNode.createSimplestAnd(Collections.emptyList());
                     });

        assertEquals(new NaryAndNode(n, n, n, n), NaryAndNode.mergeSimplestAnd(n, new AndNode(n, n), new NaryAndNode(n)));
    }
}