package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.Item;
import cdc.applic.expressions.content.ItemSet;
import cdc.applic.expressions.content.ItemSetKind;
import cdc.applic.expressions.content.UncheckedSet;
import cdc.applic.expressions.content.ValueUtils;
import cdc.applic.expressions.parsing.ItemsParsing;

public class UncheckedSetTest {
    private static final Logger LOGGER = LogManager.getLogger(UncheckedSetTest.class);

    private static final ItemSetKind B = ItemSetKind.BOOLEAN;
    private static final ItemSetKind I = ItemSetKind.INTEGER;
    private static final ItemSetKind R = ItemSetKind.REAL;
    private static final ItemSetKind S = ItemSetKind.STRING;
    private static final ItemSetKind U = ItemSetKind.UNCHECKED;

    private static void checkConstruction(String expectedContent,
                                          String s,
                                          ItemSetKind... complianceKinds) {
        final UncheckedSet set = UncheckedSet.create(s);
        LOGGER.debug("Unchecked(" + s + "): " + set);
        assertEquals(expectedContent, set.getContent());
        final Set<ItemSetKind> accepted = new HashSet<>();
        for (final ItemSetKind kind : complianceKinds) {
            accepted.add(kind);
        }
        for (final ItemSetKind kind : ItemSetKind.values()) {
            assertEquals(accepted.contains(kind), set.isCompliantWith(kind), "Failed for " + kind);
        }
        assertEquals(set.isValid(), set.isEmpty() || complianceKinds.length > 1);
        assertFalse(set.isChecked());

        set.getChecked();
        set.getChecked();

        final UncheckedSet set2 = UncheckedSet.create(set.getItems());
        assertEquals(set, set2);
        final UncheckedSet set3 = UncheckedSet.create(set.getItems().toArray(new Item[0]));
        assertEquals(set, set3);

    }

    @Test
    public void testConstruction() {
        checkConstruction("", "", B, I, R, S, U);
        checkConstruction("1", "1", I, U);
        checkConstruction("1", " 1 ", I, U);
        checkConstruction("1.0", " 1.0 ", R, U);
        checkConstruction("1,2", " 1 , 2", I, U);
        checkConstruction("2,2,1,2", " 2,2 , 1, 2", I, U);
        checkConstruction("A", "A", S, U);
        checkConstruction("A", " A ", S, U);
        checkConstruction("A,B", " A , B", S, U);
        checkConstruction("A,A,B,A", " A,A , B, A", S, U);
        checkConstruction("A,1", " A,1", U);
        checkConstruction("A,\"1\"", " A,\"1\"", S, U);
        checkConstruction("\"A A\",1", " \"A A\" , 1", U);
        checkConstruction("false", "false", B, U);
        checkConstruction("\"false\"", "\"false\"", S, U);
        checkConstruction("\"1\"", "\"1\"", S, U);
        checkConstruction("1,A", " 1, A ", U);
        checkConstruction("1,true", " 1, TRUE  ", U);
        checkConstruction("\"1A\"", "\"1A\"", S, U);

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create((Item) null);
                     });
    }

    @Test
    public void checkContains() {
        assertFalse(UncheckedSet.EMPTY.contains(ValueUtils.create("1")));
        assertTrue(UncheckedSet.create("1").contains(ValueUtils.create("1")));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("1").contains(ValueUtils.create("1.0"));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("1,A").contains(ValueUtils.create("1"));
                     });
    }

    private static void checkSetUnion(String s1,
                                      String s2,
                                      String content) {
        final UncheckedSet set1 = UncheckedSet.create(s1);
        final UncheckedSet set2 = UncheckedSet.create(s2);
        final ItemSet set = set1.union(set2);
        LOGGER.debug(set1 + " unchecked union " + set2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    @Test
    public void testSetUnion() {
        checkSetUnion("", "", "");
        checkSetUnion("1,10", "2~9", "1~10");
        checkSetUnion("1,2", "2~10", "1~10");
        checkSetUnion("1", "2", "1~2");
        checkSetUnion("1", "3", "1,3");
        checkSetUnion("A", "", "A");
        checkSetUnion("", "A", "A");
        checkSetUnion("A", "A", "A");
        checkSetUnion("A", "A", "A");
        checkSetUnion("A", "B", "A,B");
        checkSetUnion("A,B", "B", "A,B");
        checkSetUnion("A,B", "A,B", "A,B");
        checkSetUnion("A,B", "B,A", "A,B");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("A").union(UncheckedSet.create("1"));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("A,1").union(UncheckedSet.create("1"));
                     });
    }

    private static void checkItemUnion(String s1,
                                       String i2,
                                       String content) {
        final UncheckedSet set1 = UncheckedSet.create(s1);
        final Item item2 = ItemsParsing.toItems(i2).get(0);
        final ItemSet set = set1.union(item2);
        LOGGER.debug(set1 + " unchecked union " + item2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    @Test
    public void testItemUnion() {
        checkItemUnion("1", "2", "1~2");
        checkItemUnion("", "2", "2");
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("1").union(ItemsParsing.toItems("A").get(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("1,A").union(ItemsParsing.toItems("A").get(0));
                     });
    }

    private static void checkSetIntersection(String s1,
                                             String s2,
                                             String content) {
        final UncheckedSet set1 = UncheckedSet.create(s1);
        final UncheckedSet set2 = UncheckedSet.create(s2);
        final ItemSet set = set1.intersection(set2);
        LOGGER.debug(set1 + " unchecked intersection " + set2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    @Test
    public void testSetIntersection() {
        checkSetIntersection("", "", "");
        checkSetIntersection("1", "", "");
        checkSetIntersection("1~2,5", "", "");
        checkSetIntersection("1~10", "5~15", "5~10");
        checkSetIntersection("A", "", "");
        checkSetIntersection("A,B", "", "");
        checkSetIntersection("", "A", "");
        checkSetIntersection("", "A,B", "");
        checkSetIntersection("A", "A", "A");
        checkSetIntersection("A,B", "A", "A");
        checkSetIntersection("B,A", "A", "A");
        checkSetIntersection("B,A", "A,B", "B,A");
        checkSetIntersection("A", "B", "");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("A").intersection(UncheckedSet.create("1"));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("A,1").intersection(UncheckedSet.create("1"));
                     });
    }

    private static void checkItemIntersection(String s1,
                                              String i2,
                                              String content) {
        final UncheckedSet set1 = UncheckedSet.create(s1);
        final Item item2 = ItemsParsing.toItems(i2).get(0);
        final ItemSet set = set1.intersection(item2);
        LOGGER.debug(set1 + " unchecked intersection " + item2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    @Test
    public void testItemIntersection() {
        checkItemIntersection("1", "2", "");
        checkItemIntersection("", "2", "");
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("1").intersection(ItemsParsing.toItems("A").get(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("1,A").intersection(ItemsParsing.toItems("A").get(0));
                     });
    }

    private static void checkSetRemove(String s1,
                                       String s2,
                                       String content) {
        final UncheckedSet set1 = UncheckedSet.create(s1);
        final UncheckedSet set2 = UncheckedSet.create(s2);
        final ItemSet set = set1.remove(set2);
        LOGGER.debug(set1 + " unchecked remove " + set2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    @Test
    public void testSetRemove() {
        checkSetRemove("", "", "");
        checkSetRemove("1,3~10", "", "1,3~10");
        checkSetRemove("1~10", "1", "2~10");
        checkSetRemove("1~10", "2", "1,3~10");
        checkSetRemove("1~10", "9", "1~8,10");
        checkSetRemove("1~10", "10", "1~9");
        checkSetRemove("1~10", "2~8", "1,9~10");
        checkSetRemove("1~10", "3~8", "1~2,9~10");
        checkSetRemove("A", "", "A");
        checkSetRemove("A,B", "", "A,B");
        checkSetRemove("A,B,C", "", "A,B,C");
        checkSetRemove("", "A", "");
        checkSetRemove("A", "A", "");
        checkSetRemove("A,B", "A", "B");
        checkSetRemove("A,B,C", "A", "B,C");
        checkSetRemove("", "B", "");
        checkSetRemove("A", "B", "A");
        checkSetRemove("A,B", "B", "A");
        checkSetRemove("A,B,C", "B", "A,C");
        checkSetRemove("", "C", "");
        checkSetRemove("A", "C", "A");
        checkSetRemove("A,B", "C", "A,B");
        checkSetRemove("A,B,C", "C", "A,B");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("A").remove(UncheckedSet.create("1"));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("A,1").remove(UncheckedSet.create("1"));
                     });
    }

    private static void checkItemRemove(String s1,
                                        String i2,
                                        String content) {
        final UncheckedSet set1 = UncheckedSet.create(s1);
        final Item item2 = ItemsParsing.toItems(i2).get(0);
        final ItemSet set = set1.remove(item2);
        LOGGER.debug(set1 + " unchecked remove " + item2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    @Test
    public void testItemRemove() {
        checkItemRemove("1", "2", "1");
        checkItemRemove("", "2", "");
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("1").remove(ItemsParsing.toItems("A").get(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.create("1,A").remove(ItemsParsing.toItems("A").get(0));
                     });
    }

    @Test
    public void testEquals() {
        assertEquals(UncheckedSet.EMPTY, UncheckedSet.EMPTY);
        assertEquals(UncheckedSet.EMPTY, IntegerSet.EMPTY);
        assertNotEquals(UncheckedSet.EMPTY, IntegerSet.create("1"));
        assertNotEquals(UncheckedSet.EMPTY, null);
        assertNotEquals(UncheckedSet.EMPTY, "Hello");
    }

    @Test
    public void testHashCode() {
        assertEquals(UncheckedSet.EMPTY.hashCode(), UncheckedSet.EMPTY.hashCode());
    }

}