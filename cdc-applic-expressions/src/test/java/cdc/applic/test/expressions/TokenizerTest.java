package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.issues.InvalidSyntaxException;
import cdc.applic.expressions.parsing.Token;
import cdc.applic.expressions.parsing.TokenType;
import cdc.applic.expressions.parsing.Tokenizer;

public class TokenizerTest {
    private static final Logger LOGGER = LogManager.getLogger(TokenizerTest.class);

    private static class Reduced {
        private final TokenType type;
        private final String text;

        public Reduced(TokenType type,
                       String text) {
            this.type = type;
            this.text = text;
        }

        public Reduced(Token token) {
            this(token.getType(),
                 token.getText());
        }

        @Override
        public int hashCode() {
            return Objects.hash(type,
                                text);
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Reduced)) {
                return false;
            }
            final Reduced other = (Reduced) object;
            return type == other.type
                    && text.equals(other.text);
        }

        @Override
        public String toString() {
            return "[" + type + " '" + text + "']";
        }
    }

    private static Reduced r(TokenType type,
                             String text) {
        return new Reduced(type, text);
    }

    private static void checkSuccess(String expression,
                                     Reduced... expected) {
        LOGGER.debug("----------------------------------");
        LOGGER.debug("expression:'" + expression + "'");
        try {
            final List<Token> tokens = Tokenizer.tokenize(expression);
            assertEquals(expected.length, tokens.size());
            for (int index = 0; index < tokens.size(); index++) {
                assertEquals(expected[index], new Reduced(tokens.get(index)));
            }
        } catch (final InvalidSyntaxException e) {
            LOGGER.error("Failed to tokenize: '" + expression + "'", e);
            throw e;
        }
    }

    private static void checkFailure(String expression,
                                     InvalidSyntaxException.Detail detail) {
        LOGGER.debug("----------------------------------");
        LOGGER.debug("expression:'" + expression + "'");
        try {
            Tokenizer.tokenize(expression);
        } catch (final InvalidSyntaxException e) {
            assertEquals(detail, e.getDetail());
            LOGGER.debug(e.getFullMessage());
            throw e;
        }
    }

    @Test
    public void testOneTokenMath() {
        checkSuccess("∧", r(TokenType.AND, "∧"));
        checkSuccess("∨", r(TokenType.OR, "∨"));
        checkSuccess("∈", r(TokenType.IN, "∈"));
        checkSuccess("¬", r(TokenType.NOT, "¬"));
        checkSuccess("∉", r(TokenType.NOT_IN, "∉"));
        checkSuccess("≠", r(TokenType.NOT_EQUAL, "≠"));
        checkSuccess("→", r(TokenType.IMPL, "→"));
        checkSuccess("↔", r(TokenType.EQUIV, "↔"));
        checkSuccess("⊤", r(TokenType.TRUE, "⊤"));
        checkSuccess("⊥", r(TokenType.FALSE, "⊥"));
    }

    @Test
    public void testOneToken() {
        checkSuccess("and", r(TokenType.AND, "and"));
        checkSuccess("AND", r(TokenType.AND, "AND"));
        checkSuccess("&", r(TokenType.AND, "&"));

        checkSuccess("or", r(TokenType.OR, "or"));
        checkSuccess("OR", r(TokenType.OR, "OR"));
        checkSuccess("|", r(TokenType.OR, "|"));

        checkSuccess("in", r(TokenType.IN, "in"));
        checkSuccess("IN", r(TokenType.IN, "IN"));
        checkSuccess("<:", r(TokenType.IN, "<:"));

        checkSuccess("not", r(TokenType.NOT, "not"));
        checkSuccess("NOT", r(TokenType.NOT, "NOT"));
        checkSuccess("!", r(TokenType.NOT, "!"));

        checkSuccess("not in", r(TokenType.NOT_IN, "not in"));
        checkSuccess("NOT IN", r(TokenType.NOT_IN, "NOT IN"));
        checkSuccess("!<:", r(TokenType.NOT_IN, "!<:"));

        checkSuccess("imp", r(TokenType.IMPL, "imp"));
        checkSuccess("->", r(TokenType.IMPL, "->"));

        checkSuccess("iff", r(TokenType.EQUIV, "iff"));
        checkSuccess("<->", r(TokenType.EQUIV, "<->"));

        checkSuccess("=", r(TokenType.EQUAL, "="));
        checkSuccess("!=", r(TokenType.NOT_EQUAL, "!="));

        checkSuccess("(", r(TokenType.OPEN_PAREN, "("));
        checkSuccess(")", r(TokenType.CLOSE_PAREN, ")"));

        checkSuccess("{", r(TokenType.OPEN_SET, "{"));
        checkSuccess("}", r(TokenType.CLOSE_SET, "}"));

        checkSuccess("to", r(TokenType.TO, "to"));
        checkSuccess("TO", r(TokenType.TO, "TO"));
        checkSuccess("~", r(TokenType.TO, "~"));

        checkSuccess(",", r(TokenType.VALUES_SEP, ","));

        checkSuccess("true", r(TokenType.TRUE, "true"));
        checkSuccess("TRUE", r(TokenType.TRUE, "TRUE"));

        checkSuccess("false", r(TokenType.FALSE, "false"));
        checkSuccess("FALSE", r(TokenType.FALSE, "FALSE"));

        checkSuccess("10", r(TokenType.INTEGER, "10"));
        checkSuccess("+10", r(TokenType.INTEGER, "+10"));
        checkSuccess("-10", r(TokenType.INTEGER, "-10"));

        checkSuccess("10.0", r(TokenType.REAL, "10.0"));
        checkSuccess("+10.0", r(TokenType.REAL, "+10.0"));
        checkSuccess("-10.0", r(TokenType.REAL, "-10.0"));
        checkSuccess("10.0e10", r(TokenType.REAL, "10.0e10"));
        checkSuccess("10.0e+10", r(TokenType.REAL, "10.0e+10"));
        checkSuccess("10.0e-10", r(TokenType.REAL, "10.0e-10"));
        checkSuccess("10.0E+10", r(TokenType.REAL, "10.0E+10"));
        checkSuccess("10.0E-10", r(TokenType.REAL, "10.0E-10"));

        checkSuccess("$", r(TokenType.TEXT, "$"));
        checkSuccess("ä", r(TokenType.TEXT, "ä"));
        checkSuccess("value", r(TokenType.TEXT, "value"));
        checkSuccess("a", r(TokenType.TEXT, "a"));
        checkSuccess("an", r(TokenType.TEXT, "an"));
        checkSuccess("aa", r(TokenType.TEXT, "aa"));
        checkSuccess("ana", r(TokenType.TEXT, "ana"));
        checkSuccess("aad", r(TokenType.TEXT, "aad"));
        checkSuccess("anda", r(TokenType.TEXT, "anda"));

        checkSuccess("o", r(TokenType.TEXT, "o"));
        checkSuccess("oo", r(TokenType.TEXT, "oo"));
        checkSuccess("oro", r(TokenType.TEXT, "oro"));

        checkSuccess("i", r(TokenType.TEXT, "i"));
        checkSuccess("ini", r(TokenType.TEXT, "ini"));
        checkSuccess("im", r(TokenType.TEXT, "im"));
        checkSuccess("imi", r(TokenType.TEXT, "imi"));
        checkSuccess("impi", r(TokenType.TEXT, "impi"));
        checkSuccess("if", r(TokenType.TEXT, "if"));
        checkSuccess("ifg", r(TokenType.TEXT, "ifg"));
        checkSuccess("ig", r(TokenType.TEXT, "ig"));
        checkSuccess("iffi", r(TokenType.TEXT, "iffi"));

        checkSuccess("f", r(TokenType.TEXT, "f"));
        checkSuccess("fa", r(TokenType.TEXT, "fa"));
        checkSuccess("fb", r(TokenType.TEXT, "fb"));
        checkSuccess("fal", r(TokenType.TEXT, "fal"));
        checkSuccess("fam", r(TokenType.TEXT, "fam"));
        checkSuccess("fals", r(TokenType.TEXT, "fals"));
        checkSuccess("fala", r(TokenType.TEXT, "fala"));
        checkSuccess("falsf", r(TokenType.TEXT, "falsf"));
        checkSuccess("falte", r(TokenType.TEXT, "falte"));
        checkSuccess("faase", r(TokenType.TEXT, "faase"));
        checkSuccess("fblse", r(TokenType.TEXT, "fblse"));
        checkSuccess("falsef", r(TokenType.TEXT, "falsef"));

        checkSuccess("t", r(TokenType.TEXT, "t"));
        checkSuccess("ta", r(TokenType.TEXT, "ta"));
        checkSuccess("too", r(TokenType.TEXT, "too"));
        checkSuccess("tr", r(TokenType.TEXT, "tr"));
        checkSuccess("tru", r(TokenType.TEXT, "tru"));
        checkSuccess("truf", r(TokenType.TEXT, "truf"));
        checkSuccess("tra", r(TokenType.TEXT, "tra"));
        checkSuccess("truet", r(TokenType.TEXT, "truet"));
        checkSuccess("trae", r(TokenType.TEXT, "trae"));
        checkSuccess("tbue", r(TokenType.TEXT, "tbue"));

        checkSuccess("n", r(TokenType.TEXT, "n"));
        checkSuccess("no", r(TokenType.TEXT, "no"));
        checkSuccess("noo", r(TokenType.TEXT, "noo"));
        checkSuccess("notn", r(TokenType.TEXT, "notn"));
        checkSuccess("nat", r(TokenType.TEXT, "nat"));

        checkSuccess("+", r(TokenType.TEXT, "+"));
        checkSuccess("-", r(TokenType.TEXT, "-"));
        checkSuccess("+a", r(TokenType.TEXT, "+a"));
        checkSuccess("-a", r(TokenType.TEXT, "-a"));
        checkSuccess("<", r(TokenType.TEXT, "<"));
        checkSuccess("<-", r(TokenType.TEXT, "<-"));
        checkSuccess("<--", r(TokenType.TEXT, "<--"));
        checkSuccess("<+-", r(TokenType.TEXT, "<+-"));
        checkSuccess("-<", r(TokenType.TEXT, "-<"));
        checkSuccess("<>", r(TokenType.TEXT, "<>"));

        checkSuccess("\"value\"", r(TokenType.ESCAPED_TEXT, "\"value\""));
        checkSuccess("\"value\"   ", r(TokenType.ESCAPED_TEXT, "\"value\""));
        checkSuccess("\"false\"", r(TokenType.ESCAPED_TEXT, "\"false\""));
        checkSuccess("\"true\"", r(TokenType.ESCAPED_TEXT, "\"true\""));
        checkSuccess("\"or\"", r(TokenType.ESCAPED_TEXT, "\"or\""));
        checkSuccess("\"a b\"", r(TokenType.ESCAPED_TEXT, "\"a b\""));
        checkSuccess("\"-10\"", r(TokenType.ESCAPED_TEXT, "\"-10\""));
        checkSuccess("\"\"\"\"", r(TokenType.ESCAPED_TEXT, "\"\"\"\""));
        checkSuccess("not<+>", r(TokenType.TEXT, "not<+>"));
        checkSuccess("not<--", r(TokenType.TEXT, "not<--"));
        checkSuccess("notä", r(TokenType.TEXT, "notä"));
    }

    @Test
    public void testManyTokens() {
        checkSuccess("not<->",
                     r(TokenType.NOT, "not"),
                     r(TokenType.EQUIV, "<->"));
        checkSuccess("!<",
                     r(TokenType.NOT, "!"),
                     r(TokenType.TEXT, "<"));
        checkSuccess("!<",
                     r(TokenType.NOT, "!"),
                     r(TokenType.TEXT, "<"));
        checkSuccess("!aa",
                     r(TokenType.NOT, "!"),
                     r(TokenType.TEXT, "aa"));
        checkSuccess("!<a",
                     r(TokenType.NOT, "!"),
                     r(TokenType.TEXT, "<a"));
        checkSuccess("!<:<",
                     r(TokenType.NOT_IN, "!<:"),
                     r(TokenType.TEXT, "<"));
        checkSuccess("not an",
                     r(TokenType.NOT, "not"),
                     r(TokenType.TEXT, "an"));
        checkSuccess("not ia",
                     r(TokenType.NOT, "not"),
                     r(TokenType.TEXT, "ia"));
        checkSuccess("not ini",
                     r(TokenType.NOT, "not"),
                     r(TokenType.TEXT, "ini"));
        checkSuccess("a<:",
                     r(TokenType.TEXT, "a"),
                     r(TokenType.IN, "<:"));
        checkSuccess("a<->",
                     r(TokenType.TEXT, "a"),
                     r(TokenType.EQUIV, "<->"));
        checkSuccess("a->",
                     r(TokenType.TEXT, "a"),
                     r(TokenType.IMPL, "->"));
        checkSuccess(" a -> b ",
                     r(TokenType.TEXT, "a"),
                     r(TokenType.IMPL, "->"),
                     r(TokenType.TEXT, "b"));
        checkSuccess(" a <-> b ",
                     r(TokenType.TEXT, "a"),
                     r(TokenType.EQUIV, "<->"),
                     r(TokenType.TEXT, "b"));
        checkSuccess(" a = b ",
                     r(TokenType.TEXT, "a"),
                     r(TokenType.EQUAL, "="),
                     r(TokenType.TEXT, "b"));
        checkSuccess(" a != b ",
                     r(TokenType.TEXT, "a"),
                     r(TokenType.NOT_EQUAL, "!="),
                     r(TokenType.TEXT, "b"));

        checkSuccess(" a < b ",
                     r(TokenType.TEXT, "a"),
                     r(TokenType.TEXT, "<"),
                     r(TokenType.TEXT, "b"));
        checkSuccess(" a <- b ",
                     r(TokenType.TEXT, "a"),
                     r(TokenType.TEXT, "<-"),
                     r(TokenType.TEXT, "b"));
        checkSuccess(" a <a> b ",
                     r(TokenType.TEXT, "a"),
                     r(TokenType.TEXT, "<a>"),
                     r(TokenType.TEXT, "b"));
    }

    @Test
    public void testInvalidToken() {
        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("\"", InvalidSyntaxException.Detail.TOKENIZE_MISSING_CLOSING_QUOTES);
                     });
        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("\"\"\"", InvalidSyntaxException.Detail.TOKENIZE_MISSING_CLOSING_QUOTES);
                     });
        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("\"\"\"   ", InvalidSyntaxException.Detail.TOKENIZE_MISSING_CLOSING_QUOTES);
                     });

        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("\"\"", InvalidSyntaxException.Detail.TOKENIZE_EMPTY_ESCAPED_TEXT);
                     });

        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("10.", InvalidSyntaxException.Detail.TOKENIZE_INVALID_NUMBER);
                     });
        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("+10.", InvalidSyntaxException.Detail.TOKENIZE_INVALID_NUMBER);
                     });
        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("-10.", InvalidSyntaxException.Detail.TOKENIZE_INVALID_NUMBER);
                     });
        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("10.0e", InvalidSyntaxException.Detail.TOKENIZE_INVALID_NUMBER);
                     });
        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("10.0e+", InvalidSyntaxException.Detail.TOKENIZE_INVALID_NUMBER);
                     });
        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("10.0e-", InvalidSyntaxException.Detail.TOKENIZE_INVALID_NUMBER);
                     });
        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("10.0ee", InvalidSyntaxException.Detail.TOKENIZE_INVALID_NUMBER);
                     });
        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("10a", InvalidSyntaxException.Detail.TOKENIZE_MISSING_BOUNDARY);
                     });
        assertThrows(InvalidSyntaxException.class,
                     () -> {
                         checkFailure("10.0a", InvalidSyntaxException.Detail.TOKENIZE_MISSING_BOUNDARY);
                     });
    }

    @Test
    public void testNextToken() {
        final Tokenizer tokenizer = new Tokenizer();
        tokenizer.init("");
        assertEquals("", tokenizer.getExpression());
        for (int index = 0; index < 10; index++) {
            final Token token = tokenizer.nextToken();
            assertEquals(Token.TOK_EPSILON, token);
        }
    }

    @Test
    public void testCompress() {
        assertEquals("A", Tokenizer.compress("A"));
        assertEquals("A&B", Tokenizer.compress("A and B"));
        assertEquals("A|B", Tokenizer.compress("A or B"));
        assertEquals("(A|B)&C", Tokenizer.compress("(A or B) and C"));
    }
}