package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.literals.EscapingContext;
import cdc.applic.expressions.literals.EscapingMode;
import cdc.applic.expressions.literals.EscapingUtils;

public class EscapingUtilsTest {

    private static void testNeedsEscape(String s,
                                        boolean nameContext,
                                        boolean numberContext,
                                        boolean booleanContext) {
        assertEquals(nameContext, EscapingUtils.needsEscape(s, EscapingContext.NAME), "NAME Context");
        assertEquals(numberContext, EscapingUtils.needsEscape(s, EscapingContext.NUMBER), "NUMBER Context");
        assertEquals(booleanContext, EscapingUtils.needsEscape(s, EscapingContext.BOOLEAN), "BOOLEAN Context");
    }

    @Test
    public void testNeedsEscape() {
        // Illegal literals
        testNeedsEscape(null, false, false, false);
        testNeedsEscape("", false, false, false);
        testNeedsEscape("|", false, false, false);
        testNeedsEscape("~", false, false, false);

        // Legal literals (but not necessarily valid)
        testNeedsEscape(" ", true, true, true);
        testNeedsEscape("{", true, true, true);
        testNeedsEscape("}", true, true, true);
        testNeedsEscape("=", true, true, true);
        testNeedsEscape("<", true, true, true);
        testNeedsEscape(">", true, true, true);
        testNeedsEscape("-", false, false, false);
        testNeedsEscape("(", true, true, true);
        testNeedsEscape(")", true, true, true);
        testNeedsEscape("!", true, true, true);
        testNeedsEscape("&", true, true, true);
        testNeedsEscape("\"", true, true, true);
        testNeedsEscape("ABCDEFGHIJKLMNOPQRSTUVWXYZ", false, false, false);
        testNeedsEscape("abcdefghijklmnopqrstuvwxyz", false, false, false);
        testNeedsEscape("0123456789", true, false, false);
        testNeedsEscape("+1", true, false, false);
        testNeedsEscape("-1", true, false, false);
        testNeedsEscape("A1", false, false, false);
        testNeedsEscape("1A", true, false, false);
        testNeedsEscape("not", true, false, false);
        testNeedsEscape("and", true, false, false);
        testNeedsEscape("or", true, false, false);
        testNeedsEscape("imp", true, false, false);
        testNeedsEscape("iff", true, false, false);
        testNeedsEscape("to", true, false, false);
        testNeedsEscape("10", true, false, false);
        testNeedsEscape("10.0", true, false, false);
    }

    @Test
    public void testIsLegalLiteral() {
        assertTrue(EscapingUtils.isLegalLiteral(" "));
        assertTrue(EscapingUtils.isLegalLiteral("1"));
        assertFalse(EscapingUtils.isLegalLiteral("|"));
        assertFalse(EscapingUtils.isLegalLiteral("~"));
    }

    @Test
    public void testIsValidEscapedText() {
        assertFalse(EscapingUtils.isValidEscapedText(null));
        assertFalse(EscapingUtils.isValidEscapedText("abc"));
        assertFalse(EscapingUtils.isValidEscapedText("\"abc"));
        assertTrue(EscapingUtils.isValidEscapedText("\"abc\""));
        assertFalse(EscapingUtils.isValidEscapedText("\"\"\""));
        assertTrue(EscapingUtils.isValidEscapedText("\"\"\"\""));
        assertFalse(EscapingUtils.isValidEscapedText("\"\"\"\"\""));
        assertTrue(EscapingUtils.isValidEscapedText("\"\"\"\"\"\""));
        assertFalse(EscapingUtils.isValidEscapedText("\"\" \"\"\"\""));
        assertTrue(EscapingUtils.isValidEscapedText("\"\"\" \"\"\""));
        assertFalse(EscapingUtils.isValidEscapedText("\"\"\"\" \"\""));
        assertFalse(EscapingUtils.isValidEscapedText("\"ab\"\""));
        assertFalse(EscapingUtils.isValidEscapedText("\"ab\"c\""));
        assertTrue(EscapingUtils.isValidEscapedText("\"ab\"\"c\""));
        assertTrue(EscapingUtils.isValidEscapedText("\"ab\"\"\""));
        assertFalse(EscapingUtils.isValidEscapedText("\"\""));
        assertTrue(EscapingUtils.isValidEscapedText("\" \""));
    }

    private static void testIsValidUnescapedText(String s,
                                                 boolean nameContext,
                                                 boolean numberContext,
                                                 boolean booleanContext) {
        assertEquals(nameContext, EscapingUtils.isValidUnescapedText(s, EscapingContext.NAME), "NAME Context");
        assertEquals(numberContext, EscapingUtils.isValidUnescapedText(s, EscapingContext.NUMBER), "NUMBER Context");
        assertEquals(booleanContext, EscapingUtils.isValidUnescapedText(s, EscapingContext.BOOLEAN), "BOOLEAN Context");
    }

    @Test
    public void testIsValidUnescapedText() {
        // Illegal literals
        testIsValidUnescapedText(null, false, false, false);
        testIsValidUnescapedText("", false, false, false);
        testIsValidUnescapedText("~", false, false, false);
        testIsValidUnescapedText("|", false, false, false);

        // Legal literals (but not necessarily valid)
        testIsValidUnescapedText(" ", false, false, false);
        testIsValidUnescapedText("{", false, false, false);
        testIsValidUnescapedText("}", false, false, false);
        testIsValidUnescapedText("=", false, false, false);
        testIsValidUnescapedText("<", false, false, false);
        testIsValidUnescapedText(">", false, false, false);
        testIsValidUnescapedText("(", false, false, false);
        testIsValidUnescapedText(")", false, false, false);
        testIsValidUnescapedText("!", false, false, false);
        testIsValidUnescapedText("|", false, false, false);
        testIsValidUnescapedText("&", false, false, false);
        testIsValidUnescapedText("\"", false, false, false);
        testIsValidUnescapedText("abc", true, true, true);
        testIsValidUnescapedText("-", true, true, true);
        testIsValidUnescapedText("+", true, true, true);
        testIsValidUnescapedText("0123", false, true, true);
    }

    private static void testIsValidText(String s,
                                        boolean nameContext,
                                        boolean numberContext,
                                        boolean booleanContext) {
        assertEquals(nameContext, EscapingUtils.isValidText(s, EscapingContext.NAME), "NAME Context");
        assertEquals(numberContext, EscapingUtils.isValidText(s, EscapingContext.NUMBER), "NUMBER Context");
        assertEquals(booleanContext, EscapingUtils.isValidText(s, EscapingContext.BOOLEAN), "BOOLEAN Context");
    }

    @Test
    public void testIsValidText() {
        // Illegal literals
        testIsValidText(null, false, false, false);
        testIsValidText("", false, false, false);
        testIsValidText("~", false, false, false);
        testIsValidText("|", false, false, false);

        // Legal literals (but not necessarily valid)
        testIsValidText(" ", false, false, false);
        testIsValidText("{", false, false, false);
        testIsValidText("}", false, false, false);
        testIsValidText("=", false, false, false);
        testIsValidText("<", false, false, false);
        testIsValidText(">", false, false, false);
        testIsValidText("(", false, false, false);
        testIsValidText(")", false, false, false);
        testIsValidText("!", false, false, false);
        testIsValidText("&", false, false, false);
        testIsValidText("\"", false, false, false);
        testIsValidText("abc", true, true, true);
        testIsValidText("-", true, true, true);
        testIsValidText("+", true, true, true);
        testIsValidText("0123", false, true, true);
        testIsValidText("true", false, true, true);

        testIsValidText("\"AAA\"", true, true, true);

    }

    private static void checkEscaping(String s,
                                      String escaped) {
        final String e = EscapingUtils.escape(s);
        assertEquals(e, escaped);
        final String f = EscapingUtils.unescape(e);
        assertEquals(s, f);
    }

    @Test
    public void testEscaping() {
        checkEscaping("", "\"\"");
        checkEscaping("A", "\"A\"");
        checkEscaping("\"", "\"\"\"\"");
        checkEscaping("\"A", "\"\"\"A\"");
    }

    private static void testEscapeIfNeeded(String nameText,
                                           String numberText,
                                           String booleanText,
                                           String text) {
        assertEquals(nameText, EscapingUtils.escapeIfNeeded(text, EscapingContext.NAME), "NAME Context");
        assertEquals(numberText, EscapingUtils.escapeIfNeeded(text, EscapingContext.NUMBER), "NUMBER Context");
        assertEquals(booleanText, EscapingUtils.escapeIfNeeded(text, EscapingContext.BOOLEAN), "BOOLEAN Context");
    }

    @Test
    public void testEscapeIfNeeded() {
        testEscapeIfNeeded(null, null, null, null);
        testEscapeIfNeeded("", "", "", "");
        testEscapeIfNeeded("A", "A", "A", "A");
        testEscapeIfNeeded("\"A A\"", "\"A A\"", "\"A A\"", "A A");
        testEscapeIfNeeded("\"1\"", "1", "1", "1");
        testEscapeIfNeeded("+", "+", "+", "+");
        testEscapeIfNeeded("-", "-", "-", "-");
    }

    @Test
    public void testGetString() {
        // NAME

        assertEquals("\"A\"", EscapingUtils.getString("A", EscapingMode.ESCAPED, EscapingContext.NAME));
        assertEquals("A", EscapingUtils.getString("A", EscapingMode.NON_ESCAPED, EscapingContext.NAME));
        assertEquals("A", EscapingUtils.getString("A", EscapingMode.PROTECTED, EscapingContext.NAME));

        assertEquals("\"A A\"", EscapingUtils.getString("A A", EscapingMode.ESCAPED, EscapingContext.NAME));
        assertEquals("A A", EscapingUtils.getString("A A", EscapingMode.NON_ESCAPED, EscapingContext.NAME));
        assertEquals("\"A A\"", EscapingUtils.getString("A A", EscapingMode.PROTECTED, EscapingContext.NAME));

        assertEquals("\"1\"", EscapingUtils.getString("1", EscapingMode.ESCAPED, EscapingContext.NAME));
        assertEquals("1", EscapingUtils.getString("1", EscapingMode.NON_ESCAPED, EscapingContext.NAME));
        assertEquals("\"1\"", EscapingUtils.getString("1", EscapingMode.PROTECTED, EscapingContext.NAME));

        assertEquals("\"1 1\"", EscapingUtils.getString("1 1", EscapingMode.ESCAPED, EscapingContext.NAME));
        assertEquals("1 1", EscapingUtils.getString("1 1", EscapingMode.NON_ESCAPED, EscapingContext.NAME));
        assertEquals("\"1 1\"", EscapingUtils.getString("1 1", EscapingMode.PROTECTED, EscapingContext.NAME));

        assertEquals("\"true\"", EscapingUtils.getString("true", EscapingMode.ESCAPED, EscapingContext.NAME));
        assertEquals("true", EscapingUtils.getString("true", EscapingMode.NON_ESCAPED, EscapingContext.NAME));
        assertEquals("\"true\"", EscapingUtils.getString("true", EscapingMode.PROTECTED, EscapingContext.NAME));

        // NUMBER

        assertEquals("\"A\"", EscapingUtils.getString("A", EscapingMode.ESCAPED, EscapingContext.NUMBER));
        assertEquals("A", EscapingUtils.getString("A", EscapingMode.NON_ESCAPED, EscapingContext.NUMBER));
        assertEquals("A", EscapingUtils.getString("A", EscapingMode.PROTECTED, EscapingContext.NUMBER));

        assertEquals("\"A A\"", EscapingUtils.getString("A A", EscapingMode.ESCAPED, EscapingContext.NUMBER));
        assertEquals("A A", EscapingUtils.getString("A A", EscapingMode.NON_ESCAPED, EscapingContext.NUMBER));
        assertEquals("\"A A\"", EscapingUtils.getString("A A", EscapingMode.PROTECTED, EscapingContext.NUMBER));

        assertEquals("\"1\"", EscapingUtils.getString("1", EscapingMode.ESCAPED, EscapingContext.NUMBER));
        assertEquals("1", EscapingUtils.getString("1", EscapingMode.NON_ESCAPED, EscapingContext.NUMBER));
        assertEquals("1", EscapingUtils.getString("1", EscapingMode.PROTECTED, EscapingContext.NUMBER));

        assertEquals("\"1 1\"", EscapingUtils.getString("1 1", EscapingMode.ESCAPED, EscapingContext.NUMBER));
        assertEquals("1 1", EscapingUtils.getString("1 1", EscapingMode.NON_ESCAPED, EscapingContext.NUMBER));
        assertEquals("\"1 1\"", EscapingUtils.getString("1 1", EscapingMode.PROTECTED, EscapingContext.NUMBER));

        assertEquals("\"true\"", EscapingUtils.getString("true", EscapingMode.ESCAPED, EscapingContext.NUMBER));
        assertEquals("true", EscapingUtils.getString("true", EscapingMode.NON_ESCAPED, EscapingContext.NUMBER));
        assertEquals("true", EscapingUtils.getString("true", EscapingMode.PROTECTED, EscapingContext.NUMBER));

        // BOOLEAN

        assertEquals("\"A\"", EscapingUtils.getString("A", EscapingMode.ESCAPED, EscapingContext.BOOLEAN));
        assertEquals("A", EscapingUtils.getString("A", EscapingMode.NON_ESCAPED, EscapingContext.BOOLEAN));
        assertEquals("A", EscapingUtils.getString("A", EscapingMode.PROTECTED, EscapingContext.BOOLEAN));

        assertEquals("\"A A\"", EscapingUtils.getString("A A", EscapingMode.ESCAPED, EscapingContext.BOOLEAN));
        assertEquals("A A", EscapingUtils.getString("A A", EscapingMode.NON_ESCAPED, EscapingContext.BOOLEAN));
        assertEquals("\"A A\"", EscapingUtils.getString("A A", EscapingMode.PROTECTED, EscapingContext.BOOLEAN));

        assertEquals("\"1\"", EscapingUtils.getString("1", EscapingMode.ESCAPED, EscapingContext.BOOLEAN));
        assertEquals("1", EscapingUtils.getString("1", EscapingMode.NON_ESCAPED, EscapingContext.BOOLEAN));
        assertEquals("1", EscapingUtils.getString("1", EscapingMode.PROTECTED, EscapingContext.BOOLEAN));

        assertEquals("\"1 1\"", EscapingUtils.getString("1 1", EscapingMode.ESCAPED, EscapingContext.BOOLEAN));
        assertEquals("1 1", EscapingUtils.getString("1 1", EscapingMode.NON_ESCAPED, EscapingContext.BOOLEAN));
        assertEquals("\"1 1\"", EscapingUtils.getString("1 1", EscapingMode.PROTECTED, EscapingContext.BOOLEAN));

        assertEquals("\"true\"", EscapingUtils.getString("true", EscapingMode.ESCAPED, EscapingContext.BOOLEAN));
        assertEquals("true", EscapingUtils.getString("true", EscapingMode.NON_ESCAPED, EscapingContext.BOOLEAN));
        assertEquals("true", EscapingUtils.getString("true", EscapingMode.PROTECTED, EscapingContext.BOOLEAN));
    }

    @Test
    public void testisValidTextInAtLeastOneContext() {
        assertFalse(EscapingUtils.isValidTextInAtLeastOneContext(null));
        assertFalse(EscapingUtils.isValidTextInAtLeastOneContext(""));
        assertTrue(EscapingUtils.isValidTextInAtLeastOneContext("true"));
        assertTrue(EscapingUtils.isValidTextInAtLeastOneContext("to"));
        assertTrue(EscapingUtils.isValidTextInAtLeastOneContext("10"));
        assertTrue(EscapingUtils.isValidTextInAtLeastOneContext("Hello"));
    }
}