package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.Item;
import cdc.applic.expressions.content.ItemSet;
import cdc.util.lang.CollectionUtils;

public class IntegerSetTest {
    // private static final Logger LOGGER = LogManager.getLogger(RangeSetTest.class);

    private static IntegerRange r(int value) {
        return IntegerRange.create(value);
    }

    private static IntegerRange r(int min,
                                  int max) {
        return IntegerRange.create(min, max);
    }

    @SafeVarargs
    private static IntegerSet s(IntegerRange... ranges) {
        return IntegerSet.create(ranges);
    }

    @SafeVarargs
    private static void checkConstruction(String expected,
                                          IntegerRange... ranges) {
        // LOGGER.info(Arrays.toString(ranges) + " " + expected);
        final IntegerSet set1 = IntegerSet.create(ranges);
        final IntegerSet set2 = IntegerSet.create(CollectionUtils.toList(ranges));
        // LOGGER.info(Arrays.toString(ranges) + " " + set1);
        assertEquals(expected, set1.toString());
        assertEquals(set1, set1);
        assertEquals(set1, set2);
        assertTrue(set1.isChecked());
        assertTrue(set1.isValid());
        assertEquals(set1, set1.getChecked());
    }

    @Test
    public void testConstruction() {
        assertTrue(IntegerSet.EMPTY.isEmpty());

        checkConstruction("{}");
        checkConstruction("{1}", r(1));
        checkConstruction("{}", r(1, -1));
        checkConstruction("{1~10}", r(1, 10));
        checkConstruction("{1~10,20}", r(1, 10), r(20));
        checkConstruction("{1~10,12}", r(1, 10), r(12));
        checkConstruction("{1~11}", r(1, 10), r(11));
        checkConstruction("{1~10}", r(1, 10), r(10));
        checkConstruction("{1~20}", r(1, 10), r(9, 20));
        checkConstruction("{1~10}", r(1, 10), r(20, 10));
        checkConstruction("{}", r(11, 10), r(20, 10));
        checkConstruction("{1~2}", r(1), r(2));

        checkConstruction("{1}", r(1));

        checkConstruction("{1~2}", r(1), r(2));
        checkConstruction("{1~2}", r(2), r(1));

        checkConstruction("{-1,1~2}", r(-1), r(1, 2));
        checkConstruction("{-1,1~2}", r(1, 2), r(-1));
        checkConstruction("{0~2}", r(0), r(1, 2));
        checkConstruction("{0~2}", r(1, 2), r(0));
        checkConstruction("{1~2}", r(1), r(1, 2));
        checkConstruction("{1~2}", r(1, 2), r(1));
        checkConstruction("{1~2}", r(2), r(1, 2));
        checkConstruction("{1~2}", r(1, 2), r(2));
        checkConstruction("{1~3}", r(3), r(1, 2));
        checkConstruction("{1~3}", r(1, 2), r(3));
        checkConstruction("{1~2,4}", r(4), r(1, 2));
        checkConstruction("{1~2,4}", r(1, 2), r(4));

        checkConstruction("{0~3}", r(1, 2), r(0, 3));

        checkConstruction("{1~2,4~5,7~8}", r(4, 5), r(7, 8), r(1, 2));
        checkConstruction("{1~5,7~8}", r(4, 5), r(7, 8), r(1, 3));
        checkConstruction("{1~5,7~8}", r(4, 5), r(7, 8), r(1, 4));
        checkConstruction("{1~5,7~8}", r(4, 5), r(7, 8), r(1, 5));
        checkConstruction("{1~8}", r(4, 5), r(7, 8), r(1, 6));
        checkConstruction("{1~8}", r(4, 5), r(7, 8), r(1, 7));
        checkConstruction("{1~8}", r(4, 5), r(7, 8), r(1, 8));
        checkConstruction("{1~9}", r(4, 5), r(7, 8), r(1, 9));

        checkConstruction("{2,4~5,7~8}", r(4, 5), r(7, 8), r(2, 2));
        checkConstruction("{2~5,7~8}", r(4, 5), r(7, 8), r(2, 3));
        checkConstruction("{2~5,7~8}", r(4, 5), r(7, 8), r(2, 4));
        checkConstruction("{2~5,7~8}", r(4, 5), r(7, 8), r(2, 5));
        checkConstruction("{2~8}", r(4, 5), r(7, 8), r(2, 6));
        checkConstruction("{2~8}", r(4, 5), r(7, 8), r(2, 7));
        checkConstruction("{2~8}", r(4, 5), r(7, 8), r(2, 8));
        checkConstruction("{2~9}", r(4, 5), r(7, 8), r(2, 9));

        checkConstruction("{3~5,7~8}", r(4, 5), r(7, 8), r(3, 3));
        checkConstruction("{3~5,7~8}", r(4, 5), r(7, 8), r(3, 4));
        checkConstruction("{3~5,7~8}", r(4, 5), r(7, 8), r(3, 5));
        checkConstruction("{3~8}", r(4, 5), r(7, 8), r(3, 6));
        checkConstruction("{3~8}", r(4, 5), r(7, 8), r(3, 7));
        checkConstruction("{3~8}", r(4, 5), r(7, 8), r(3, 8));
        checkConstruction("{3~9}", r(4, 5), r(7, 8), r(3, 9));

        checkConstruction("{4~5,7~8}", r(4, 5), r(7, 8), r(4, 4));
        checkConstruction("{4~5,7~8}", r(4, 5), r(7, 8), r(4, 5));
        checkConstruction("{4~8}", r(4, 5), r(7, 8), r(4, 6));
        checkConstruction("{4~8}", r(4, 5), r(7, 8), r(4, 7));
        checkConstruction("{4~8}", r(4, 5), r(7, 8), r(4, 8));
        checkConstruction("{4~9}", r(4, 5), r(7, 8), r(4, 9));

        checkConstruction("{4~5,7~8}", r(4, 5), r(7, 8), r(5, 5));
        checkConstruction("{4~8}", r(4, 5), r(7, 8), r(5, 6));
        checkConstruction("{4~8}", r(4, 5), r(7, 8), r(5, 7));
        checkConstruction("{4~8}", r(4, 5), r(7, 8), r(5, 8));
        checkConstruction("{4~9}", r(4, 5), r(7, 8), r(5, 9));

        checkConstruction("{4~8}", r(4, 5), r(7, 8), r(6, 6));
        checkConstruction("{4~8}", r(4, 5), r(7, 8), r(6, 7));
        checkConstruction("{4~8}", r(4, 5), r(7, 8), r(6, 8));
        checkConstruction("{4~9}", r(4, 5), r(7, 8), r(6, 9));

        checkConstruction("{4~5,7~8}", r(4, 5), r(7, 8), r(7, 7));
        checkConstruction("{4~5,7~8}", r(4, 5), r(7, 8), r(7, 8));
        checkConstruction("{4~5,7~9}", r(4, 5), r(7, 8), r(7, 9));

        checkConstruction("{4~5,7~8}", r(4, 5), r(7, 8), r(8, 8));
        checkConstruction("{4~5,7~9}", r(4, 5), r(7, 8), r(8, 9));

        checkConstruction("{4~5,7~9}", r(4, 5), r(7, 8), r(9, 9));

        checkConstruction("{1,4,7}", r(4), r(7), r(1, 1));
        checkConstruction("{1~2,4,7}", r(4), r(7), r(1, 2));
        checkConstruction("{1~4,7}", r(4), r(7), r(1, 3));
        checkConstruction("{1~4,7}", r(4), r(7), r(1, 4));
        checkConstruction("{1~5,7}", r(4), r(7), r(1, 5));
        checkConstruction("{1~7}", r(4), r(7), r(1, 6));
        checkConstruction("{1~7}", r(4), r(7), r(1, 7));
        checkConstruction("{1~8}", r(4), r(7), r(1, 8));
        checkConstruction("{1~9}", r(4), r(7), r(1, 9));

        checkConstruction("{2,4,7}", r(4), r(7), r(2, 2));
        checkConstruction("{2~4,7}", r(4), r(7), r(2, 3));
        checkConstruction("{2~4,7}", r(4), r(7), r(2, 4));
        checkConstruction("{2~5,7}", r(4), r(7), r(2, 5));
        checkConstruction("{2~7}", r(4), r(7), r(2, 6));
        checkConstruction("{2~7}", r(4), r(7), r(2, 7));
        checkConstruction("{2~8}", r(4), r(7), r(2, 8));
        checkConstruction("{2~9}", r(4), r(7), r(2, 9));

        checkConstruction("{3~4,7}", r(4), r(7), r(3, 3));
        checkConstruction("{3~4,7}", r(4), r(7), r(3, 4));
        checkConstruction("{3~5,7}", r(4), r(7), r(3, 5));
        checkConstruction("{3~7}", r(4), r(7), r(3, 6));
        checkConstruction("{3~7}", r(4), r(7), r(3, 7));
        checkConstruction("{3~8}", r(4), r(7), r(3, 8));
        checkConstruction("{3~9}", r(4), r(7), r(3, 9));

        checkConstruction("{4,7}", r(4), r(7), r(4, 4));
        checkConstruction("{4~5,7}", r(4), r(7), r(4, 5));
        checkConstruction("{4~7}", r(4), r(7), r(4, 6));
        checkConstruction("{4~7}", r(4), r(7), r(4, 7));
        checkConstruction("{4~8}", r(4), r(7), r(4, 8));
        checkConstruction("{4~9}", r(4), r(7), r(4, 9));

        checkConstruction("{4~5,7}", r(4), r(7), r(5, 5));
        checkConstruction("{4~7}", r(4), r(7), r(5, 6));
        checkConstruction("{4~7}", r(4), r(7), r(5, 7));
        checkConstruction("{4~8}", r(4), r(7), r(5, 8));
        checkConstruction("{4~9}", r(4), r(7), r(5, 9));

        checkConstruction("{4,6~7}", r(4), r(7), r(6, 6));
        checkConstruction("{4,6~7}", r(4), r(7), r(6, 7));
        checkConstruction("{4,6~8}", r(4), r(7), r(6, 8));
        checkConstruction("{4,6~9}", r(4), r(7), r(6, 9));

        checkConstruction("{4,7}", r(4), r(7), r(7, 7));
        checkConstruction("{4,7~8}", r(4), r(7), r(7, 8));
        checkConstruction("{4,7~9}", r(4), r(7), r(7, 9));

        checkConstruction("{4,7~8}", r(4), r(7), r(8, 8));
        checkConstruction("{4,7~9}", r(4), r(7), r(8, 9));

        checkConstruction("{4,7,9}", r(4), r(7), r(9, 9));

        checkConstruction("{1~2}", r(1), r(1, 2));
        checkConstruction("{1~2}", r(2), r(1, 2));
        checkConstruction("{1~2}", r(1, 2), r(1));
        checkConstruction("{1~2}", r(1, 2), r(2));

        checkConstruction("{1,3}", r(1), r(3));
        checkConstruction("{1,3}", r(3), r(1));
    }

    @Test
    public void testContains() {
        assertFalse(s().contains(1));

        assertFalse(s(r(1, 2)).contains(0));
        assertTrue(s(r(1, 2)).contains(1));
        assertTrue(s(r(1, 2)).contains(2));
        assertFalse(s(r(1, 2)).contains(3));

        assertTrue(s().contains(r(1, 0)));
        assertTrue(s(r(1)).contains(r(1, 0)));

        assertFalse(s(r(1, 2), r(4, 5)).contains(r(0)));
        assertTrue(s(r(1, 2), r(4, 5)).contains(r(1)));
        assertTrue(s(r(1, 2), r(4, 5)).contains(r(2)));
        assertFalse(s(r(1, 2), r(4, 5)).contains(r(3)));
        assertTrue(s(r(1, 2), r(4, 5)).contains(r(4)));
        assertTrue(s(r(1, 2), r(4, 5)).contains(r(5)));
        assertFalse(s(r(1, 2), r(4, 5)).contains(r(6)));
        assertTrue(s(r(1, 2), r(4, 5)).contains(r(1, 2)));
        assertTrue(s(r(1, 2), r(4, 5)).contains(r(4, 5)));

        assertFalse(s(r(1, 2), r(4, 5)).contains(r(0, 1)));

        assertTrue(s(r(1, 2), r(4, 5)).contains((Item) r(1)));

    }

    @Test
    public void testUnion() {
        assertEquals(s(r(1)), s().union(1));
        assertEquals(s(r(1)), s(r(1)).union(1));

        assertEquals(s(r(1)), s().union(r(1)));
        assertEquals(s(r(2)), s().union(r(2)));

        assertEquals(s(r(1)), s(r(1)).union(r(1)));
        assertEquals(s(r(1, 2)), s(r(1)).union(r(2)));

        assertEquals(s(r(1)), s(r(1)).union(s()));
        assertEquals(s(r(1, 2)), s(r(1)).union(s(r(2))));

        assertEquals(s(r(1, 2)), s(r(1)).union((ItemSet) s(r(2))));
        assertEquals(s(r(1, 2)), s(r(1)).union((Item) r(2)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s(r(1)).union(BooleanValue.TRUE);
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s(r(1)).union(BooleanSet.TRUE);
                     });

    }

    @Test
    public void testIntersection() {
        assertEquals(s(), s().intersection(1));
        assertEquals(s(r(1)), s(r(1)).intersection(1));
        assertEquals(s(), s(r(2)).intersection(1));

        assertEquals(s(), s().intersection(r(1)));
        assertEquals(s(r(1)), s(r(1)).intersection(r(1)));
        assertEquals(s(), s(r(2)).intersection(r(1)));
        assertEquals(s(r(2)), s(r(2)).intersection(r(1, 2)));

        assertEquals(s(), s(r(1, 3), r(5, 7)).intersection(r(0, 0)));
        assertEquals(s(r(1)), s(r(1, 3), r(5, 7)).intersection(r(0, 1)));
        assertEquals(s(r(1, 2)), s(r(1, 3), r(5, 7)).intersection(r(0, 2)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).intersection(r(0, 3)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).intersection(r(0, 4)));
        assertEquals(s(r(1, 3), r(5)), s(r(1, 3), r(5, 7)).intersection(r(0, 5)));
        assertEquals(s(r(1, 3), r(5, 6)), s(r(1, 3), r(5, 7)).intersection(r(0, 6)));
        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(0, 7)));
        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(0, 8)));

        assertEquals(s(r(1)), s(r(1, 3), r(5, 7)).intersection(r(1, 1)));
        assertEquals(s(r(1, 2)), s(r(1, 3), r(5, 7)).intersection(r(1, 2)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).intersection(r(1, 3)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).intersection(r(1, 4)));
        assertEquals(s(r(1, 3), r(5)), s(r(1, 3), r(5, 7)).intersection(r(1, 5)));
        assertEquals(s(r(1, 3), r(5, 6)), s(r(1, 3), r(5, 7)).intersection(r(1, 6)));
        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(1, 7)));
        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(1, 8)));

        assertEquals(s(r(2)), s(r(1, 3), r(5, 7)).intersection(r(2, 2)));
        assertEquals(s(r(2, 3)), s(r(1, 3), r(5, 7)).intersection(r(2, 3)));
        assertEquals(s(r(2, 3)), s(r(1, 3), r(5, 7)).intersection(r(2, 4)));
        assertEquals(s(r(2, 3), r(5)), s(r(1, 3), r(5, 7)).intersection(r(2, 5)));
        assertEquals(s(r(2, 3), r(5, 6)), s(r(1, 3), r(5, 7)).intersection(r(2, 6)));
        assertEquals(s(r(2, 3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(2, 7)));
        assertEquals(s(r(2, 3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(2, 8)));

        assertEquals(s(r(3)), s(r(1, 3), r(5, 7)).intersection(r(3, 3)));
        assertEquals(s(r(3)), s(r(1, 3), r(5, 7)).intersection(r(3, 4)));
        assertEquals(s(r(3), r(5)), s(r(1, 3), r(5, 7)).intersection(r(3, 5)));
        assertEquals(s(r(3), r(5, 6)), s(r(1, 3), r(5, 7)).intersection(r(3, 6)));
        assertEquals(s(r(3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(3, 7)));
        assertEquals(s(r(3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(3, 8)));

        assertEquals(s(), s(r(1, 3), r(5, 7)).intersection(r(4, 4)));
        assertEquals(s(r(5)), s(r(1, 3), r(5, 7)).intersection(r(4, 5)));
        assertEquals(s(r(5, 6)), s(r(1, 3), r(5, 7)).intersection(r(4, 6)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(4, 7)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(4, 8)));

        assertEquals(s(r(5)), s(r(1, 3), r(5, 7)).intersection(r(5, 5)));
        assertEquals(s(r(5, 6)), s(r(1, 3), r(5, 7)).intersection(r(5, 6)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(5, 7)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(5, 8)));

        assertEquals(s(r(6)), s(r(1, 3), r(5, 7)).intersection(r(6, 6)));
        assertEquals(s(r(6, 7)), s(r(1, 3), r(5, 7)).intersection(r(6, 7)));
        assertEquals(s(r(6, 7)), s(r(1, 3), r(5, 7)).intersection(r(6, 8)));

        assertEquals(s(r(7)), s(r(1, 3), r(5, 7)).intersection(r(7, 7)));
        assertEquals(s(r(7)), s(r(1, 3), r(5, 7)).intersection(r(7, 8)));

        assertEquals(s(), s(r(1, 3), r(5, 7)).intersection(r(8, 8)));

        assertEquals(s(r(3), r(5, 7), r(9)), s(r(1, 3), r(5, 7), r(9, 11)).intersection(r(3, 9)));

        assertEquals(s(r(3), r(5, 7), r(9)), s(r(1, 3), r(5, 7), r(9, 11)).intersection(s(r(3, 9))));

        assertEquals(s(), s().intersection(s()));
        assertEquals(s(), s(r(1)).intersection(s()));

        assertEquals(s(r(1)), s(r(1)).intersection((Item) r(1)));
        assertEquals(s(r(1)), s(r(1)).intersection((ItemSet) s(r(1))));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         assertEquals(s(r(1)), s(r(1)).intersection(BooleanSet.TRUE));
                     });
    }

    @Test
    public void testRemove() {
        assertEquals(s(), s().remove(1));
        assertEquals(s(), s(r(1)).remove(1));
        assertEquals(s(r(2, 3)), s(r(1, 3)).remove(1));
        assertEquals(s(r(1), r(3)), s(r(1, 3)).remove(2));

        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(0));
        assertEquals(s(r(2, 3)), s(r(1, 3)).remove(1));
        assertEquals(s(r(1), r(3)), s(r(1, 3)).remove(2));
        assertEquals(s(r(1, 2)), s(r(1, 3)).remove(3));
        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(4));
        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(5));

        assertEquals(s(), s().remove(r(1)));
        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(r(1, 0)));
        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(r(0)));
        assertEquals(s(r(2, 3)), s(r(1, 3)).remove(r(1)));
        assertEquals(s(r(1), r(3)), s(r(1, 3)).remove(r(2)));
        assertEquals(s(r(1, 2)), s(r(1, 3)).remove(r(3)));
        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(r(4)));
        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(r(5)));

        assertEquals(s(r(2, 3)), s(r(1, 3)).remove(r(0, 1)));
        assertEquals(s(r(3)), s(r(1, 3)).remove(r(0, 2)));
        assertEquals(s(), s(r(1, 3)).remove(r(0, 3)));
        assertEquals(s(), s(r(1, 3)).remove(r(0, 4)));

        assertEquals(s(r(2, 3)), s(r(1, 3)).remove(r(1, 1)));
        assertEquals(s(r(3)), s(r(1, 3)).remove(r(1, 2)));
        assertEquals(s(), s(r(1, 3)).remove(r(1, 3)));
        assertEquals(s(), s(r(1, 3)).remove(r(1, 4)));

        assertEquals(s(r(1), r(3)), s(r(1, 3)).remove(r(2, 2)));
        assertEquals(s(r(1)), s(r(1, 3)).remove(r(2, 3)));
        assertEquals(s(r(1)), s(r(1, 3)).remove(r(2, 4)));

        assertEquals(s(r(1, 2)), s(r(1, 3)).remove(r(3, 3)));
        assertEquals(s(r(1, 2)), s(r(1, 3)).remove(r(3, 4)));

        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(0, 0)));
        assertEquals(s(r(2, 3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(0, 1)));
        assertEquals(s(r(3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(0, 2)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(0, 3)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(0, 4)));
        assertEquals(s(r(6, 7)), s(r(1, 3), r(5, 7)).remove(r(0, 5)));
        assertEquals(s(r(7)), s(r(1, 3), r(5, 7)).remove(r(0, 6)));
        assertEquals(s(), s(r(1, 3), r(5, 7)).remove(r(0, 7)));
        assertEquals(s(), s(r(1, 3), r(5, 7)).remove(r(0, 8)));

        assertEquals(s(r(2, 3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(1, 1)));
        assertEquals(s(r(3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(1, 2)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(1, 3)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(1, 4)));
        assertEquals(s(r(6, 7)), s(r(1, 3), r(5, 7)).remove(r(1, 5)));
        assertEquals(s(r(7)), s(r(1, 3), r(5, 7)).remove(r(1, 6)));
        assertEquals(s(), s(r(1, 3), r(5, 7)).remove(r(1, 7)));
        assertEquals(s(), s(r(1, 3), r(5, 7)).remove(r(1, 8)));

        assertEquals(s(r(1), r(3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(2, 2)));
        assertEquals(s(r(1), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(2, 3)));
        assertEquals(s(r(1), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(2, 4)));
        assertEquals(s(r(1), r(6, 7)), s(r(1, 3), r(5, 7)).remove(r(2, 5)));
        assertEquals(s(r(1), r(7)), s(r(1, 3), r(5, 7)).remove(r(2, 6)));
        assertEquals(s(r(1)), s(r(1, 3), r(5, 7)).remove(r(2, 7)));
        assertEquals(s(r(1)), s(r(1, 3), r(5, 7)).remove(r(2, 8)));

        assertEquals(s(r(1, 2), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(3, 3)));
        assertEquals(s(r(1, 2), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(3, 4)));
        assertEquals(s(r(1, 2), r(6, 7)), s(r(1, 3), r(5, 7)).remove(r(3, 5)));
        assertEquals(s(r(1, 2), r(7)), s(r(1, 3), r(5, 7)).remove(r(3, 6)));
        assertEquals(s(r(1, 2)), s(r(1, 3), r(5, 7)).remove(r(3, 7)));
        assertEquals(s(r(1, 2)), s(r(1, 3), r(5, 7)).remove(r(3, 8)));

        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(4, 4)));
        assertEquals(s(r(1, 3), r(6, 7)), s(r(1, 3), r(5, 7)).remove(r(4, 5)));
        assertEquals(s(r(1, 3), r(7)), s(r(1, 3), r(5, 7)).remove(r(4, 6)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).remove(r(4, 7)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).remove(r(4, 8)));

        assertEquals(s(r(1, 3), r(6, 7)), s(r(1, 3), r(5, 7)).remove(r(5, 5)));
        assertEquals(s(r(1, 3), r(7)), s(r(1, 3), r(5, 7)).remove(r(5, 6)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).remove(r(5, 7)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).remove(r(5, 8)));

        assertEquals(s(r(1, 3), r(5), r(7)), s(r(1, 3), r(5, 7)).remove(r(6, 6)));
        assertEquals(s(r(1, 3), r(5)), s(r(1, 3), r(5, 7)).remove(r(6, 7)));
        assertEquals(s(r(1, 3), r(5)), s(r(1, 3), r(5, 7)).remove(r(6, 8)));

        assertEquals(s(r(1, 3), r(5, 6)), s(r(1, 3), r(5, 7)).remove(r(7, 7)));
        assertEquals(s(r(1, 3), r(5, 6)), s(r(1, 3), r(5, 7)).remove(r(7, 8)));

        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(8, 8)));

        assertEquals(s(r(1, 2), r(10, 11)), s(r(1, 3), r(5, 7), r(9, 11)).remove(r(3, 9)));

        assertEquals(s(), s().remove(s(r(1))));
        assertEquals(s(r(1)), s(r(1)).remove(s()));

        assertEquals(s(r(1, 2), r(10, 11)), s(r(1, 3), r(5, 7), r(9, 11)).remove(s(r(3, 9))));
        assertEquals(s(r(1, 3), r(5, 7), r(9, 11)), s(r(1, 3), r(5, 7), r(9, 11)).remove(s()));

        assertEquals(s(r(2, 3)), s(r(1, 3)).remove((Item) r(1)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s(r(1, 3)).remove(BooleanSet.TRUE);
                     });

    }

    @Test
    public void testConvert() {
        assertEquals(IntegerSet.EMPTY, IntegerSet.convert(BooleanSet.EMPTY));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         IntegerSet.convert(BooleanSet.TRUE);
                     });
    }

    @Test
    public void testEquals() {
        assertEquals(s(r(1), r(2)), s(r(2), r(1)));
        assertNotEquals(s(r(1), r(2)), null);
        assertNotEquals(s(r(1), r(2)), 1);
        assertNotEquals(s(), 1);
        assertEquals(s(), s());
        assertEquals(s(), s(r(1, 0)));
        assertNotEquals(s(), s(r(0)));
    }

    @Test
    public void testHashCode() {
        assertEquals(s().hashCode(), s().hashCode());
        assertEquals(s().hashCode(), s(r(1, 0)).hashCode());
        assertEquals(s(r(1)).hashCode(), s(r(1)).hashCode());
    }
}