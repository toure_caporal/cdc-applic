package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.AbstractItemSet;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.Item;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.UncheckedSet;

public class StringSetTest {
    private static final Logger LOGGER = LogManager.getLogger(StringSetTest.class);

    private static StringValue v(String text) {
        return StringValue.create(text, false);
    }

    private static StringSet s(String content) {
        return StringSet.create(content);
    }

    @SafeVarargs
    private static StringSet s(StringValue... values) {
        return StringSet.create(values);
    }

    @Test
    public void checkEmpty() {
        assertTrue(StringSet.EMPTY.isEmpty());
        assertEquals(StringSet.EMPTY, IntegerSet.EMPTY);
        assertEquals(StringSet.EMPTY, UncheckedSet.EMPTY);
        assertEquals(StringSet.EMPTY, RealSet.EMPTY);
        assertEquals(StringSet.EMPTY, BooleanSet.EMPTY);
        assertEquals(StringSet.EMPTY, StringSet.EMPTY);
    }

    private static void checkEquals(boolean expected,
                                    String s1,
                                    String s2) {
        final StringSet set1 = s(s1);
        final StringSet set2 = s(s2);
        LOGGER.debug(set1 + " equals " + set2 + ": " + set1.equals(set2));
        if (expected) {
            assertEquals(set1, set2);
        } else {
            assertNotEquals(set1, set2);
        }
    }

    @Test
    public void checkEquals() {
        checkEquals(true, "A, B", "A,   B");
        checkEquals(true, "A, B", "A,B");
    }

    private static void checkConstruction(String expectedContent,
                                          String s) {
        final StringSet set = s(s);
        LOGGER.debug("String(" + s + "): " + set);
        assertEquals(expectedContent, set.getContent());

        assertEquals(set, set.getChecked());
        assertTrue(set.isValid());
        assertTrue(set.isChecked());

        final StringSet set2 = StringSet.create(set.getItems());
        assertEquals(expectedContent, set2.getContent());

        final StringSet set3 = StringSet.create(set.getItems().toArray(new StringValue[0]));
        assertEquals(expectedContent, set3.getContent());

    }

    @Test
    public void testConstruction() {
        checkConstruction("", "");
        checkConstruction("A", "A");
        checkConstruction("\"1\"", "\"1\"");
        checkConstruction("A", " A ");
        checkConstruction("A,B", " A , B");
        checkConstruction("A,B", " A,A , B, A");
        checkConstruction("\"A A\",A", "  \"A A\"  , A  ");
        checkConstruction("\"A \"\"A\",A", "  \"A \"\"A\"  , A  ");
        checkConstruction("\"A \",A", "  \"A \",\"A\"  , A  ");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         StringSet.create((StringValue) null);
                     });

        assertEquals(s("A,B"), s(v("A"), v("B"), v("A")));
    }

    @Test
    public void testConvert() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         StringSet.convert(BooleanSet.TRUE);
                     });
    }

    @Test
    public void testContains() {
        assertTrue(s("A").contains((Item) v("A")));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s("A").contains(BooleanValue.TRUE);
                     });
    }

    private static void checkUnion(StringSet set1,
                                   AbstractItemSet set2,
                                   String content) {
        final StringSet set = set1.union(set2);
        LOGGER.debug(set1 + " enumerated union " + set2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    private static void checkUnion(String s1,
                                   String s2,
                                   String content) {
        final StringSet set1 = s(s1);
        checkUnion(set1, s(s2), content);
        checkUnion(set1, UncheckedSet.create(s2), content);

    }

    @Test
    public void testUnion() {
        checkUnion("", "", "");
        checkUnion("A", "", "A");
        checkUnion("", "A", "A");
        checkUnion("A", "A", "A");
        checkUnion("A", "A", "A");
        checkUnion("A", "B", "A,B");
        checkUnion("A,B", "B", "A,B");
        checkUnion("A,B", "A,B", "A,B");
        checkUnion("A,B", "B,A", "A,B");
        checkUnion("A,B", "A,C", "A,B,C");

        assertEquals(s("A,B"), s("A").union((Item) v("B")));
        assertEquals(s("A,B"), s("A,B").union(v("B")));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s("A").union(BooleanValue.TRUE);
                     });
    }

    private static void checkIntersection(StringSet set1,
                                          AbstractItemSet set2,
                                          String content) {
        final StringSet set = set1.intersection(set2);
        LOGGER.debug(set1 + " string intersection " + set2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    private static void checkIntersection(String s1,
                                          String s2,
                                          String content) {
        final StringSet set1 = s(s1);
        checkIntersection(set1, s(s2), content);
        checkIntersection(set1, UncheckedSet.create(s2), content);
    }

    @Test
    public void testIntersection() {
        checkIntersection("", "", "");
        checkIntersection("A", "", "");
        checkIntersection("A,B", "", "");
        checkIntersection("", "A", "");
        checkIntersection("", "A,B", "");
        checkIntersection("A", "A", "A");
        checkIntersection("A,B", "A", "A");
        checkIntersection("B,A", "A", "A");
        checkIntersection("B,A", "A,B", "B,A");
        checkIntersection("A", "B", "");

        assertEquals(s("A"), s("A,B").intersection((Item) v("A")));
        assertEquals(s(), s("A,B").intersection(v("C")));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s("A").intersection(BooleanValue.TRUE);
                     });
    }

    private static void checkRemove(StringSet set1,
                                    AbstractItemSet set2,
                                    String content) {
        final StringSet set = set1.remove(set2);
        LOGGER.debug(set1 + " string remove " + set2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    private static void checkRemove(String s1,
                                    String s2,
                                    String content) {
        final StringSet set1 = StringSet.create(s1);
        checkRemove(set1, StringSet.create(s2), content);
        checkRemove(set1, UncheckedSet.create(s2), content);
    }

    @Test
    public void testRemove() {
        checkRemove("", "", "");
        checkRemove("A", "", "A");
        checkRemove("A,B", "", "A,B");
        checkRemove("A,B,C", "", "A,B,C");
        checkRemove("", "A", "");
        checkRemove("A", "A", "");
        checkRemove("A,B", "A", "B");
        checkRemove("A,B,C", "A", "B,C");
        checkRemove("", "B", "");
        checkRemove("A", "B", "A");
        checkRemove("A,B", "B", "A");
        checkRemove("A,B,C", "B", "A,C");
        checkRemove("", "C", "");
        checkRemove("A", "C", "A");
        checkRemove("A,B", "C", "A,B");
        checkRemove("A,B,C", "C", "A,B");

        assertEquals(s("A"), s("A,B").remove((Item) v("B")));
        assertEquals(s("A"), s("A").remove(v("B")));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s("A").remove(BooleanValue.TRUE);
                     });
    }

    @Test
    public void testEquals() {
        assertEquals(StringSet.EMPTY, IntegerSet.EMPTY);
        assertNotEquals(StringSet.EMPTY, BooleanSet.TRUE);
        assertNotEquals(StringSet.EMPTY, null);
    }

    @Test
    public void testHashCode() {
        assertEquals(StringSet.EMPTY.hashCode(), StringSet.EMPTY.hashCode());
    }

}