package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.AbstractItemSet;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.Item;
import cdc.applic.expressions.content.ItemSet;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.UncheckedSet;

public class BooleanSetTest {
    @Test
    public void checkEmpty() {
        assertTrue(BooleanSet.EMPTY.isEmpty());
        assertEquals(BooleanSet.EMPTY, IntegerSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, UncheckedSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, RealSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, BooleanSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, StringSet.EMPTY);
    }

    private static void checkConstruction(String expectedContent,
                                          String s) {
        final BooleanSet set = BooleanSet.create(s);
        assertEquals(expectedContent, set.getContent());
        assertTrue(set.isValid());
        assertTrue(set.isChecked());
        assertEquals(set, set.getChecked());
    }

    @Test
    public void testConstruction() {
        checkConstruction("", "");
        checkConstruction("true", "true");
        checkConstruction("true", " true ");
        checkConstruction("false", " FALSE  ");
        checkConstruction("false,true", " true, false, TRUE, FALSE  ");

        assertTrue(BooleanSet.TRUE.contains(BooleanValue.TRUE));
        assertTrue(BooleanSet.TRUE.contains((Item) BooleanValue.TRUE));
        assertFalse(BooleanSet.TRUE.contains(IntegerValue.create(0)));
    }

    @Test
    public void testConvert() {
        assertEquals(BooleanSet.EMPTY, BooleanSet.convert(IntegerSet.EMPTY));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.convert(null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.convert(IntegerSet.create("0"));
                     });
    }

    private static void checkUnion(BooleanSet set1,
                                   AbstractItemSet set2,
                                   String expectedContent) {
        final BooleanSet set = set1.union(set2);
        assertEquals(expectedContent, set.getContent());

        BooleanSet s = set1;
        for (final Item item : set2.getItems()) {
            s = s.union(item);
        }
        assertEquals(expectedContent, s.getContent());
    }

    private static void checkUnion(String s1,
                                   String s2,
                                   String expectedContent) {
        checkUnion(BooleanSet.create(s1), BooleanSet.create(s2), expectedContent);
        checkUnion(BooleanSet.create(s1), UncheckedSet.create(s2), expectedContent);
    }

    @Test
    public void testUnion() {
        checkUnion("", "", "");
        checkUnion("true", "", "true");
        checkUnion("", "TRUE", "true");
        checkUnion("true", "TRUE", "true");
        checkUnion("true", "false", "false,true");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.union((Item) null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.union(IntegerValue.create(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.union((ItemSet) null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.union(IntegerSet.create("0"));
                     });
    }

    private static void checkIntersection(BooleanSet set1,
                                          AbstractItemSet set2,
                                          String expectedContent) {
        final BooleanSet set = set1.intersection(set2);
        assertEquals(expectedContent, set.getContent());
    }

    private static void checkIntersection(String s1,
                                          String s2,
                                          String expectedContent) {
        checkIntersection(BooleanSet.create(s1), BooleanSet.create(s2), expectedContent);
        checkIntersection(BooleanSet.create(s1), UncheckedSet.create(s2), expectedContent);
    }

    @Test
    public void testIntersection() {
        checkIntersection("", "", "");
        checkIntersection("true", "", "");
        checkIntersection("true,false", "", "");
        checkIntersection("", "true", "");
        checkIntersection("", "true,false", "");
        checkIntersection("TRUE", "true", "true");
        checkIntersection("TRUE,FALSE", "true", "true");
        checkIntersection("FALSE,TRUE", "TRUE", "true");
        checkIntersection("TRUE,false", "FALSE,true", "false,true");
        checkIntersection("TRUE", "FALSE", "");

        assertEquals(BooleanSet.TRUE, BooleanSet.FALSE_TRUE.intersection(BooleanSet.TRUE));
        assertEquals(BooleanSet.TRUE, BooleanSet.FALSE_TRUE.intersection(BooleanValue.TRUE));
        assertEquals(BooleanSet.EMPTY, BooleanSet.FALSE.intersection(BooleanValue.TRUE));
        assertEquals(BooleanSet.EMPTY, BooleanSet.FALSE.intersection((Item) BooleanValue.TRUE));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.intersection((Item) null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.intersection((ItemSet) null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.intersection(IntegerValue.create(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.intersection(IntegerSet.create("0"));
                     });
    }

    private static void checkRemove(BooleanSet set1,
                                    AbstractItemSet set2,
                                    String expectedContent) {
        final BooleanSet set = set1.remove(set2);
        assertEquals(expectedContent, set.getContent());

        BooleanSet s = set1;
        for (final Item item : set2.getItems()) {
            s = s.remove(item);
        }
        assertEquals(expectedContent, s.getContent());
    }

    private static void checkRemove(String s1,
                                    String s2,
                                    String expectedContent) {
        checkRemove(BooleanSet.create(s1), BooleanSet.create(s2), expectedContent);
        checkRemove(BooleanSet.create(s1), UncheckedSet.create(s2), expectedContent);
    }

    @Test
    public void testRemove() {
        checkRemove("", "", "");
        checkRemove("TRUE", "", "true");
        checkRemove("", "TRUE", "");
        checkRemove("", "FALSE", "");
        checkRemove("TRUE,FALSE", "", "false,true");
        checkRemove("TRUE", "TRUE", "");
        checkRemove("TRUE,FALSE", "TRUE", "false");
        checkRemove("TRUE,FALSE", "TRUE", "false");
        checkRemove("TRUE", "FALSE", "true");
        checkRemove("TRUE,FALSE", "FALSE", "true");
        checkRemove("TRUE", "", "true");
        checkRemove("TRUE,FALSE", "", "false,true");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.remove((Item) null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.remove(IntegerValue.create(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.remove((ItemSet) null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.remove(IntegerSet.create("0"));
                     });
    }

    @Test
    public void testEquals() {
        assertEquals(BooleanSet.FALSE, BooleanSet.FALSE);
        assertEquals(BooleanSet.EMPTY, IntegerSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, RealSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, StringSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, UncheckedSet.EMPTY);
        assertNotEquals(BooleanSet.EMPTY, IntegerSet.create(IntegerValue.create(1)));
        assertNotEquals(BooleanSet.EMPTY, null);
        assertNotEquals(BooleanSet.EMPTY, "true");
        assertNotEquals(BooleanSet.FALSE, BooleanSet.TRUE);
        assertNotEquals(BooleanSet.FALSE, null);
        assertNotEquals(BooleanSet.FALSE, "false");

        assertEquals(BooleanSet.EMPTY, BooleanSet.create());
        assertEquals(BooleanSet.FALSE, BooleanSet.create(BooleanValue.FALSE));
        assertEquals(BooleanSet.TRUE, BooleanSet.create(BooleanValue.TRUE));
        assertEquals(BooleanSet.FALSE_TRUE, BooleanSet.create(BooleanValue.FALSE, BooleanValue.TRUE));
        assertEquals(BooleanSet.FALSE_TRUE, BooleanSet.create(BooleanValue.TRUE, BooleanValue.FALSE));
    }

    @Test
    public void testIsFull() {
        assertTrue(BooleanSet.FALSE_TRUE.isFull());
        assertFalse(BooleanSet.FALSE.isFull());
        assertFalse(BooleanSet.TRUE.isFull());
        assertFalse(BooleanSet.EMPTY.isFull());
    }

    @Test
    public void testToString() {
        assertEquals("{}", BooleanSet.EMPTY.toString());
        assertEquals("{true}", BooleanSet.TRUE.toString());
        assertEquals("{false}", BooleanSet.FALSE.toString());
        assertEquals("{false,true}", BooleanSet.FALSE_TRUE.toString());
    }

    @Test
    public void testHashCode() {
        assertNotEquals(BooleanSet.TRUE.hashCode(), BooleanSet.FALSE.hashCode());
        assertNotEquals(BooleanSet.EMPTY.hashCode(), BooleanSet.FALSE.hashCode());
    }
}