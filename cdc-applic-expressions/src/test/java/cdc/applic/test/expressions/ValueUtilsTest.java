package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.ItemKind;
import cdc.applic.expressions.content.ValueUtils;

public class ValueUtilsTest {

    @Test
    public void test() {
        assertEquals(ItemKind.INTEGER, ValueUtils.create("1").getKind());
        assertEquals(ItemKind.REAL, ValueUtils.create("1.0").getKind());
        assertEquals(ItemKind.BOOLEAN, ValueUtils.create("true").getKind());
        assertEquals(ItemKind.STRING, ValueUtils.create("A").getKind());
        assertEquals(ItemKind.STRING, ValueUtils.create("\"A\"").getKind());
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         ValueUtils.create("~");
                     });
    }
}