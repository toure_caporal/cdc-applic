package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.ItemSetKind;
import cdc.applic.expressions.content.RealItem;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.UncheckedSet;
import cdc.util.lang.CollectionUtils;

public class RealSetTest {
    // private static final Logger LOGGER = LogManager.getLogger(RangeSetTest.class);

    private static RealValue v(double value) {
        return RealValue.create(value);
    }

    private static RealRange r(double value) {
        return RealRange.create(value);
    }

    private static RealRange r(double min,
                               double max) {
        return RealRange.create(min, max);
    }

    @SafeVarargs
    private static RealSet s(RealRange... ranges) {
        return RealSet.create(ranges);
    }

    @SafeVarargs
    private static void checkConstruction(String expected,
                                          RealItem... items) {
        // LOGGER.info(Arrays.toString(ranges) + " " + expected);
        final RealSet set1 = RealSet.create(items);
        final RealSet set2 = RealSet.create(CollectionUtils.toList(items));
        // LOGGER.info(Arrays.toString(ranges) + " " + set1);
        assertEquals(expected, set1.toString());
        assertEquals(set1, set1);
        assertEquals(set1, set2);
        assertEquals(ItemSetKind.REAL, set1.getKind());
    }

    @Test
    public void testConstruction() {
        assertTrue(RealSet.EMPTY.isEmpty());

        checkConstruction("{}");
        checkConstruction("{1.0}", v(1));
        checkConstruction("{1.0}", r(1));
        checkConstruction("{}", r(1.0, -1));
        checkConstruction("{1.0~10.0}", r(1.0, 10));
        checkConstruction("{1.0~10.0,20.0}", r(1.0, 10), r(20));
        checkConstruction("{1.0~10.0,12.0}", r(1.0, 10), r(12));
        checkConstruction("{1.0~10.0,11.0}", r(1.0, 10), r(11));
        checkConstruction("{1.0~10.0}", r(1.0, 10), r(10));
        checkConstruction("{1.0~20.0}", r(1.0, 10), r(9, 20));
        checkConstruction("{1.0~10.0}", r(1.0, 10), r(20, 10));
        checkConstruction("{}", r(11.0, 10), r(20, 10));
        checkConstruction("{1.0,2.0}", r(1), r(2));

        checkConstruction("{1.0}", r(1));

        checkConstruction("{1.0,2.0}", r(1), r(2));
        checkConstruction("{1.0,2.0}", r(2), r(1));

        checkConstruction("{-1.0,1.0~2.0}", r(-1), r(1.0, 2));
        checkConstruction("{-1.0,1.0~2.0}", r(1.0, 2), r(-1));
        checkConstruction("{0.0,1.0~2.0}", r(0), r(1.0, 2));
        checkConstruction("{0.0,1.0~2.0}", r(1.0, 2), r(0));
        checkConstruction("{1.0~2.0}", r(1), r(1.0, 2));
        checkConstruction("{1.0~2.0}", r(1.0, 2), r(1));
        checkConstruction("{1.0~2.0}", r(2), r(1.0, 2));
        checkConstruction("{1.0~2.0}", r(1.0, 2), r(2));
        checkConstruction("{1.0~2.0,3.0}", r(3), r(1.0, 2));
        checkConstruction("{1.0~2.0,3.0}", r(1.0, 2), r(3));
        checkConstruction("{1.0~2.0,4.0}", r(4), r(1.0, 2));
        checkConstruction("{1.0~2.0,4.0}", r(1.0, 2), r(4));

        checkConstruction("{0.0~3.0}", r(1.0, 2), r(0, 3));

        // checkConstruction("{1.0~2.0,4.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(1.0, 2));
        // checkConstruction("{1.0~3.0,4.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(1.0, 3));
        // checkConstruction("{1.0~5.0,7.0~8}", r(4.0, 5), r(7, 8), r(1.0, 4));
        // checkConstruction("{1.0~5.0,7.0~8}", r(4.0, 5), r(7, 8), r(1.0, 5));
        // checkConstruction("{1.0~8.0}", r(4.0, 5), r(7, 8), r(1.0, 6));
        // checkConstruction("{1.0~8.0}", r(4.0, 5), r(7, 8), r(1.0, 7));
        // checkConstruction("{1.0~8.0}", r(4.0, 5), r(7, 8), r(1.0, 8));
        // checkConstruction("{1.0~9.0}", r(4.0, 5), r(7, 8), r(1.0, 9));
        //
        // checkConstruction("{2,4.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(2, 2));
        // checkConstruction("{2.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(2, 3));
        // checkConstruction("{2.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(2, 4));
        // checkConstruction("{2.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(2, 5));
        // checkConstruction("{2.0~8.0}", r(4.0, 5), r(7, 8), r(2, 6));
        // checkConstruction("{2.0~8.0}", r(4.0, 5), r(7, 8), r(2, 7));
        // checkConstruction("{2.0~8.0}", r(4.0, 5), r(7, 8), r(2, 8));
        // checkConstruction("{2.0~9.0}", r(4.0, 5), r(7, 8), r(2, 9));
        //
        // checkConstruction("{3.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(3, 3));
        // checkConstruction("{3.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(3, 4));
        // checkConstruction("{3.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(3, 5));
        // checkConstruction("{3.0~8.0}", r(4.0, 5), r(7, 8), r(3, 6));
        // checkConstruction("{3.0~8.0}", r(4.0, 5), r(7, 8), r(3, 7));
        // checkConstruction("{3.0~8.0}", r(4.0, 5), r(7, 8), r(3, 8));
        // checkConstruction("{3.0~9.0}", r(4.0, 5), r(7, 8), r(3, 9));
        //
        // checkConstruction("{4.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(4.0, 4));
        // checkConstruction("{4.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(4.0, 5));
        // checkConstruction("{4.0~8.0}", r(4.0, 5), r(7, 8), r(4.0, 6));
        // checkConstruction("{4.0~8.0}", r(4.0, 5), r(7, 8), r(4.0, 7));
        // checkConstruction("{4.0~8.0}", r(4.0, 5), r(7, 8), r(4.0, 8));
        // checkConstruction("{4.0~9.0}", r(4.0, 5), r(7, 8), r(4.0, 9));
        //
        // checkConstruction("{4.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(5.0, 5));
        // checkConstruction("{4.0~8.0}", r(4.0, 5), r(7, 8), r(5.0, 6));
        // checkConstruction("{4.0~8.0}", r(4.0, 5), r(7, 8), r(5.0, 7));
        // checkConstruction("{4.0~8.0}", r(4.0, 5), r(7, 8), r(5.0, 8));
        // checkConstruction("{4.0~9.0}", r(4.0, 5), r(7, 8), r(5.0, 9));
        //
        // checkConstruction("{4.0~8.0}", r(4.0, 5), r(7, 8), r(6, 6));
        // checkConstruction("{4.0~8.0}", r(4.0, 5), r(7, 8), r(6, 7));
        // checkConstruction("{4.0~8.0}", r(4.0, 5), r(7, 8), r(6, 8));
        // checkConstruction("{4.0~9.0}", r(4.0, 5), r(7, 8), r(6, 9));
        //
        // checkConstruction("{4.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(7, 7));
        // checkConstruction("{4.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(7, 8));
        // checkConstruction("{4.0~5.0,7.0~9.0}", r(4.0, 5), r(7, 8), r(7, 9));
        //
        // checkConstruction("{4.0~5.0,7.0~8.0}", r(4.0, 5), r(7, 8), r(8, 8));
        // checkConstruction("{4.0~5.0,7.0~9.0}", r(4.0, 5), r(7, 8), r(8, 9));
        //
        // checkConstruction("{4.0~5.0,7.0~9.0}", r(4.0, 5), r(7, 8), r(9, 9));
        //
        // checkConstruction("{1.0,4.0,7.0}", r(4), r(7), r(1.0, 1));
        // checkConstruction("{1.0~2,4.0,7.0}", r(4), r(7), r(1.0, 2));
        // checkConstruction("{1.0~4.0,7.0}", r(4), r(7), r(1.0, 3));
        // checkConstruction("{1.0~4.0,7.0}", r(4), r(7), r(1.0, 4));
        // checkConstruction("{1.0~5.0,7.0}", r(4), r(7), r(1.0, 5));
        // checkConstruction("{1.0~7.0}", r(4), r(7), r(1.0, 6));
        // checkConstruction("{1.0~7.0}", r(4), r(7), r(1.0, 7));
        // checkConstruction("{1.0~8.0}", r(4), r(7), r(1.0, 8));
        // checkConstruction("{1.0~9.0}", r(4), r(7), r(1.0, 9));
        //
        // checkConstruction("{2.0,4.0,7.0}", r(4), r(7), r(2, 2));
        // checkConstruction("{2.0~4.0,7.0}", r(4), r(7), r(2, 3));
        // checkConstruction("{2.0~4.0,7.0}", r(4), r(7), r(2, 4));
        // checkConstruction("{2.0~5.0,7.0}", r(4), r(7), r(2, 5));
        // checkConstruction("{2.0~7.0}", r(4), r(7), r(2, 6));
        // checkConstruction("{2.0~7.0}", r(4), r(7), r(2, 7));
        // checkConstruction("{2.0~8.0}", r(4), r(7), r(2, 8));
        // checkConstruction("{2.0~9.0}", r(4), r(7), r(2, 9));
        //
        // checkConstruction("{3.0~4.0,7.0}", r(4), r(7), r(3, 3));
        // checkConstruction("{3.0~4.0,7.0}", r(4), r(7), r(3, 4));
        // checkConstruction("{3.0~5.0,7.0}", r(4), r(7), r(3, 5));
        // checkConstruction("{3.0~7.0}", r(4), r(7), r(3, 6));
        // checkConstruction("{3.0~7.0}", r(4), r(7), r(3, 7));
        // checkConstruction("{3.0~8.0}", r(4), r(7), r(3, 8));
        // checkConstruction("{3.0~9.0}", r(4), r(7), r(3, 9));
        //
        // checkConstruction("{4.0,7.0}", r(4), r(7), r(4.0, 4));
        // checkConstruction("{4.0~5.0,7.0}", r(4), r(7), r(4.0, 5));
        // checkConstruction("{4.0~7.0}", r(4), r(7), r(4.0, 6));
        // checkConstruction("{4.0~7.0}", r(4), r(7), r(4.0, 7));
        // checkConstruction("{4.0~8.0}", r(4), r(7), r(4.0, 8));
        // checkConstruction("{4.0~9.0}", r(4), r(7), r(4.0, 9));
        //
        // checkConstruction("{4.0~5.0,7.0}", r(4), r(7), r(5.0, 5));
        // checkConstruction("{4.0~7.0}", r(4), r(7), r(5.0, 6));
        // checkConstruction("{4.0~7.0}", r(4), r(7), r(5.0, 7));
        // checkConstruction("{4.0~8.0}", r(4), r(7), r(5.0, 8));
        // checkConstruction("{4.0~9.0}", r(4), r(7), r(5.0, 9));
        //
        // checkConstruction("{4.0,6~7.0}", r(4), r(7), r(6, 6));
        // checkConstruction("{4.0,6~7.0}", r(4), r(7), r(6, 7));
        // checkConstruction("{4.0,6~8.0}", r(4), r(7), r(6, 8));
        // checkConstruction("{4.0,6~9.0}", r(4), r(7), r(6, 9));
        //
        // checkConstruction("{4.0,7.0}", r(4), r(7), r(7, 7));
        // checkConstruction("{4.0,7.0~8.0}", r(4), r(7), r(7, 8));
        // checkConstruction("{4.0,7.0~9.0}", r(4), r(7), r(7, 9));
        //
        // checkConstruction("{4.0,7.0~8.0}", r(4), r(7), r(8, 8));
        // checkConstruction("{4.0,7.0~9.0}", r(4), r(7), r(8, 9));
        //
        // checkConstruction("{4.0,7,9.0}", r(4), r(7), r(9, 9));
        //
        // checkConstruction("{1.0~2.0}", r(1), r(1.0, 2));
        // checkConstruction("{1.0~2.0}", r(2), r(1.0, 2));
        // checkConstruction("{1.0~2.0}", r(1.0, 2), r(1));
        // checkConstruction("{1.0~2.0}", r(1.0, 2), r(2));
        //
        // checkConstruction("{1.0,3.0}", r(1), r(3));
        // checkConstruction("{1.0,3.0}", r(3), r(1));
    }

    @Test
    public void testContains() {
        assertFalse(s().contains(1));

        assertFalse(s(r(1.0, 2)).contains(0));
        assertTrue(s(r(1.0, 2)).contains(1));
        assertTrue(s(r(1.0, 2)).contains(2));
        assertFalse(s(r(1.0, 2)).contains(3));

        assertTrue(s().contains(r(1.0, 0)));
        assertTrue(s(r(1)).contains(r(1.0, 0)));

        assertFalse(s(r(1.0, 2), r(4.0, 5)).contains(r(0)));
        assertTrue(s(r(1.0, 2), r(4.0, 5)).contains(r(1)));
        assertTrue(s(r(1.0, 2), r(4.0, 5)).contains(r(2)));
        assertFalse(s(r(1.0, 2), r(4.0, 5)).contains(r(3)));
        assertTrue(s(r(1.0, 2), r(4.0, 5)).contains(r(4)));
        assertTrue(s(r(1.0, 2), r(4.0, 5)).contains(r(5)));
        assertFalse(s(r(1.0, 2), r(4.0, 5)).contains(r(6)));
        assertTrue(s(r(1.0, 2), r(4.0, 5)).contains(r(1.0, 2)));
        assertTrue(s(r(1.0, 2), r(4.0, 5)).contains(r(4.0, 5)));

        assertFalse(s(r(1.0, 2), r(4.0, 5)).contains(r(0, 1)));
    }

    @Test
    public void testUnion() {
        assertEquals(s(r(1)), s().union(1));
        assertEquals(s(r(1)), s(r(1)).union(1));

        assertEquals(s(r(1)), s().union(r(1)));
        assertEquals(s(r(2)), s().union(r(2)));

        assertEquals(s(r(1)), s(r(1)).union(r(1)));
        assertEquals(s(r(1.0), r(2)), s(r(1)).union(r(2)));

        assertEquals(s(r(1)), s(r(1)).union(s()));
        assertEquals(s(r(1.0), r(2)), s(r(1)).union(s(r(2))));
    }

    @Test
    public void testIntersection() {
        assertEquals(s(), s().intersection(1));
        assertEquals(s(r(1)), s(r(1)).intersection(1));
        assertEquals(s(), s(r(2)).intersection(1));

        assertEquals(s(), s().intersection(r(1)));
        assertEquals(s(r(1)), s(r(1)).intersection(r(1)));
        assertEquals(s(), s(r(2)).intersection(r(1)));
        assertEquals(s(r(2)), s(r(2)).intersection(r(1.0, 2)));

        assertEquals(s(), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 0)));
        assertEquals(s(r(1)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 1)));
        assertEquals(s(r(1.0, 2)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 2)));
        assertEquals(s(r(1.0, 3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 3)));
        assertEquals(s(r(1.0, 3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 4)));
        assertEquals(s(r(1.0, 3), r(5)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 5)));
        assertEquals(s(r(1.0, 3), r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 6)));
        assertEquals(s(r(1.0, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 7)));
        assertEquals(s(r(1.0, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 8)));

        assertEquals(s(r(1)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 1)));
        assertEquals(s(r(1.0, 2)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 2)));
        assertEquals(s(r(1.0, 3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 3)));
        assertEquals(s(r(1.0, 3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 4)));
        assertEquals(s(r(1.0, 3), r(5)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 5)));
        assertEquals(s(r(1.0, 3), r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 6)));
        assertEquals(s(r(1.0, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 7)));
        assertEquals(s(r(1.0, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 8)));

        assertEquals(s(r(2)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 2)));
        assertEquals(s(r(2, 3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 3)));
        assertEquals(s(r(2, 3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 4)));
        assertEquals(s(r(2, 3), r(5)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 5)));
        assertEquals(s(r(2, 3), r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 6)));
        assertEquals(s(r(2, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 7)));
        assertEquals(s(r(2, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 8)));

        assertEquals(s(r(3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(3, 3)));
        assertEquals(s(r(3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(3, 4)));
        assertEquals(s(r(3), r(5)), s(r(1.0, 3), r(5.0, 7)).intersection(r(3, 5)));
        assertEquals(s(r(3), r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(3, 6)));
        assertEquals(s(r(3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(3, 7)));
        assertEquals(s(r(3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(3, 8)));

        assertEquals(s(), s(r(1.0, 3), r(5.0, 7)).intersection(r(4.0, 4)));
        assertEquals(s(r(5)), s(r(1.0, 3), r(5.0, 7)).intersection(r(4.0, 5)));
        assertEquals(s(r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(4.0, 6)));
        assertEquals(s(r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(4.0, 7)));
        assertEquals(s(r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(4.0, 8)));

        assertEquals(s(r(5)), s(r(1.0, 3), r(5.0, 7)).intersection(r(5.0, 5)));
        assertEquals(s(r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(5.0, 6)));
        assertEquals(s(r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(5.0, 7)));
        assertEquals(s(r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(5.0, 8)));

        assertEquals(s(r(6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(6, 6)));
        assertEquals(s(r(6, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(6, 7)));
        assertEquals(s(r(6, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(6, 8)));

        assertEquals(s(r(7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(7, 7)));
        assertEquals(s(r(7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(7, 8)));

        assertEquals(s(), s(r(1.0, 3), r(5.0, 7)).intersection(r(8, 8)));

        assertEquals(s(r(3), r(5.0, 7), r(9)), s(r(1.0, 3), r(5.0, 7), r(9, 11)).intersection(r(3, 9)));

        assertEquals(s(r(3), r(5.0, 7), r(9)), s(r(1.0, 3), r(5.0, 7), r(9, 11)).intersection(s(r(3, 9))));

        assertEquals(s(), s().intersection(s()));
        assertEquals(s(), s(r(1)).intersection(s()));
    }

    @Test
    public void testRemove() {
        assertEquals(s(), s().remove(1));
        assertEquals(s(), s(r(1)).remove(1));
        // assertEquals(s(r(2, 3)), s(r(1.0, 3)).remove(1));
        // assertEquals(s(r(1), r(3)), s(r(1.0, 3)).remove(2));
        //
        // assertEquals(s(r(1.0, 3)), s(r(1.0, 3)).remove(0));
        // assertEquals(s(r(2, 3)), s(r(1.0, 3)).remove(1));
        // assertEquals(s(r(1), r(3)), s(r(1.0, 3)).remove(2));
        // assertEquals(s(r(1.0, 2)), s(r(1.0, 3)).remove(3));
        // assertEquals(s(r(1.0, 3)), s(r(1.0, 3)).remove(4));
        // assertEquals(s(r(1.0, 3)), s(r(1.0, 3)).remove(5));
        //
        // assertEquals(s(), s().remove(r(1)));
        // assertEquals(s(r(1.0, 3)), s(r(1.0, 3)).remove(r(1.0, 0)));
        // assertEquals(s(r(1.0, 3)), s(r(1.0, 3)).remove(r(0)));
        // assertEquals(s(r(2, 3)), s(r(1.0, 3)).remove(r(1)));
        // assertEquals(s(r(1), r(3)), s(r(1.0, 3)).remove(r(2)));
        // assertEquals(s(r(1.0, 2)), s(r(1.0, 3)).remove(r(3)));
        // assertEquals(s(r(1.0, 3)), s(r(1.0, 3)).remove(r(4)));
        // assertEquals(s(r(1.0, 3)), s(r(1.0, 3)).remove(r(5)));
        //
        // assertEquals(s(r(2, 3)), s(r(1.0, 3)).remove(r(0, 1)));
        // assertEquals(s(r(3)), s(r(1.0, 3)).remove(r(0, 2)));
        // assertEquals(s(), s(r(1.0, 3)).remove(r(0, 3)));
        // assertEquals(s(), s(r(1.0, 3)).remove(r(0, 4)));
        //
        // assertEquals(s(r(2, 3)), s(r(1.0, 3)).remove(r(1.0, 1)));
        // assertEquals(s(r(3)), s(r(1.0, 3)).remove(r(1.0, 2)));
        // assertEquals(s(), s(r(1.0, 3)).remove(r(1.0, 3)));
        // assertEquals(s(), s(r(1.0, 3)).remove(r(1.0, 4)));
        //
        // assertEquals(s(r(1), r(3)), s(r(1.0, 3)).remove(r(2, 2)));
        // assertEquals(s(r(1)), s(r(1.0, 3)).remove(r(2, 3)));
        // assertEquals(s(r(1)), s(r(1.0, 3)).remove(r(2, 4)));
        //
        // assertEquals(s(r(1.0, 2)), s(r(1.0, 3)).remove(r(3, 3)));
        // assertEquals(s(r(1.0, 2)), s(r(1.0, 3)).remove(r(3, 4)));
        //
        // assertEquals(s(r(1.0, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(0, 0)));
        // assertEquals(s(r(2, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(0, 1)));
        // assertEquals(s(r(3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(0, 2)));
        // assertEquals(s(r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(0, 3)));
        // assertEquals(s(r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(0, 4)));
        // assertEquals(s(r(6, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(0, 5)));
        // assertEquals(s(r(7)), s(r(1.0, 3), r(5.0, 7)).remove(r(0, 6)));
        // assertEquals(s(), s(r(1.0, 3), r(5.0, 7)).remove(r(0, 7)));
        // assertEquals(s(), s(r(1.0, 3), r(5.0, 7)).remove(r(0, 8)));
        //
        // assertEquals(s(r(2, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(1.0, 1)));
        // assertEquals(s(r(3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(1.0, 2)));
        // assertEquals(s(r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(1.0, 3)));
        // assertEquals(s(r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(1.0, 4)));
        // assertEquals(s(r(6, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(1.0, 5)));
        // assertEquals(s(r(7)), s(r(1.0, 3), r(5.0, 7)).remove(r(1.0, 6)));
        // assertEquals(s(), s(r(1.0, 3), r(5.0, 7)).remove(r(1.0, 7)));
        // assertEquals(s(), s(r(1.0, 3), r(5.0, 7)).remove(r(1.0, 8)));
        //
        // assertEquals(s(r(1), r(3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(2, 2)));
        // assertEquals(s(r(1), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(2, 3)));
        // assertEquals(s(r(1), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(2, 4)));
        // assertEquals(s(r(1), r(6, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(2, 5)));
        // assertEquals(s(r(1), r(7)), s(r(1.0, 3), r(5.0, 7)).remove(r(2, 6)));
        // assertEquals(s(r(1)), s(r(1.0, 3), r(5.0, 7)).remove(r(2, 7)));
        // assertEquals(s(r(1)), s(r(1.0, 3), r(5.0, 7)).remove(r(2, 8)));
        //
        // assertEquals(s(r(1.0, 2), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(3, 3)));
        // assertEquals(s(r(1.0, 2), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(3, 4)));
        // assertEquals(s(r(1.0, 2), r(6, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(3, 5)));
        // assertEquals(s(r(1.0, 2), r(7)), s(r(1.0, 3), r(5.0, 7)).remove(r(3, 6)));
        // assertEquals(s(r(1.0, 2)), s(r(1.0, 3), r(5.0, 7)).remove(r(3, 7)));
        // assertEquals(s(r(1.0, 2)), s(r(1.0, 3), r(5.0, 7)).remove(r(3, 8)));
        //
        // assertEquals(s(r(1.0, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(4.0, 4)));
        // assertEquals(s(r(1.0, 3), r(6, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(4.0, 5)));
        // assertEquals(s(r(1.0, 3), r(7)), s(r(1.0, 3), r(5.0, 7)).remove(r(4.0, 6)));
        // assertEquals(s(r(1.0, 3)), s(r(1.0, 3), r(5.0, 7)).remove(r(4.0, 7)));
        // assertEquals(s(r(1.0, 3)), s(r(1.0, 3), r(5.0, 7)).remove(r(4.0, 8)));
        //
        // assertEquals(s(r(1.0, 3), r(6, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(5.0, 5)));
        // assertEquals(s(r(1.0, 3), r(7)), s(r(1.0, 3), r(5.0, 7)).remove(r(5.0, 6)));
        // assertEquals(s(r(1.0, 3)), s(r(1.0, 3), r(5.0, 7)).remove(r(5.0, 7)));
        // assertEquals(s(r(1.0, 3)), s(r(1.0, 3), r(5.0, 7)).remove(r(5.0, 8)));
        //
        // assertEquals(s(r(1.0, 3), r(5), r(7)), s(r(1.0, 3), r(5.0, 7)).remove(r(6, 6)));
        // assertEquals(s(r(1.0, 3), r(5)), s(r(1.0, 3), r(5.0, 7)).remove(r(6, 7)));
        // assertEquals(s(r(1.0, 3), r(5)), s(r(1.0, 3), r(5.0, 7)).remove(r(6, 8)));
        //
        // assertEquals(s(r(1.0, 3), r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).remove(r(7, 7)));
        // assertEquals(s(r(1.0, 3), r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).remove(r(7, 8)));
        //
        // assertEquals(s(r(1.0, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).remove(r(8, 8)));
        //
        // assertEquals(s(r(1.0, 2), r(10, 11)), s(r(1.0, 3), r(5.0, 7), r(9, 11)).remove(r(3, 9)));
        //
        // assertEquals(s(), s().remove(s(r(1))));
        // assertEquals(s(r(1)), s(r(1)).remove(s()));
        //
        // assertEquals(s(r(1.0, 2), r(10, 11)), s(r(1.0, 3), r(5.0, 7), r(9, 11)).remove(s(r(3, 9))));
        // assertEquals(s(r(1.0, 3), r(5.0, 7), r(9, 11)), s(r(1.0, 3), r(5.0, 7), r(9, 11)).remove(s()));
    }

    @Test
    public void testConvert() {
        assertEquals(RealSet.EMPTY, RealSet.convert(BooleanSet.EMPTY));
        assertEquals(RealSet.create("1.0"), RealSet.convert(RealSet.create("1.0")));
        assertEquals(RealSet.create("1.0"), RealSet.convert(UncheckedSet.create("1.0")));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealSet.convert(BooleanSet.TRUE);
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealSet.convert(IntegerSet.create("0"));
                     });
    }

    @Test
    public void testEquals() {
        assertEquals(s(r(1), r(2)), s(r(2), r(1)));
        assertNotEquals(s(r(1), r(2)), null);
        assertNotEquals(s(r(1), r(2)), 1);
        assertNotEquals(s(), 1);
        assertEquals(s(), s());
        assertEquals(s(), s(r(1.0, 0)));
        assertNotEquals(s(), s(r(0)));
    }

    @Test
    public void testHashCode() {
        assertEquals(s().hashCode(), s().hashCode());
        assertEquals(s().hashCode(), s(r(1.0, 0)).hashCode());
        assertEquals(s(r(1)).hashCode(), s(r(1)).hashCode());
    }
}