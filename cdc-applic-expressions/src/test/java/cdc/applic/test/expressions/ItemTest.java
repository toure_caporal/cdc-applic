package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.Item;

public class ItemTest {
    @Test
    public void test() {
        assertEquals(IllegalArgumentException.class, Item.newIllegalConversion(null, null).getClass());
    }
}