package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;

public class ExpressionTest {

    @Test
    public void test() {
        assertTrue(Expression.TRUE.isTrue());
        assertFalse(Expression.TRUE.isFalse());
        assertFalse(Expression.FALSE.isTrue());
        assertTrue(Expression.FALSE.isFalse());
        assertTrue(Expression.fromString("true").isTrue());
        assertTrue(Expression.fromString("TRUE").isTrue());
        assertTrue(Expression.fromString("false").isFalse());
        assertTrue(Expression.fromString("FALSE").isFalse());

        assertEquals(Expression.TRUE, Expression.TRUE);
        assertNotEquals(Expression.TRUE, null);
        assertNotEquals(Expression.TRUE, "true");
        assertEquals(Expression.TRUE, Expression.fromString("true"));
        assertNotEquals(Expression.TRUE, Expression.fromString("TRUE"));

        assertEquals("A", Expression.asString(Expression.fromString("A")));

        assertTrue(Expression.isValid("A"));
        assertFalse(Expression.isValid("and"));

        assertEquals("true", Expression.TRUE.getContent());
        assertEquals(Expression.TRUE.toString(), Expression.TRUE.getContent());
        assertEquals(Expression.TRUE, Expression.fromString(null));
        assertEquals(Expression.TRUE, Expression.fromString(""));
        assertEquals(Expression.TRUE, Expression.fromString("  "));

        assertTrue(Expression.TRUE.isValid());

        final Expression invalid = new Expression("and", false);
        assertFalse(invalid.isValid());
        final Expression valid = new Expression("A", false);
        assertTrue(valid.isValid());

        assertNotEquals(Expression.FALSE.hashCode(), Expression.TRUE.hashCode());
    }
}