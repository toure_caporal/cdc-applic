package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.ItemKind;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.Value;

public class RealRangeTest {

    private static RealValue v(double value) {
        return RealValue.create(value);
    }

    private static RealRange r(double value) {
        return RealRange.create(value);
    }

    private static RealRange r(double min,
                               double max) {
        return RealRange.create(min, max);
    }

    @Test
    public void test() {
        assertEquals(ItemKind.REAL_RANGE, r(1).getKind());
        assertTrue(r(1).isSingleton());
        assertFalse(r(1).isEmpty());
        assertTrue(r(1, 0).isEmpty());
        assertTrue(r(1).contains(1));
        assertFalse(r(1).contains(2));
        assertFalse(r(1).contains(0));
        assertFalse(r(1, 0).contains(0));
        assertFalse(r(1, 0).contains(1));
        assertFalse(r(1, 0).contains(v(1)));
        assertFalse(r(1, 0).contains((Value) v(1)));
        assertEquals(v(1), r(1, 2).getMin());
        assertEquals(v(2), r(1, 2).getMax());
        assertEquals(v(1), r(1, 0).getMin());
        assertEquals(v(0), r(1, 0).getMax());

        assertTrue(r(1, 2).contains(r(10, 9)));
        assertTrue(r(1, 2).contains(r(1)));
        assertTrue(r(1, 2).contains(r(2)));
        assertTrue(r(1, 2).contains(r(1, 2)));
        assertFalse(r(1, 2).contains(r(3)));
        assertFalse(r(1, 2).contains(r(0)));
        assertFalse(r(1, 2).contains(r(0, 1)));
        assertFalse(r(1, 2).contains(r(0, 2)));
        assertFalse(r(1, 2).contains(r(0, 3)));
        assertFalse(r(1, 2).contains(r(1, 3)));
        assertFalse(r(1, 2).contains(r(2, 3)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r(1, 0).contains(IntegerValue.create(0));
                     });
    }

    @Test
    public void testCompare() {
        assertTrue(r(1).compareTo(r(1)) == 0);
        assertTrue(r(1, 2).compareTo(r(1)) == 0);
        assertTrue(r(1, 2).compareTo(r(2, 3)) == 0);

        assertTrue(r(1, 2).compareTo(r(3)) < 0);
        assertTrue(r(3).compareTo(r(1, 2)) > 0);

        assertTrue(r(1, 2).compareTo(r(0)) > 0);
        assertTrue(r(0).compareTo(r(1, 2)) < 0);

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r(0).compareTo(r(1, 0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r(1, 0).compareTo(r(0));
                     });
    }

    @Test
    public void testEquals() {
        final RealRange r = r(1);
        assertEquals(r, r);
        assertEquals(r(1, 0), r(2, 1));
        assertEquals(r(1, 2), r(1, 2));
        assertNotEquals(r(1, 2), r(1, 3));
        assertNotEquals(r(1, 2), r(0, 2));
        assertNotEquals(r(1, 2), r(2, 1));
        assertNotEquals(r(2, 1), r(1, 2));

        assertNotEquals(r(1, 2), null);
        assertNotEquals(r(1, 2), "hello");
    }

    @Test
    public void testHashCode() {
        assertEquals(r(0).hashCode(), r(0).hashCode());
        assertEquals(r(1, 0).hashCode(), r(2, 1).hashCode());
    }

    @Test
    public void testToString() {
        assertEquals("0.0~1.0", r(0, 1).toString());
        assertEquals("0.0", r(0).toString());
        assertEquals("0.0", r(-0.0).toString());
        assertEquals("0.0~-1.0", r(0, -1).toString());
    }
}