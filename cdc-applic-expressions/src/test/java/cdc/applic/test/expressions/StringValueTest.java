package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.ItemSetKind;
import cdc.applic.expressions.content.StringValue;

public class StringValueTest {
    private static void testConstructor(String literal,
                                        boolean escaped) {
        final StringValue value = StringValue.create(literal, escaped);

        assertEquals(value, value);

        assertTrue(value.isCompliantWith(ItemSetKind.STRING));
        assertTrue(value.isCompliantWith(ItemSetKind.UNCHECKED));
        assertFalse(value.isCompliantWith(ItemSetKind.INTEGER));
        assertFalse(value.isCompliantWith(ItemSetKind.REAL));
        assertFalse(value.isCompliantWith(ItemSetKind.BOOLEAN));
    }

    @Test
    public void testConstructor() {
        testConstructor("Hello", false);
        testConstructor("\"Hello\"", false);
        testConstructor("\"Hello\"", true);
    }

    @Test
    public void testInvalidArg() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         StringValue.create("Hello", true);
                     });
    }

    @Test
    public void testEquals() {
        assertEquals(StringValue.create("Hello", false), StringValue.create("\"Hello\"", true));
        assertNotEquals(StringValue.create("Hello", false), StringValue.create("Hel lo", false));
        assertNotEquals(StringValue.create("Hello", false), null);
        assertNotEquals(StringValue.create("Hello", false), "Hello");
    }

    @Test
    public void testToString() {
        assertEquals("Hello", StringValue.create("Hello", false).toString());
        assertEquals("Hello", StringValue.create("\"Hello\"", true).toString());
        assertEquals("\"Hel lo\"", StringValue.create("Hel lo", false).toString());
        assertEquals("\"Hel lo\"", StringValue.create("\"Hel lo\"", true).toString());
        assertEquals("\"\"\"Hel lo\"\"\"", StringValue.create("\"Hel lo\"", false).toString());
    }

    @Test
    public void testHashCode() {
        assertEquals(StringValue.create("Hello", false).hashCode(), StringValue.create("Hello", false).hashCode());
        assertNotEquals(StringValue.create("Hello", false).hashCode(), StringValue.create("Hel lo", false).hashCode());
    }
}