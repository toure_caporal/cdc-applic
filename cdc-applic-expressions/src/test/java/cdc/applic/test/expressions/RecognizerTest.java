package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.Recognizer;

public class RecognizerTest {

    private final Recognizer recognizer = new Recognizer();

    @Test
    public void testValid() {
        assertTrue(recognizer.isValid("Alias"));
        assertTrue(recognizer.isValid("Version = V1"));
        assertTrue(recognizer.isValid("Version in {}"));
        assertTrue(recognizer.isValid("Version in {V1}"));
        assertTrue(recognizer.isValid("Version in {V1, V2}"));
        assertTrue(recognizer.isValid("Version in {1, 2, 3~4}"));
        assertTrue(recognizer.isValid("Version = V1 or Version = V2"));
        assertTrue(recognizer.isValid("Version = V1 or Version = V2 and Standard = S1"));
        assertTrue(recognizer.isValid("M1000 = TRUE"));
        assertTrue(recognizer.isValid("TRUE"));
        assertTrue(recognizer.isValid("FALSE"));
        assertTrue(recognizer.isValid("(FALSE)"));
        assertTrue(recognizer.isValid("Version = V1 or true"));
        assertTrue(recognizer.isValid("A -> B"));
        assertTrue(recognizer.isValid("A <-> B"));
        assertTrue(recognizer.isValid("\"A A\" <-> B"));
        assertTrue(recognizer.isValid("\"A Property\" = \"A Value\""));
        assertTrue(recognizer.isValid("\"A Property\" = \"A \"\"Value\""));
        assertTrue(recognizer.isValid("\"A Property\" in { \"A Value\" }"));
        assertTrue(recognizer.isValid("\"A Property\" in { \"A Value\", \"Another Value\" }"));
        assertTrue(recognizer.isValid("\"A Property\" = \" \""));
        assertTrue(recognizer.isValid("Temp = 1.0"));
        assertTrue(recognizer.isValid("Temp != 1.0"));
        assertTrue(recognizer.isValid("Temp in  {1.0, 2.0}"));
        assertTrue(recognizer.isValid("Temp in {1.0, 2.0, 3.0~4.0}"));
        assertTrue(recognizer.isValid("Temp not in {1.0, 2.0, 3.0~4.0}"));
        assertTrue(recognizer.isValid("(Value = A)"));
        assertTrue(recognizer.isValid("(Value)"));
        assertTrue(recognizer.isValid("not Value"));
        assertTrue(recognizer.isValid("! Value"));
    }

    @Test
    public void testInvalid() {
        assertFalse(recognizer.isValid("(Value = A"));
        assertFalse(recognizer.isValid("(Version = V1"));
        assertFalse(recognizer.isValid("Value = A)"));
        assertFalse(recognizer.isValid("Value ="));
        assertFalse(recognizer.isValid("Value =)"));
        assertFalse(recognizer.isValid("Value)"));
        assertFalse(recognizer.isValid("Value in { A to B }"));
        assertFalse(recognizer.isValid("Value in { 1-B }"));
        assertFalse(recognizer.isValid("Value in { 1-BBBB }"));
        assertFalse(recognizer.isValid("Version in {M}}"));
        assertFalse(recognizer.isValid("Version in {M"));
        assertFalse(recognizer.isValid("()"));
        assertFalse(recognizer.isValid("(FALSE))"));
        assertFalse(recognizer.isValid("((FALSE)"));
        assertFalse(recognizer.isValid("FALSE = FALSE"));
        assertFalse(recognizer.isValid("XXX = "));
        assertFalse(recognizer.isValid("XXX = {}"));
        assertFalse(recognizer.isValid("XXX in {,}"));
        assertFalse(recognizer.isValid("XXX in {A,}"));
        assertFalse(recognizer.isValid("\"A Property\" = \"A \"Value\""));
        assertFalse(recognizer.isValid("\"A Property\" in {,}"));
        assertFalse(recognizer.isValid("\"A Property\" in {\"\"}"));
        assertFalse(recognizer.isValid("\"A Property\" in {A,}"));
        assertFalse(recognizer.isValid("\"A Property\" in { \"A \"Value\" }"));
        assertFalse(recognizer.isValid("\"A Property\" in { \"A Value\", }"));
        assertFalse(recognizer.isValid("\"A Property\" in { \"A Value\", \"}"));
        assertFalse(recognizer.isValid("\"A Property\" in { \"A Value\", \"\"}"));
    }
}