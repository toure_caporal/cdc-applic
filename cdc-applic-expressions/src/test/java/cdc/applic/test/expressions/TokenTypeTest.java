package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.TokenType;

public class TokenTypeTest {

    @Test
    public void testIsBinary() {
        final Set<TokenType> types = new HashSet<>();
        types.add(TokenType.AND);
        types.add(TokenType.OR);
        types.add(TokenType.EQUIV);
        types.add(TokenType.IMPL);
        for (final TokenType type : TokenType.values()) {
            assertEquals(type.isBinary(), types.contains(type));
        }
    }

    @Test
    public void testIsSetElement() {
        final Set<TokenType> types = new HashSet<>();
        types.add(TokenType.ESCAPED_TEXT);
        types.add(TokenType.TEXT);
        types.add(TokenType.INTEGER);
        types.add(TokenType.REAL);
        types.add(TokenType.FALSE);
        types.add(TokenType.TRUE);
        for (final TokenType type : TokenType.values()) {
            assertEquals(type.isItem(), types.contains(type));
        }
    }

    @Test
    public void testSupportsRange() {
        final Set<TokenType> types = new HashSet<>();
        types.add(TokenType.INTEGER);
        types.add(TokenType.REAL);
        for (final TokenType type : TokenType.values()) {
            assertEquals(type.supportsRange(), types.contains(type));
        }
    }
}