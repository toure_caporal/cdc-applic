package cdc.applic.test.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.UncheckedSet;

public class AbstractItemSetTest {

    @Test
    public void test() {
        assertEquals(BooleanSet.EMPTY, BooleanSet.EMPTY.toBooleanSet());
        assertEquals(BooleanSet.EMPTY, IntegerSet.EMPTY.toBooleanSet());
        assertEquals(BooleanSet.EMPTY, RealSet.EMPTY.toBooleanSet());
        assertEquals(BooleanSet.EMPTY, StringSet.EMPTY.toBooleanSet());
        assertEquals(BooleanSet.EMPTY, UncheckedSet.EMPTY.toBooleanSet());

        assertEquals(IntegerSet.EMPTY, BooleanSet.EMPTY.toIntegerSet());
        assertEquals(IntegerSet.EMPTY, IntegerSet.EMPTY.toIntegerSet());
        assertEquals(IntegerSet.EMPTY, RealSet.EMPTY.toIntegerSet());
        assertEquals(IntegerSet.EMPTY, StringSet.EMPTY.toIntegerSet());
        assertEquals(IntegerSet.EMPTY, UncheckedSet.EMPTY.toIntegerSet());

        assertEquals(RealSet.EMPTY, BooleanSet.EMPTY.toRealSet());
        assertEquals(RealSet.EMPTY, IntegerSet.EMPTY.toRealSet());
        assertEquals(RealSet.EMPTY, RealSet.EMPTY.toRealSet());
        assertEquals(RealSet.EMPTY, StringSet.EMPTY.toRealSet());
        assertEquals(RealSet.EMPTY, UncheckedSet.EMPTY.toRealSet());

        assertEquals(StringSet.EMPTY, BooleanSet.EMPTY.toStringSet());
        assertEquals(StringSet.EMPTY, IntegerSet.EMPTY.toStringSet());
        assertEquals(StringSet.EMPTY, RealSet.EMPTY.toStringSet());
        assertEquals(StringSet.EMPTY, StringSet.EMPTY.toStringSet());
        assertEquals(StringSet.EMPTY, UncheckedSet.EMPTY.toStringSet());

        assertTrue(BooleanSet.EMPTY.isEmpty());
        assertFalse(BooleanSet.EMPTY.isSingleton());
        assertTrue(BooleanSet.TRUE.isSingleton());

        assertTrue(BooleanSet.TRUE.contains(IntegerSet.EMPTY));
        assertTrue(BooleanSet.TRUE.contains(UncheckedSet.create("true")));
        assertFalse(BooleanSet.TRUE.contains(UncheckedSet.create("false")));
    }
}