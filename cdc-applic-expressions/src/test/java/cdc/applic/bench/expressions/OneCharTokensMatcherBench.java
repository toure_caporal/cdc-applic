package cdc.applic.bench.expressions;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.applic.expressions.parsing.OneCharTokens;
import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time = 100, timeUnit = TimeUnit.MILLISECONDS)
public class OneCharTokensMatcherBench {

    @Param({ "a", " ", "{", "b", "!", "c", "(", ")", "&", "|" })
    public char c;

    @Benchmark
    public boolean benchMultiplyShiftMatcher() {
        return OneCharTokens.MULTIPLY_SHIFT_MATCHER.matches(c);
    }

    @Benchmark
    public boolean benchMultiplyShiftInlineMatcher() {
        return OneCharTokens.MULTIPLY_SHIFT_INLINE_MATCHER.matches(c);
    }

    @Benchmark
    public boolean benchBitSetMatcher() {
        return OneCharTokens.BIT_SET_MATCHER.matches(c);
    }

    @Benchmark
    public boolean benchBooleanArrayMatcher() {
        return OneCharTokens.BOOLEAN_ARRAY_MATCHER.matches(c);
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(OneCharTokensMatcherBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", OneCharTokensMatcherBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}