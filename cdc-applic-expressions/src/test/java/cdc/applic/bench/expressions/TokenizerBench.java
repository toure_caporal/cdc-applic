package cdc.applic.bench.expressions;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.applic.expressions.parsing.Tokenizer;
import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 100, timeUnit = TimeUnit.MILLISECONDS)
public class TokenizerBench {

    @Param({ "V",
            "true",
            "false",
            "⊤",
            "⊥",
            // "⊤⊥",
            // "V=V1",
            // "V in {V1}",
            // "V <: {V1}",
            // "V<:{V1}",
            // "V ∈ {V1}",
            // "V∈{V1}",
            // "V in {V1,V2}",
            // "V <: {V1,V2}",
            // "V ∈ {V1,V2}",
            // "V=V1&S=S2|V=V2"
    })
    public String expression;

    final Tokenizer tokenizer = new Tokenizer();

    @Benchmark
    public int benchNextToken() {
        int count = 0;
        tokenizer.init(expression);
        while (tokenizer.hasMoreTokens()) {
            tokenizer.nextToken();
            count++;
        }
        return count;
    }

    @Benchmark
    public int benchNext() {
        int count = 0;
        tokenizer.init(expression);
        while (tokenizer.hasMoreTokens()) {
            tokenizer.next();
            count++;
        }
        return count;
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(TokenizerBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", TokenizerBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}