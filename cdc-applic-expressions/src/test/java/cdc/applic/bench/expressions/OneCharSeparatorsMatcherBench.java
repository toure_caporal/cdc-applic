package cdc.applic.bench.expressions;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.applic.expressions.parsing.OneCharSeparators;
import cdc.applic.expressions.parsing.Tokenizer;
import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time = 100, timeUnit = TimeUnit.MILLISECONDS)
public class OneCharSeparatorsMatcherBench {

    @Param({ "a", " ", "{", "b", "!", "c", "(", ")", "&", "|" })
    public char c;

    final Tokenizer tokenizer = new Tokenizer();

    @Benchmark
    public boolean benchMultiplyShift() {
        return OneCharSeparators.MULTIPLY_SHIFT_MATCHER.matches(c);
    }

    @Benchmark
    public boolean benchMultiplyInlineShift() {
        return OneCharSeparators.MULTIPLY_SHIFT_INLINE_MATCHER.matches(c);
    }

    @Benchmark
    public boolean benchBitSet() {
        return OneCharSeparators.BIT_SET_MATCHER.matches(c);
    }

    @Benchmark
    public boolean benchBooleanArray() {
        return OneCharSeparators.BOOLEAN_ARRAY_MATCHER.matches(c);
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(OneCharSeparatorsMatcherBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", OneCharSeparatorsMatcherBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}